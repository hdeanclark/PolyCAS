#!/usr/bin/env bash

set -x

# Compile the GR compiler.
./compile.sh  

# Input filename.
INPUTFILE="/tmp/Concatenated.gr"

# Destination filenames.
SOURCEFILE="../src/PolyCAS_Autogen_Simplify.cc"
HEADERFILE="../src/PolyCAS_Autogen_Simplify.h"
BACKUPSTOR="../src/OLD/PolyCAS_Autogen_Backups/"


# Remove the input file, if it exists.
if [ -f "${INPUTFILE}" ] ; then
    echo "Concatenated file ${INPUTFILE} exists. Making a backup of existing file at ${INPUTFILE}.backup"
    mv "${INPUTFILE}" "${INPUTFILE}.backup"
fi

# Concatenate all the recipes in ./Recipes/ into a single file.
for i in ./Recipes/*.gr ; do
    cat "$i" >> "${INPUTFILE}"
done

# Testing for the input file.
if [ ! -f "${INPUTFILE}" ] ; then
    echo "Unable to find input file ${INPUTFILE} . Were there any recipes to concatenate? Exiting."
    exit
fi

# Move/Backup existing destination files.
TIMESTAMP=`date +%Y-%m-%d-%H:%M:%S`
mkdir -p "${BACKUPSTOR}"

if [ -f "${SOURCEFILE}" ] ; then
    echo "Encountered existing source file ${SOURCEFILE} . Storing it"
    mv "${SOURCEFILE}" "${BACKUPSTOR}`basename $SOURCEFILE`${TIMESTAMP}"
#    rm "${SOURCEFILE}"
fi
if [ -f "${HEADERFILE}" ] ; then
    echo "Encountered existing header file ${HEADERFILE} . Storing it"
    mv "${HEADERFILE}" "${BACKUPSTOR}`basename $HEADERFILE`${TIMESTAMP}"
#    rm "${HEADERFILE}"
fi

# Generate the files from the .gr file.
./gr "${INPUTFILE}"    "${SOURCEFILE}" "${HEADERFILE}"


