" This puts keywords into the grKeywords highlight group.
:syn keyword grFlow   if fi and else elif bail lest elsebail 
:syn keyword grFlow   req then for rof from while do doing 
:syn keyword grItems  child children type string
:syn keyword grLogic  and or not is a has of with prefixed into intvalue number value by numerical true
:syn keyword grOps    becomes swap wrap duplicate dup turns new append remove increase
:syn keyword grNPC    infinity inf missingno
:syn keyword grDebug  print printnode

:syn match grLogic  /==/
:syn match grSpec   /->/

" This creates a match on the date and puts in the highlight group called logDate.  The
" nextgroup and skipwhite makes vim look for logTime after the match
":syn match grComments /^\d\{4}-\d\{2}-\d\{2}/ nextgroup=logTime skipwhite

" This creates a match on the time (but only if it follows the date)
":syn match logTime /\d\{2}:\d\{2}:\d\{2},\d\{3}/
":syn match grComments "\v(^\s*//.*\n)+" fold
":syn match grComments "(//.*\n)"
":syn match grComments start="//" skip="\\$" end="$" keepend
syntax region  grComments      start="//" skip="\\$" end="$" keepend 

:syn match grFunctions /^.*{$/ 
:syn match grFunctions /^}$/    

" Now make them appear:
" Link just links logError to the colouring for error
hi link grFlow      Keyword
hi link grItems     Operator
hi link grLogic     Conditional
hi link grOps       Boolean
hi link grComments  Comment
hi link grFunctions Function
hi link grSpec      Function
hi link grNPC       Special
hi link grDebug     Special

"  HiLink cmakeStatement Statement
"  HiLink cmakeComment Comment
"  HiLink cmakeString String
"  HiLink cmakeVariableValue Type
"  HiLink cmakeRegistry Underlined
"  HiLink cmakeArguments Identifier
"  HiLink cmakeArgument Constant
"  HiLink cmakeEnvironment Special
"  HiLink cmakeOperators Operator
"  HiLink cmakeMacro PreProc
"  HiLink cmakeError Error
"  HiLink cmakeTodo TODO
"  HiLink cmakeEscaped Special

" Def means default colour - colourschemes can override
"hi def grKeywords guibg=black guifg=yellow
