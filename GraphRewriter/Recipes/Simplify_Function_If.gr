///* vim: set filetype=gr : */
//
//Recipe file for generating graph rewriting operations.
//
//Note: 'and' has precedence over 'or', where applicable (as in C/C++.)
//

Simplify_Function_If{
    //This routine requires a FuncNode, which holds a function:
    //  - named 'if' or 'IF'
    //  - which has three children:
    //      if( logical expr.,  nodeA, nodeB).
    //
    //Assumptions:
    //  - The logical expression must be able to evaluate to a number
    //     or must be a NegateNode with a number child. Otherwise, we 
    //     cannot simplify.
    //
    //Actions:
    //  - If the logical expression is NON-ZERO, nodeA is promoted.
    //     Otherwise, nodeB is promoted.
    //

    req   base is a function with name if 
    or    base is a function with name IF   
    elsebail  

    req   base has ==3 child
    elsebail

    base children prefixed A

    //First, the case that the number is not wrapped with a negative.
    if   A[0] is type ExprRational   or   A[0] is type ExprDouble  then
        if    A[0] has numerical value 0  then
            base becomes A[2]
        else
            base becomes A[1]
        fi 
        -> bail
    fi

    //Second, the case that the number is wrapped with a negative.
    if   A[0] is type FuncNegate   and  A[0] has ==1 child  then
        A[0] children prefixed B

        if   B[0] is type ExprRational   or   B[0] is type ExprDouble  then
            if    B[0] has numerical value 0  then
                base becomes A[2]
            else
                base becomes A[1]
            fi
            -> bail
        fi
    fi

}
