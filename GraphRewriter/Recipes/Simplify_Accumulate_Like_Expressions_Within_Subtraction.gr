///* vim: set filetype=gr : */
//
//Recipe file for generating graph rewriting operations.
//
//Note: 'and' has precedence over 'or', where applicable (as in C/C++.)
//


Simplify_Pair_Identical_Expressions_Within_Subtraction{
    //This function pairs (or cancels) identical FuncSubtractNode children subexpressions, 
    // multiplying with a ExprRationalNode with value 2 or 0.
    req  base is type FuncSubtract and base has >1 child elsebail
    base children prefixed A

    for i from 1 while i<A.size() doing ++i
        for j from 0 while j<i doing ++j
            //Look for sub-expressions which are equal.
            if A[i] == A[j] then
                //Finally, ensure catch any loops from previous simplification. Catch zeros!
                if A[i] is type ExprRational and A[i] has numerical value 0 then
                    //Do nothing - continue with loop!

                elif A[i] is type ExprDouble and A[i] has numerical value 0 then
                    //Do nothing - continue with loop!

                //Cancel the nodes because they are like: '+A-A-...'
                elif j==0 is true then
                    -> C is a new ExprRationalNode with intvalue 0

                    if base has >2 children then
                        //Cancel the nodes.
                        A[j] becomes C
                        remove child number i from base
                        bail 
                    else
                        base becomes C
                        bail
                    fi

                //Sum the nodes because they are like: '...-A-A-...'
                else
                    //Create a new FuncMultiplyNode.
                    -> B is a new FuncMultiplyNode

                    //Replace the first occurence with it.
                    A[j] becomes B

                    //Append the first child as an ExprRationalNode with value 2.
                    // Append the second occurence as the second child
                    C is a new ExprRationalNode with intvalue 2
                    A[j] append child C
                    A[j] append child A[i]

                    //Remove the second occurence child from base's children.
                    remove child number i from base
                    bail
                fi
            fi
        rof
    rof
}

Simplify_Cancel_Identical_Expressions_Within_Subtraction{
    //This function cancels identical FuncAddNode children subexpressions, multiplying with 
    // a ExprRationalNode with value 0. 
    req  base is type FuncAdd and base has >1 child elsebail
    base children prefixed A

    for i from 0 while i<A.size() doing ++i
        for j from 0 while j<A.size() doing ++j
            if A[i] is type FuncNegate and A[i] has >0 children then
                A[i] children prefixed B //B[0] is the actual expression (sans the negation.)

                if B[0] == A[j] then     //A[j] is the actual expression.

                    //Finally, ensure catch any loops from previous simplification. Catch zeros!
                    if A[j] is type ExprRational and A[j] has numerical value 0 then

                        //Do nothing - continue looping!

                    elif A[j] is type ExprDouble and A[j] has numerical value 0 then

                        //Do nothing - continue looping!

                    //There are three cases. The simplest is: '+A-X-(-X)-...'
                    elif i!=0 is true and j!=0 is true then
                        -> C is a new ExprRationalNode with intvalue 0 
                        D is a new ExprRationalNode with intvalue 0
                        A[i] becomes C
                        A[j] becomes D
                        //We now have '+A-0-(-0)-...'
                        bail 

                    //The case when A[i=0] is the FuncNegationNode: '(-X)-A-X-...'
                    elif i==0 is true and j!=0 is true then
                        //Create a new FuncMultiplyNode. Replace the FuncNegationNode-wrapped occurence with it.
                        -> C is a new FuncMultiplyNode
                        B[0] becomes C
    
                        //Append the first child as an ExprRationalNode with value 2.
                        // Append the second occurence as the second child
                        D is a new ExprRationalNode with intvalue 2
                        B[0] append child D
                        B[0] append child A[j]
    
                        //Remove the second occurence child from base's children.
                        remove child number j from base

                        //We now have: '(-(2*X))-A-0-...'
                        bail

                    //The case when A[i=0] is not the FuncNegationNode: '+X-A-(-X)-...'
                    elif i!=0 is true and j==0 is true then
                        //Create a new FuncMultiplyNode. Replace the FuncNegationNode-wrapped occurence with it.
                        -> C is a new FuncMultiplyNode
                        A[j] becomes C
 
                        //Append the first child as an ExprRationalNode with value 2.
                        // Append the second occurence as the second child
                        D is a new ExprRationalNode with intvalue 2
                        A[j] append child D
                        A[j] append child B[0]
 
                        //Remove the second occurence child from base's children.
                        remove child number i from base

                        //We now have: '(2*X)-A-0-...'
                        bail
                    fi
                fi
            fi
        rof
    rof
}


//Simplify_Accumulate_PseudoSimilar_Expressions_Within_Addition{
//    //This function pairs similar FuncAddNode children subexpressions where one expression contains
//    // a ExprRationalNode factor multiplying a subexpression X, with a 'free' X.
//    // The ExprRationalNode is incremented by 1. 
//    //
//    //This routine requires numerical nodes to be on the left.
//    req  base is type FuncAdd and base has >1 child elsebail
//    base children prefixed A
//
//    for i from 0 while i<A.size() doing ++i
//        for j from 0 while j<A.size() doing ++j
//            //Look for pseudo-similar sub-expressions.
//            if A[i] is type FuncMultiply and A[i] has ==2 child then
//                A[i] children prefixed B
//                if B[1] == A[j] then
//                    if B[0] is type ExprRational or B[0] is type ExprDouble then
//    
//                        //We increment the ExprRational by 1.
//                        -> increase value of B[0] by intvalue 1
//    
//                        //Being careful not to produce a single argument FuncAddNode, we consider two possibilities.
//                        if base has >2 children then
//                            //Remove the extra node in A at j.
//                            remove child number j from base
//                        elif base has ==2 children then 
//                            //We replace the base. A[j] is redundant and A[i] is the only other child of base.
//                            base becomes A[i]
//                        fi
//    
//                        //Whichever possibility, A is now invalid.
//                        bail
//                    fi
//                fi
//            fi
//        rof
//    rof
//}
//
//
//Simplify_Cancel_PseudoSimilar_Expressions_Within_Addition{
//    //This function pairs similar FuncAddNode children subexpressions where one expression contains
//    // a ExprRationalNode factor multiplying a subexpression X, with a 'free' NegateNode-wrapped X.
//    // The ExprRationalNode is decremented by 1. 
//    //
//    //This routine requires numerical nodes to be on the left.
//    req  base is type FuncAdd and base has >1 child elsebail
//    base children prefixed A
//
//    for i from 0 while i<A.size() doing ++i
//        for j from 0 while j<A.size() doing ++j
//
//            //Look for pseudo-similar sub-expressions.
//            if A[i] is type FuncMultiply and A[i] has ==2 child and A[j] is type FuncNegate then
//                A[i] children prefixed B
//                A[j] children prefixed C
//                if B[1] == C[0] then
//                    if B[0] is type ExprRational or B[0] is type ExprDouble then
//
//                        //We increment the ExprRational by 1.
//                        -> increase value of B[0] by intvalue -1
//
//                        //Being careful not to produce a single argument FuncAddNode, we consider two possibilities.
//                        if base has >2 children then
//                            //Remove the extra node in A at j.
//                            remove child number j from base
//                        elif base has ==2 children then
//                            //We replace the base. A[j] is redundant and A[i] is the only other child of base.
//                            base becomes A[i]
//                        fi
//
//                        //Whichever possibility, A is now invalid.
//                        bail
//                    fi
//                fi
//            fi
//        rof
//    rof
//}
//
