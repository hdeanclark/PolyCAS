///* vim: set filetype=gr : */
//
//Recipe file for generating graph rewriting operations.
//
//Note: 'and' has precedence over 'or', where applicable (as in C/C++.)
//

Simplify_Numerical_LessThans{
    //This routine takes FuncLessThan node with
    //  - two numerical children, or
    //  - two negatenodes each with one numerical child, or
    //  - one of each.
    //
    //Actions:
    //  - If this expression is unambiguous and contains only numerical values,
    //     we can safely convert inequalities to 0 or 1.
    //

//- Input some rules for dealing with the < > <= >= nodes in the special case of numeric nodes.
//  If both children are numbers, it is never going to be the case that it is a definition (whereas
//  it might be a definition or assumption if we have something like 'a < -1').
//  Needs to handle cases where:
//  A is number, B is number
//  A is negate (number child), B is number
//  A is number, B is negate (number child)
//  A is negate (number child), B is negate (number child)


    req   base is type FuncLessThan  and   base has ==2 child 
    elsebail

    base children prefixed A

    req ( 
         A[0] is type ExprRational   or   A[0] is type ExprDouble   
           or  ( A[0] is type FuncNegate    and   A[0] has ==1 child )      
    ) and (   
         A[1] is type ExprRational   or   A[1] is type ExprDouble
           or ( A[1] is type FuncNegate    and   A[1] has ==1 child )      
    ) elsebail

    //Duplicate the nodes so we can modify them without worry.
    B is duplicate of A[0]
    C is duplicate of A[1]

    if  B is type FuncNegate then
        B children prefixed b

        //Require a numerical child.
        req   b[0] is type ExprRational   or  b[0] is type ExprDouble 
        elsebail 

        //Overrule the separated negation and promote the value.
        multiply value of b[0] by intvalue -1
        B becomes b[0]
    fi

    if  C is type FuncNegate then
        C children prefixed c

        //Require a numerical child.
        req   c[0] is type ExprRational   or  c[0] is type ExprDouble
        elsebail

        //Overrule the separated negation and promote the value.
        multiply value of c[0] by intvalue -1
        C becomes c[0]
    fi

    //At this point, we are certain we can reduce the expression.

    if B is numerically less than C then
        D is a new ExprDoubleNode with intvalue 1
        base becomes D
    else
        D is a new ExprDoubleNode with intvalue 0
        base becomes D
    fi

    -> bail
}
