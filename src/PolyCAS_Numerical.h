//Numerical.h - Routines for (purely) numerical evaluation.

#ifndef PROJECT_POLYCAS_NUMERICAL_H_HDR_GRD_
#define PROJECT_POLYCAS_NUMERICAL_H_HDR_GRD_

#include <iostream>
#include <string>
#include <memory>
#include <list>
#include <cmath>
#include <typeinfo>      //Needed for runtime type identification.

#include <gmpxx.h>       //Needed for ExprRational and ExprDouble (numerical) types.

#include <boost/spirit/include/qi.hpp>
//#include <boost/spirit/include/phoenix_core.hpp>
//#include <boost/spirit/include/phoenix_operator.hpp>

#include "YgorMisc.h"
#include "YgorString.h"
#include "YgorFilesDirs.h"

#include "PolyCAS_Structs.h"
//#include "Simplify.h"

#define NUMERICAL_H_VERBOSE false


template <class T>
T Local_Use_Built_In_Function_Evaluation(const std::string &name, const std::vector<T> &vars, bool *Found){
    *Found = true;
    const auto cname = Canonicalize_String2(name, CANONICALIZE::TO_UPPER);

    //Check if the string matches any known, common function signature.

//NOTE: Should I just try use something like dlopen on libm in order to resolve all libm functions? 
// If I do, can I statically link with dlopen? How will I manage the number of args safely?

    //Elementary functions.
    if(      (cname == "SIN")      && (vars.size() == 1)){    return  std::sin(vars[0]);
    }else if((cname == "COS")      && (vars.size() == 1)){    return  std::cos(vars[0]);
    }else if((cname == "TAN")      && (vars.size() == 1)){    return  std::tan(vars[0]);
    }else if((cname == "EXP")      && (vars.size() == 1)){    return  std::exp(vars[0]);
    }else if((cname == "LOG")      && (vars.size() == 1)){    return  std::log(vars[0]);
    }else if((cname == "LN")       && (vars.size() == 1)){    return  std::log(vars[0]);
    }else if((cname == "SQRT")     && (vars.size() == 1)){    return  std::sqrt(vars[0]);
    }else if((cname == "POW")      && (vars.size() == 2)){    return  std::pow(vars[0],vars[1]);
    }else if((cname == "POWF")     && (vars.size() == 2)){    return  std::pow(vars[0],vars[1]);
    }else if((cname == "ASIN")     && (vars.size() == 1)){    return  std::asin(vars[0]);
    }else if((cname == "ACOS")     && (vars.size() == 1)){    return  std::acos(vars[0]);
    }else if((cname == "ATAN2")    && (vars.size() == 2)){    return  std::atan2(vars[0],vars[1]);

    //Discontinuous or selector functions.
    }else if((cname == "MIN")      && (vars.size() == 2)){    return  YGORMIN(vars[0],vars[1]);
    }else if((cname == "MAX")      && (vars.size() == 2)){    return  YGORMAX(vars[0],vars[1]);
    }else if((cname == "AREEQUAL") && (vars.size() == 2)){    return  (vars[0] == vars[1]) ? (T)(1) : (T)(0);
    }else if((cname == "GT")       && (vars.size() == 2)){    return  (vars[0] >  vars[1]) ? (T)(1) : (T)(0); //I think this is a full-fledged node types. This function may be unnecessary...
    }else if((cname == "LT")       && (vars.size() == 2)){    return  (vars[0] <  vars[1]) ? (T)(1) : (T)(0); //I think this is a full-fledged node types. This function may be unnecessary...
    }else if((cname == "GTE")      && (vars.size() == 2)){    return  (vars[0] >= vars[1]) ? (T)(1) : (T)(0); //I think this is a full-fledged node types. This function may be unnecessary...
    }else if((cname == "LTE")      && (vars.size() == 2)){    return  (vars[0] <= vars[1]) ? (T)(1) : (T)(0); //I think this is a full-fledged node types. This function may be unnecessary...

    //Control flow (emulated with 'TRUE' == 1.0, 'FALSE' == 0.0).
    }else if((cname == "IF")       && (vars.size() == 3)){    return  ((vars[0] != (T)(0)) ? vars[1] : vars[2]);
    }else if((cname == "OR")       && (vars.size() == 2)){    return  ((vars[0] || vars[1]) ? (T)(1) : (T)(0));
    }else if((cname == "AND")      && (vars.size() == 2)){    return  ((vars[0] && vars[1]) ? (T)(1) : (T)(0));

    // ...put any other desirable built-ins here...
    }

    FUNCWARN("Unable to determine which built-in function corresponds to given function name '" << name << "'");
    *Found = false;
    return static_cast<T>(0);
}


template <class T>
std::unique_ptr<BaseNode> Local_Use_Numerical_Basic(std::unique_ptr<BaseNode> in, bool *OK, T *result){
    //This is a recursive routine which attempts to cast numerical nodes to type T and evaluate 
    // certain built-in/predefined functions assuming this type.
    //
    //NOTE: This routine does *NOT* invoke the simplification rules! Everything here is done
    // in a tailored-to-numerical-simplification way!
    //
    //NOTE: Whether or not this routine should emit nan can be set at compile time. Although it is
    // a valid floating-point number, this is a 'basic' numerical routine. The default should 
    // probably be to disallow nans and infs.
    //
    //NOTE: Do not attempt to filter out expressions with INF or NAN here. If they should be 
    // disallowed, do so in the calling function (Numerical_Basic) which is a natural gateway
    // back to the user. A more technical reason is that this way we can support a intermediate
    // INF and NAN in, say, IF statements. This is not lazy evaluation, but is a sort of 'lazy
    // error checking'.
    //
    //NOTE: IF functions support lazy evaluation.

    //Basic sanity checks.
    if(in     == nullptr) FUNCERR("Unable to evaluate expression numerically - invalid tree passed in");
    if(OK     == nullptr) FUNCERR("Unable to evaluate expression numerically - invalid bool passed in");
    if(result == nullptr) FUNCERR("Unable to evaluate expression numerically - invalid output container passed in");
    *OK = false;

    //Check if the user has implemented some special new nodes. Remind them that these routines are whitelists.
    if(in->Type > 0x11){
        FUNCERR("Detected a node which has not been properly added to this routine. See source for info");
        // More info:
        //
        // Please add it (this is a sort of whitelist function which will fail on unknown node types).
        // Wherever possible, functionality from this function should be moved into the autogenerated
        // simplification routines. This has several benefits:
        //    - Changes to the autogeneration get applied to everything,
        //    - The toy language makes (some things) easier and clearer to reason about,
        //    - Having the routines in one spot allows better possibilities of total simplification.
        //
        //   As an alternative, consider putting any numerical simplification routines into a separate
        // file or function so that it can be called by the grand simplification routines.
    }


    //If this is a numerical type, then attempt to convert the numerical member to type T.
    if(in->Type == NodeType::ExprDouble){
        in = Get_Numerical_Member_x<ExprDoubleNode,T>(std::move(in), result);
        *OK = true;
        return in;

    }else if(in->Type == NodeType::ExprRational){
        mpq_class x(0);
        in = Get_Numerical_Member_x<ExprRationalNode,mpq_class>(std::move(in), &x);
        //Here we convert to double PRIOR to converting to type T. There is likely a better way, but it will suffice for now..
        *result = static_cast<T>(x.get_d()); 
        *OK = true;
        return in;
    }

    //If this is an ExprNode, then attempt to convert the Raw_Expr to type T.
    if(in->Type == NodeType::Expr){
        //The following routine is *considerably* faster than stringstreams (stringtoX.) It depends on the boost qi parser.
        // It can handle the built-in numeric types (float, double, long double, int, long int, unsigned, etc..)
        // See: http://www.boost.org/doc/libs/1_51_0/libs/spirit/doc/html/spirit/qi/quick_reference/qi_parsers/numeric.html
        // and  http://boost.2283326.n4.nabble.com/how-to-get-qi-double-from-template-argument-T-if-T-is-double-td2679291.html.
        //if(!boost::spirit::qi::parse(in->Raw_Expr.begin(), in->Raw_Expr.end(), PROJ_POLYCAS_NUM_BST_NUM_PARSE(T), *result)){
        if(!boost::spirit::qi::parse(in->Raw_Expr.begin(), in->Raw_Expr.end(), boost::spirit::qi::create_parser<T>(), *result)){
            if(NUMERICAL_H_VERBOSE) FUNCINFO("Could not interpret '" << in->Raw_Expr << "' as type T = " << typeid(T(0)).name());
        }else{
            *OK = true;
        }
        return in;
    }

    //If this is node which cannot possibly be converted to a valid numerical statement (eg. ShuttleNode, ContCommaNode)
    // then we immediately bail. 
    if( (in->Type == NodeType::Shuttle)   || (in->Type == NodeType::ContParenthesis) 
     || (in->Type == NodeType::ContComma) || (in->Type == NodeType::ContAssignment)  ){
        FUNCWARN("Encountered a node of type " << in->Type << " and bailed on numerical evaluation. Dumping graph");
        WriteStringToFile(in->Output_Parse_Tree(), "/tmp/polycas_parsed.gv");
        Execute_Command_In_Pipe(" cat /tmp/polycas_parsed.gv | dot -Tpng -o /tmp/polycas_parsed.png &>/dev/null && chromium --incognito /tmp/polycas_parsed.png &>/dev/null" );
        return in;
    }


    //--------------------------- Nodes which will reduce to numbers -------------------------
    //At this point, we cycle through the children and attempt to convert them all to numbers.
    // If we find any which won't convert, we immediately terminate. We use recursion because
    // most nodes will depend on children being converted to numbers first.
    std::vector<T> vals;
    for(auto it = in->Children.begin(); it != in->Children.end(); ++it){
        //Lazily-implemented lazy evaluation for certain functions.
        if( !vals.empty() && (in->Type == NodeType::Func) ){
            const auto cname = Canonicalize_String2(in->Raw_Expr, CANONICALIZE::TO_UPPER);
            if(cname == "IF"){
                const auto childnum = std::distance(in->Children.begin(), it);
                if( ((vals[0] == (T)(0)) && (childnum == 1)) || ((vals[0] != (T)(0)) && (childnum == 2)) ){
                    vals.push_back(std::numeric_limits<double>::infinity()); //The value shouldn't matter...Just putting something suspicious in case of failure.
                    continue;
                }

            }else if(cname == "OR"){
                const auto childnum = std::distance(in->Children.begin(), it);
                if( (vals[0] != (T)(0)) && (childnum == 1) ){
                    vals.push_back(std::numeric_limits<double>::infinity()); //The value shouldn't matter...Just putting something suspicious in case of failure.
                    continue;
                }
     
            }

            //...any others here...
        }

        //Recurse on children.
        double l_result(0.0);
        bool l_OK(false);
        *it = Local_Use_Numerical_Basic(std::move(*it), &l_OK, &l_result);
        if(l_OK == true){
            vals.push_back(l_result);
        }else{
            //If we get here, this child cannot be converted to a number. This means the 
            // present node will also not be able. We are forced to bail unsuccessfully.
            return in;
        }
    }

    //Based on the NodeType, we perform a specific action on the numerical values.
    if(in->Type == NodeType::Func){
        *result = Local_Use_Built_In_Function_Evaluation(in->Raw_Expr, vals, OK);

    }else if(in->Type == NodeType::FuncAdd){
        *result = static_cast<T>(0);
        for(auto it = vals.begin(); it != vals.end(); ++it) *result += *it;

    }else if(in->Type == NodeType::FuncSubtract){
        *result = vals[0];
        for(auto it = ++(vals.begin()); it != vals.end(); ++it) *result -= *it;

    }else if(in->Type == NodeType::FuncNegate){
        *result = -vals[0];

    }else if(in->Type == NodeType::FuncMultiply){
        *result = vals[0];
        for(auto it = ++(vals.begin()); it != vals.end(); ++it) *result *= *it;

    }else if(in->Type == NodeType::FuncDivide){
        *result = vals[0];
        *result /= vals[1];

    }else if(in->Type == NodeType::FuncExponentiate){
        *result = pow(vals[0], vals[1]);

    }else if(in->Type == NodeType::FuncLessThan){
        *result = ((vals[0] < vals[1]) ? 1.0 : 0.0);

    }else if(in->Type == NodeType::FuncMoreThan){
        *result = ((vals[0] > vals[1]) ? 1.0 : 0.0);

    }else if(in->Type == NodeType::FuncLessThanEq){
        *result = ((vals[0] <= vals[1]) ? 1.0 : 0.0);

    }else if(in->Type == NodeType::FuncMoreThanEq){
        *result = ((vals[0] >= vals[1]) ? 1.0 : 0.0);

    }else{
        FUNCERR("Encountered an unknown node type. Unable to perform numerical action. See source for info");
        //NOTE: There is currently some redundancy in the layout of numerical and simplificatin routines.
        //      It is a little dangerous, but it will take a little time to port the numerical routines
        //      into the autogen simplification style (or even 'native' simplification routines.
        // 
        //      At the moment, numerical evaluation is (somewhat) independant of the simplification rules.
        //      This means that if you introduce new nodes, you'll need to emulate their numerical reduction
        //      in this routine!
        //
        //      Also, if you change something in the simplification routines (say, 'true' becomes 0 while
        //      'false' becomes -1) then you'll need to reflect that change here. Otherwise, you'll get weird
        //      and contradictory results!
    }

    *OK = true;
    return in;
}


template <class T>
std::unique_ptr<BaseNode> Numerical_Basic(std::unique_ptr<BaseNode> base, bool *OK, T *result){
    //This routine performs basic numerical evaluation. It will attempt to evaluate whatever it can and 
    // should not fail if something cannot be evaluated numerically (but it should pass this info on 
    // to the user). 
    //
    //NOTE: ONLY scalar, native types are supported. A more careful routine should handle arbitrary precision types.
    //      This means that everything is first converted to type T (double, float, long double, etc..) PRIOR to eval.
    //
    //NOTE: This routine should NOT alter a tree passed in. All numerical rules are INDEPENDENT of the
    //      simplification rules!
    //
    //NOTE: The bool will indicate whether or not the evaluation was successful (true for yes.)
    //      -Upon success, "result" will hold the numerical result.
    //      -Upon failure, "result" will be invalid.
    //      -Make sure to send it in as 'true' initially.
    //
    //NOTE: Whether or not this routine should emit nan can be set at compile time. Although it is
    //      a valid floating-point number, this is a 'basic' numerical routine. The default should 
    //      probably be to disallow nans and infs.
    //
    //NOTE: If INF and NAN are disallowed, we *may* still allow them in unevaluated branches. 
    //      This is (possibly) lazy evaluation and might happen with, say, 'IF' functions.
    //----------------------------------------------------------------------------------------------
    const bool Allow_INF_NAN(false);

    if(base   == nullptr) FUNCERR("Unable to evaluate expression numerically - invalid tree passed in");
    if(OK     == nullptr) FUNCERR("Unable to evaluate expression numerically - invalid bool passed in");
    if(result == nullptr) FUNCERR("Unable to evaluate expression numerically - invalid output container passed in");

    *OK     = true;
    *result = static_cast<T>(0);

    base = Local_Use_Numerical_Basic<T>(std::move(base), OK, result);

    //Check for infs and nans.
    if(!Allow_INF_NAN && *OK){
        if(!std::isfinite(*result)) *OK = false;
    }

    return base; //Local_Use_Numerical_Basic<T>(std::move(base), OK, result);
}




#endif
