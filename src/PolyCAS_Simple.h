//PolyCAS_Simple.h.

#ifndef POLYCAS_SIMPLE_H
#define POLYCAS_SIMPLE_H

#include <list>
#include <string>
#include <memory>


double PolyCAS_Eval(bool *wasOK, const std::string &expr, const std::list<std::string> &subs);

#endif
