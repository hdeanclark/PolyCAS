//Substitution.h - Routines for mixing/injecting parsing trees.

#ifndef PROJECT_POLYCAS_SUBSTITUTION_H_HDR_GRD_
#define PROJECT_POLYCAS_SUBSTITUTION_H_HDR_GRD_

#include <iostream>
#include <string>
#include <memory>
#include <list>

#include "YgorMisc.h"

#include "PolyCAS_Structs.h"

#define SUBSTITUTION_H_VERBOSE false



std::unique_ptr<BaseNode> Substitute_Basic(std::unique_ptr<BaseNode> base, std::list<std::unique_ptr<BaseNode>> &subs){
    //This routine takes the first tree (the first pointer in the list) and attempts to locate instances
    // of the remaining nodes (of which all must have a top-level ContAssignmentNode) in the first to substitute.
    //
    //This functions *copies* the subs and does a literal replace, discarding the previous node. 
    //
    //This function will require an identical tree for a match (with no wildcards!)
    //
    //Substitution occurs from left to right in the input list. This means that nested substitution must be
    // declared from left to right (e.g. [x]=[y+z], [y]=[z+3], [z]=[2] would yield [x]=[[2+3]+2].
    //
    //NOTE: The substitution is NOT repeated. This means that [x] can be replaced with [x+2], which
    // I believe to be more useful. A recursive routine *could* be built, even on top of this routine, though.
    //

    //First, we verify the basics. We should have zero or more substitution nodes.
    if(subs.empty()) return base;

    if(base == nullptr) FUNCERR("Attempted substitution on an invalid node!");

    //Check if the top node in the subs is an equality node.
    for(auto it = subs.begin(); it != subs.end(); ++it){
        if((*it)->Type != NodeType::ContAssignment){
            FUNCWARN("This routine requires all subs to have a top-level ContAssignmentNode. No substitution performed");
            return base;
        }
    }

    //Now, check against the LHS of the substitutions (using the == operator.) If a match is found, do NOT 
    // recurse on the children (otherwise we cannot perform [x]->[x+2] because it will loop infinitely!)
    bool Subbed = false;

    for(auto it = subs.begin(); it != subs.end(); ++it){
        const auto RHSit = (*it)->Children.rbegin();  //The part on the RHS of the ContAssignmentNode.
        const auto LHSit = (*it)->Children.begin(); 
        if( (*base) == (**LHSit) ){
            Subbed = true;

            // ... replace the node "base" with a COPY of the RHS of the sub...
            auto RHSdup = (*RHSit)->Duplicate();
            base = std::move( RHSdup );

            //Do NOT break...
        }
    }
    
    if(Subbed) return base; //Do not recurse on the (newly-subbed) children.

    //Recurse on the children.
    for(auto it = base->Children.begin(); it != base->Children.end(); ++it){
        (*it) = Substitute_Basic(std::move(*it), subs); 
    }

    return base;
}





#endif
