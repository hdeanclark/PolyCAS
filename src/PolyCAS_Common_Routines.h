//Common_Routines.h - A collection of commonly-required routines which appear.
//    These routines should all be inlined. This is to indicate they are typically small
//    and to avoid multiple definitions spread across a code base (i.e. to avoid linking errors.)
    

#ifndef PROJECT_POLYCAS_COMMON_ROUTINES_H_HDR_GRD_
#define PROJECT_POLYCAS_COMMON_ROUTINES_H_HDR_GRD_

#include <string>
#include <memory>
#include <list>

#include "PolyCAS_Structs.h"

#define COMMON_ROUTINES_H_VERBOSE false


//--------------------------------- Basic Convenience Functions --------------------------------------
inline bool Terminate_If_Null(const BaseNode *in){
    //This function returns true if in is not ==nullptr. It is used for chaining logical 
    // statements together. It will never get a chance to return false!
    //
    //It may seem weird to bother returning a bool. Check GraphRewriter/Autogen* for usage!
    if(in == nullptr) FUNCERR("Encountered a null pointer");
    return true;
}


//------------------------------------ STL-Container Helpers ----------------------------------------
inline void Append_Node_To_List(std::list<std::unique_ptr<BaseNode>> &thelist, std::unique_ptr<BaseNode> thenode){
    thelist.push_back(nullptr);
    (*(thelist.rbegin())) = std::move(thenode);
    return;
}

inline void Insert_Node_To_List_After_Number(std::list<std::unique_ptr<BaseNode>> &thelist, const long int i, std::unique_ptr<BaseNode> thenode){
    if(!isininc(0, i, static_cast<long int>(thelist.size())-1 )){
        FUNCERR("Attempted to insert node into list at a point which does not exist");
    }
    auto c_it = thelist.begin();
    std::advance(c_it, i+1);

    auto new_it = thelist.insert(c_it, nullptr);
    new_it->swap( thenode );
    return;
}



//------------------------------------- Basic Node Factories -----------------------------------------
template <class T>
inline std::unique_ptr<BaseNode> Node_Factory(void){
    std::unique_ptr<BaseNode> out = std::make_unique<T>();
    return out;
}

template <class T>
inline std::unique_ptr<BaseNode> Node_Factory(const std::string &shuttle){
    auto out = Node_Factory<T>();
    out->Raw_Expr = shuttle;
    return out;
}

template <class T, class R>
inline std::unique_ptr<BaseNode> Node_Factory(const std::string &funcname, const std::string &shuttle){
    auto out = Node_Factory<T>();
    out->Raw_Expr = funcname;

    out->Children.push_back(nullptr);
    (*(out->Children.rbegin())) = Node_Factory<R>(shuttle);
    return out;
}

//---------------------------------- Numerical Node Factories ---------------------------------------
template <class T, class R>
inline std::unique_ptr<BaseNode> Numerical_Node_Factory(R in){   //T might be (ExprRationalNode) and R might be (long int).
    //T *actual = new T(in);
    //std::unique_ptr<BaseNode> out = nullptr;
    //out.reset( static_cast<BaseNode*>( actual ) );
    std::unique_ptr<BaseNode> out = std::make_unique<T>(in);
    return out;
}


//---------------------------------- Numerical Node Routines ----------------------------------------
template <class R>
inline std::unique_ptr<BaseNode> Add_To_Numerical(std::unique_ptr<BaseNode> in, R x){
    bool Worked = false;
    if(      auto *casted = dynamic_cast<ExprRationalNode *>(in.get())){
        casted->x += x; 
        Worked = true;
    }else if(auto *casted = dynamic_cast<ExprDoubleNode *>(in.get())){
        casted->x += static_cast<double>(x); 
        Worked = true;
    }

    if(Worked != true){
        FUNCERR("Unable to perform numerical operation - this is not a numerically-castable node type!");
    }
    return in;
}

template <class R>
inline std::unique_ptr<BaseNode> Multiply_To_Numerical(std::unique_ptr<BaseNode> in, R x){
    bool Worked = false;
    if(      auto *casted = dynamic_cast<ExprRationalNode *>(in.get())){
        casted->x *= x;
        Worked = true;
    }else if(auto *casted = dynamic_cast<ExprDoubleNode *>(in.get())){
        casted->x *= static_cast<double>(x);
        Worked = true;
    }

    if(Worked != true){
        FUNCERR("Unable to perform numerical operation - this is not a numerically-castable node type!");
    }
    return in;
}

template <class R>
inline bool Numerical_Value_Is_Equal_To(const BaseNode* in, R x){  //This routine requires a raw pointer in order to return a bool!
    if(auto *casted = dynamic_cast<const ExprRationalNode *>(in))  return (casted->x == x);
    if(auto *casted = dynamic_cast<const ExprDoubleNode   *>(in))  return (casted->x == x);

    FUNCERR("Unable to perform numerical comparison - this is not a numerically-castable node type!");
    return false;
}

template <class R>
inline bool Numerical_Value_Is_Less_Than(const BaseNode* in, R x){  //This routine requires a raw pointer in order to return a bool!
    if(auto *casted = dynamic_cast<const ExprRationalNode *>(in))  return (casted->x < x);
    if(auto *casted = dynamic_cast<const ExprDoubleNode   *>(in))  return (casted->x < x);

FUNCWARN("Encountered a non-numerical node of type " << in->Type);
    FUNCERR("Unable to perform numerical comparison - this is not a numerically-castable node type!");
    return false;
}

inline bool Numerical_Values_Equal(const BaseNode* A, const BaseNode* B){  //This routine requires raw pointers in order to return a bool!
    //This function returns (A->x == B->x). Because we are mixing numerical types, we need to 
    // be aware of which node is placed first. The proper operator overloading needs to exist!
    if(    auto *castedA = dynamic_cast<const ExprRationalNode *>(A)){
        if(auto *castedB = dynamic_cast<const ExprRationalNode *>(B))  return (castedA->x == castedB->x);
        if(auto *castedB = dynamic_cast<const ExprDoubleNode   *>(B))  return (castedA->x == castedB->x);
    }
    if(    auto *castedA = dynamic_cast<const ExprDoubleNode   *>(A)){
        if(auto *castedB = dynamic_cast<const ExprDoubleNode   *>(B))  return (castedA->x == castedB->x);
        if(auto *castedB = dynamic_cast<const ExprRationalNode *>(B))  return (castedB->x == castedA->x); // <-- oddball case.
    }  
 
    FUNCERR("Unable to perform numerical comparison - this is not a numerically-castable node type!");
    return false;
}   

inline bool Numerical_Values_Less_Than(const BaseNode* A, const BaseNode* B){  //This routine requires raw pointers in order to return a bool!
    //This function returns (A->x < B->x). Because we are mixing numerical types, we need to 
    // be aware of which node is placed first. The proper operator overloading needs to exist!
    if(    auto *castedA = dynamic_cast<const ExprRationalNode *>(A)){
        if(auto *castedB = dynamic_cast<const ExprRationalNode *>(B))  return (castedA->x < castedB->x);
        if(auto *castedB = dynamic_cast<const ExprDoubleNode   *>(B))  return (castedA->x < castedB->x);
    }
    if(    auto *castedA = dynamic_cast<const ExprDoubleNode   *>(A)){
        if(auto *castedB = dynamic_cast<const ExprDoubleNode   *>(B))  return (castedA->x < castedB->x);
        if(auto *castedB = dynamic_cast<const ExprRationalNode *>(B))  return (castedB->x > castedA->x); // <-- oddball case.
    }
    FUNCERR("Unable to perform numerical comparison - this is not a numerically-castable node type!");
    return false;
}



template <class T, class R>
inline std::unique_ptr<BaseNode> Get_Numerical_Member_x(std::unique_ptr<BaseNode> in, R *x){
    //This routine casts a BaseNode pointer to a T pointer in order to pull out an R-casted member ->x.
    // T might be (ExprDoubleNode) and R might be (double).
    if(auto *actual = dynamic_cast<T *>( in.get() )){ //This statement should evaluate true.
        *x = static_cast<R>(actual->x);
        return in;
    }
    FUNCERR("Unable to perform numerical operation - this is not a numerically-castable node!");
    return in;
}

template <class T>
inline decltype(((T*)(nullptr))->x) Get_Numerical_Member_x(const BaseNode *in){  //This routine requires a raw pointer in order to return the value!
    if(auto *casted = dynamic_cast<const T *>(in)){
        return (casted->x);
    }
    FUNCERR("Unable to cast node to type T / Type T doesn't have member 'x'!");
    return static_cast<decltype( ((T*)(nullptr))->x )>(0);
}

//---------------------------------------- Node Alchemy ---------------------------------------------
//This is typically used with FuncNegateNode and other unary ops.
template <class T>
inline std::unique_ptr<BaseNode> Wrap_Node_With(std::unique_ptr<BaseNode> in){
    auto out = Node_Factory<T>();
    out->Children.push_back(nullptr);
    (*(out->Children.rbegin())) = std::move(in);
    return out;
}

template <class R, class T>
inline std::unique_ptr<T> Cast_From_To_Node(std::unique_ptr<R> in){  
   //Going forward, R will usually be BaseNode and T will probably be numerical. 
   //
   //Note: This routine should not be used to *convert* nodes, only to cast them (temporarily.)
   //      In particular, the node's type will not be altered by this function. See conversion
   //      routine "Turn_Node_To<...>" below.
   std::unique_ptr<T> out = nullptr;
   out.reset( static_cast<T*>(in.release()) );
   return out;
}


template <class T>
inline std::unique_ptr<BaseNode> Turn_Node_To(std::unique_ptr<BaseNode> in){
    //This routine 'converts' a node from one type to another. This is not a cast! (See above for casts.)
    // In particular, members are copied. If either the incoming or outgoing node types has a non-derived
    // member, then the node conversion fails.
    auto out = Node_Factory<T>();

    //Check if either node has a specialized payload. If so, error! (How can we guarantee transfer of the payload from old to T type?)
    bool HasSpecialPayload = false;
    if(      dynamic_cast<const ExprRationalNode *>(in.get())){   HasSpecialPayload = true;
    }else if(dynamic_cast<const ExprDoubleNode *>(in.get())){     HasSpecialPayload = true;
    }else if(dynamic_cast<const ExprRationalNode *>(out.get())){  HasSpecialPayload = true;
    }else if(dynamic_cast<const ExprDoubleNode *>(out.get())){    HasSpecialPayload = true;
    }
    if(HasSpecialPayload == true){
        FUNCERR("Attempted to convert node to/from a type with a non-derived member. This routine CANNOT handle this term!");
        // Advice if you see this: consider making a duplicate of the special node instead of performing alchemy!
    }
   
    //Back to business: copy over all the derived members (except the type, which will probably be different.)
    out->Raw_Expr = in->Raw_Expr;
    for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it){
        out->Children.push_back(nullptr);
        (*(out->Children.rbegin())) = std::move(*c_it);
    }

    return out;
}



#endif
