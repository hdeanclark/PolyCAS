//Numerical_Conversions.h - A set of routines which are used to convert between the various
//                          numerical nodes types and built-in numerical types.

#ifndef PROJECT_POLYCAS_NUMERICAL_CONVERSIONS_H_HDR_GRD_
#define PROJECT_POLYCAS_NUMERICAL_CONVERSIONS_H_HDR_GRD_

#include <iostream>
#include <cmath>
#include <string>
#include <memory>

#include <gmpxx.h>       //Needed for ExprRational and ExprDouble (numerical) types.

#include <boost/spirit/include/qi.hpp>
//#include <boost/spirit/include/phoenix_core.hpp>
//#include <boost/spirit/include/phoenix_operator.hpp>

#include "PolyCAS_Structs.h"

#define NUMERICAL_CONVERSIONS_H_VERBOSE true


//---------------------------------------- String to T Conversions -------------------------------------------

//This routine uses Boost's Spirit/Qi parser to quickly convert from a string to type T. It supports
// built-in types only (as far as I can tell.)
//
//Note: Returns 'true' on success, 'false' on failure.
template <class T>
bool Convert_String_To_Type(std::string::const_iterator begin, std::string::const_iterator end, T &x){ 
    return boost::spirit::qi::parse(begin, end, boost::spirit::qi::create_parser<T>(), x);
}

template <class T>
bool Convert_String_To_Type(const std::string &in, T &x){   // This is a wrapper for the previous.
    return Convert_String_To_Type(in.begin(), in.end(), x);
}

//This routine uses GNU GMP's mpq_class (rational numbers) .set_str method. Only lossless conversions take
// place (as far as I can tell.) This includes some scientific notation, like "1.03E5". Lossless conversions
// (i.e. floats with fractional parts) will NOT convert.
//
//Note: To use this number for numeric purposes, one must always .canonicalize() it after this routine!
template<>
bool Convert_String_To_Type(const std::string &in, mpq_class &x){   // This is a template specialization of the previous.
    return ( 0 == x.set_str(in, 10) );
}


//---------------------------------------- Double to T Conversions -------------------------------------------



static mpq_class Convert_Double_To_Rational(double x, long int depth){
    double intpart;
    double power = pow(10.0,depth);
    double fractpart = modf(x*power, &intpart);
    if(RELATIVE_DIFF(fractpart, 0.0) < 1E-6){
        return mpq_class(static_cast<long int>(x*power), power);
    }
    return Convert_Double_To_Rational(x, depth+1);
}

mpq_class Convert_Double_To_Rational(double x){
    if(x == 0.0){
        return mpq_class(0,1);
    }else{
        const int sign = x < 0.0 ? -1 : 1;
        return sign*Convert_Double_To_Rational(sign * x, 0);
    }
}



/*
static mpq_class Convert_Double_To_Rational(double x, double limit, int iterations){
    double intpart;
    double fractpart = modf(x, &intpart);
    double d = 1.0 / fractpart;
    long int left = static_cast<long int>(intpart);
    if((d > limit) || (iterations == 0)){
        return mpq_class(left);
    }else{
        mpq_class temp(left);
        mpq_class temp2(Convert_Double_To_Rational(d, limit*0.1, --iterations));
        mpq_class temp3( temp2.get_den(), temp2.get_num() ); // i.e. 1/temp2.
        return temp + temp2;
//        return mpq_class(left) + Convert_Double_To_Rational(d, limit * 0.1, --iterations).invert();
    }
}

mpq_class Convert_Double_To_Rational(double x, int iterations){
    if(x == 0.0){
        return mpq_class(0,1);
    }else{
        const int sign = x < 0.0 ? -1 : 1;
        return sign*Convert_Double_To_Rational(sign * x, 1E12, iterations);
    }
}
*/


/*
static Rational toRational(double x, double limit, int iterations) {
  double intpart;
  double fractpart = modf(x, &intpart);
  double d = 1.0 / fractpart;
  long left = long(intpart);
  if ( d > limit || iterations == 0 ) {
    return Rational(left);
  } else {
    return Rational(left) + toRational(d, limit * 0.1, --iterations).invert();
  }
}

Rational toRational(double x, int iterations) {
  if ( x == 0.0 ||
       x < numeric_limits<long>::min() ||
       x > numeric_limits<long>::max() ) {
    return Rational(0,1);
  } else {
    int sign = x < 0.0 ? -1 : 1;
    return sign * toRational(sign * x, 1.0e12, iterations);
  }
}
*/

#endif
