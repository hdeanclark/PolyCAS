//PolyCAS_Fitting.h.

#ifndef POLYCAS_FITTING_H
#define POLYCAS_FITTING_H

#include <list>
#include <string>
#include <tuple>
#include <memory>

//This is pretty much a class shoved into a single function.
// Maybe a class would be better...
std::tuple<std::list<std::string>,double,double,long int,double,double,double,std::list<double>> 
PolyCAS_Fit_Data(bool *wasOK, 
                 const std::string &f, 
                 const std::list<std::list<double>> &data, 
                 const std::list<std::string> &vars, 
                 const std::string &F_data, 
                 const std::string &uncertainties,
                 const std::list<std::string> &params,
                 bool Verbose = false, 
                 double char_len = 0.3, 
                 int max_iters = 450, 
                 double ftol = 1E-6 );


//Plots image. Should this be fed through YgorPlot?
//void PolyCAS_Plot_Fit_Results( std::tuple<std::list<std::string>,double,long int,double,double,double> in );


//Writes a gnuplot-able file to the desired location. 
bool PolyCAS_Write_Fit_Results_As_Plottable_File(
                 const std::string &f,
                 const std::list<std::list<double>> &data,
                 const std::list<std::string> &vars,
                 const std::string &F_data,
                 const std::string &uncertainties,
                 const std::list<std::string> &params,
                 std::tuple<std::list<std::string>,double,double,long int,double,double,double,std::list<double>> res,
                 const std::string &Filename,
                 const std::string &Title = "" );


#endif
