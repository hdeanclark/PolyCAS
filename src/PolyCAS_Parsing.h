//Parsing.h - A set of parsing routines used to convert std::strings into parse trees.

#ifndef PROJECT_POLYCAS_PARSING_H_HDR_GRD_
#define PROJECT_POLYCAS_PARSING_H_HDR_GRD_

#include <iostream>

#include <string>
#include <memory>
#include <vector>

#include <gmpxx.h>     //Needed for ExprRational and ExprFloat (numerical) types.

//#include <boost/spirit/include/qi.hpp>    //Needed for (quickly) converting strings to doubles.
//#include <boost/spirit/include/phoenix_core.hpp>
//#include <boost/spirit/include/phoenix_operator.hpp>

#include "YgorMisc.h"
#include "YgorString.h"  //Several small text routines are used from here.

#include "PolyCAS_Structs.h"
#include "PolyCAS_Common_Routines.h"
#include "PolyCAS_Numerical_Conversions.h"

#include "PolyCAS_Simplify.h"

#define PARSING_H_VERBOSE false


std::unique_ptr<BaseNode> Internal_Use_Parse_String(std::unique_ptr<BaseNode> in){

    //First, check if there is anything to parse. If this is a simple expression, then recurse on the children and return.
    if(in->Type != NodeType::Shuttle){
        for(auto it = in->Children.begin(); it != in->Children.end(); ++it){
            *it = Internal_Use_Parse_String( std::move( *it ) );
        }
        return in;
    }

    //Check to ensure there is something to parse.
    if(in->Raw_Expr.empty()) FUNCERR("Unable to parse - there is no Raw_Expr");

    //------------------------------------ Syntax Checking ----------------------------------------
    //Search for unmatched parenthesis.
    if(Contains_Unmatched_Char_Pairs(in->Raw_Expr, '(', ')')) FUNCERR("Unmatched () in node: '" << in->Raw_Expr << "'");
    if(Contains_Unmatched_Char_Pairs(in->Raw_Expr, '[', ']')) FUNCERR("Unmatched [] in node: '" << in->Raw_Expr << "'");
    if(Contains_Unmatched_Char_Pairs(in->Raw_Expr, '{', '}')) FUNCERR("Unmatched {} in node: '" << in->Raw_Expr << "'");

    //Search for unbalanced parenthesis.

    // ...

    //--------------------------------------- Reduction --------------------------------------------
    //Reduce redundant brackets like "((((x))))". 
    in->Raw_Expr = Remove_Unneeded_Surrounding_Parenthesis(in->Raw_Expr, '(', ')');
    if(PARSING_H_VERBOSE) FUNCINFO("Proceeding parsing expression '" << in->Raw_Expr << "'");

    //---------------------------------------- Scanning --------------------------------------------
    std::list<std::unique_ptr<BaseNode>> tokens;
    std::list<std::unique_ptr<BaseNode>> ops;

    std::string shuttle, funcname;
    long int parendepth(0), numofunaryops(0);
    bool mostrecentnodewasanop = true; //Must be true to bootstrap handling negation (and other unary ops?) nodes properly...

    for(auto it = in->Raw_Expr.begin(); it != in->Raw_Expr.end(); ++it){
        if(PARSING_H_VERBOSE) FUNCINFO("Pushing back character '" << *it << "' now.");

        //Check if we just entered a deeper level of parenthesis depth.
        if( *it == '('){
            //If we have non-operator characters immediately preceeding this, we have a function. Store the name for later.
            if( (parendepth == 0) && !shuttle.empty() ){ 
                funcname = shuttle;
                shuttle.clear();
            }
            ++parendepth;
            shuttle += *it;
            continue;
        }

        shuttle += *it;

        //Check if we just left a deeper level of parenthesis depth.
        if(*it == ')'){
            --parendepth;
            if(parendepth == 0){
                if(shuttle.empty()) FUNCERR("Parsing error: attempted to create node with empty shuttle. (1)");
                //It is very easy to perform wrapping around the function argument commas. We explicitly do this
                // so that we can adjust the generic comma operator precedence (if we wanted to for eg. Lie brackets).
                shuttle = Wrap_Comma_Separated_Stuff_At_Same_Depth(shuttle, '(', ')');
//-----------------
                if(PARSING_H_VERBOSE) FUNCINFO("Making a node (1) with funcname '" << funcname << "' and shuttle '" << shuttle << "'");
                if(!funcname.empty()){
                    auto newnode = Node_Factory<FuncNode, ShuttleNode>(funcname, shuttle);
                    Append_Node_To_List(tokens, std::move(newnode) );
                }else{
                    auto newnode = Node_Factory<ShuttleNode>(shuttle);
                    Append_Node_To_List(tokens, std::move(newnode) );
                }
                shuttle.clear();
                funcname.clear();
                mostrecentnodewasanop = false;
//-----------------
                continue;
            }
        }
        
        auto it2 = it;
        ++it2;
        if(!(it2 != in->Raw_Expr.end())){
            if(parendepth != 0) FUNCERR("Incorrectly parsed parenthesis in expression!"); //(Already checked parenthesis matched...?)
            if(shuttle.empty()) FUNCERR("Parsing error: attempted to create node with empty shuttle (at the end of the expression) (2)");
//-----------------
            if(PARSING_H_VERBOSE) FUNCINFO("Making a node (2) with funcname '" << funcname << "' and shuttle '" << shuttle << "'");
            if(!funcname.empty()){
                auto newnode = Node_Factory<FuncNode, ShuttleNode>(funcname, shuttle);
                Append_Node_To_List(tokens, std::move(newnode) );
            }else{
                auto newnode = Node_Factory<ShuttleNode>(shuttle);
                Append_Node_To_List(tokens, std::move(newnode) );
            }
            shuttle.clear();
            funcname.clear();
            mostrecentnodewasanop = false;
//-----------------
            //Loop terminates now.
            break; //This break necessary?
        }

        if(parendepth != 0) continue;

        //---------- Two-Char Unary operators --------------
        //These are not yet implemented, but we nevertheless try to catch them.
        if((*it == '-') && (std::next(it) != in->Raw_Expr.end()) && (*std::next(it) == '-')){
            //Two possibilities: this is a prefix (--x) or suffix (x--). Check for either by examining the shuttle contents.
            FUNCERR("operators '--x' and 'x--' have not been implemented");
        }

        if((*it == '+') && (std::next(it) != in->Raw_Expr.end()) && (*std::next(it) == '+')){
            //Two possibilities: this is a prefix (++x) or suffix (x++). Check for either by examining the shuttle contents.
            FUNCERR("operators '++x' and 'x++' have not been implemented");
        }


        //----------- Unary operators. --------------------
        //if((*it == '-') && (shuttle == "-")){  //Will fail on '(a+b)-c' because it won't see the (a+b) part...
        //if((*it == '-') && (shuttle == "-") && tokens.empty()){  //Will fail on 'a^-b^c'
        if((*it == '-') && (shuttle == "-") && (mostrecentnodewasanop == true)){
            shuttle.erase( shuttle.begin() + shuttle.size() - 1 );

            //Push back a unary operation.

            if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type NegateNode");
            auto newnode = Node_Factory<FuncNegateNode>();
            Append_Node_To_List(ops, std::move(newnode) );

            ++numofunaryops;
            mostrecentnodewasanop = true;
            continue;
        }

        if((*it == '+') && (shuttle == "+") && (mostrecentnodewasanop == true)){ 
            //This is a spurious symbol. For example: '1' == '+1'.

            //Option A - try to work around it without disrupting anything else.
            //shuttle.erase( shuttle.begin() + shuttle.size() - 1 );
            //if(PARSING_H_VERBOSE) FUNCINFO("Avoided making an op node by omitting spurious unary '+'");
            //mostrecentnodewasanop = true;
            //continue;

            //Option B - remove the offending spurious mark and reparse the entire string without it.
            // This is the slower/worse-on-stack option, but is probably safest/easiest. It seems to work
            // where option A doesn't.
            in->Raw_Expr.erase(it);
            return Internal_Use_Parse_String(std::move(in));
        }


        //----------- Binary operators. -------------------
        //If *it matches ANY of the operators.
        if(   (*it == '+') || (*it == '-') || (*it == '*')  // <--- includes "*" and "**" operators.
           || (*it == '/') || (*it == ',') || (*it == '^') || (*it == '=') 
           || (*it == '<') || (*it == '>')  ){  // <--- includes "<", ">", "<=", and ">=" operators.

            if(PARSING_H_VERBOSE) FUNCINFO("Going into the binary ops creation node section with shuttle = '" << shuttle << "'");

            //Check for scientific notation expressions:  e.g. 3E-
            {
              auto snit = ++(shuttle.rbegin()); //scientific notation it.
              if( (shuttle.size() >= 3) && ((*it == '+') || (*it == '-')) && ((*snit == 'E') || (*snit == 'e')) ){
                  ++snit;
                  if( isdigit(static_cast<int>(*snit)) || (*snit  == '.') ){ 
                      if(PARSING_H_VERBOSE) FUNCINFO("Intercepted an operator within an expression in scientific notation '" << shuttle << "'");
                      continue;
                  }
              }
            }

            //Erase the operation symbol from the shuttle (so we don't pass it on to the next level parser).
            if(!shuttle.empty()) shuttle.erase( shuttle.begin() + shuttle.size() - 1 );

            if(!shuttle.empty()){  //This if is required for handling, for instance, (...)+(...). (Shuttle may legitimately be empty!)
//-------------
                if(PARSING_H_VERBOSE) FUNCINFO("Making a node (3) with funcname '" << funcname << "' and shuttle '" << shuttle << "'");
                if(!funcname.empty()){
                    auto newnode = Node_Factory<FuncNode, ShuttleNode>(funcname, shuttle);
                    Append_Node_To_List(tokens, std::move(newnode) );
                }else{
                    auto newnode = Node_Factory<ShuttleNode>(shuttle);
                    Append_Node_To_List(tokens, std::move(newnode) );
                }
                shuttle.clear();
                funcname.clear();
                mostrecentnodewasanop = true;
//-------------
            }

            if(*it == '+'){
                if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type AddNode");
                auto newnode = Node_Factory<FuncAddNode>();
                Append_Node_To_List(ops, std::move(newnode) );
                mostrecentnodewasanop = true;
            }else if(*it == '-'){
                if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type SubtractNode");
                auto newnode = Node_Factory<FuncSubtractNode>();
                Append_Node_To_List(ops, std::move(newnode) );
                mostrecentnodewasanop = true;
            }else if(*it == '*'){
                //Check if the next character is '*' to handle '**' operator. If so, consume it.
                auto itt = it;
                ++itt;
                if((itt != in->Raw_Expr.end()) && (*itt == '*')){
                    ++it;
                    if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type ExponentiateNode-type1");
                    auto newnode = Node_Factory<FuncExponentiateNode>();
                    Append_Node_To_List(ops, std::move(newnode) );

                }else{
                    if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type MultiplyNode");
                    auto newnode = Node_Factory<FuncMultiplyNode>();
                    Append_Node_To_List(ops, std::move(newnode) );
                }
                mostrecentnodewasanop = true;

            }else if(*it == '/'){
                if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type DivideNode");
                auto newnode = Node_Factory<FuncDivideNode>();
                Append_Node_To_List(ops, std::move(newnode) );
                mostrecentnodewasanop = true;

            }else if(*it == '^'){
                if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type ExponentiateNode-type2");
                auto newnode = Node_Factory<FuncExponentiateNode>();
                Append_Node_To_List(ops, std::move(newnode) );
                mostrecentnodewasanop = true;

            }else if(*it == ','){
                if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type CommaNode");
                auto newnode = Node_Factory<ContCommaNode>();
                Append_Node_To_List(ops, std::move(newnode) );
                mostrecentnodewasanop = true;

            }else if(*it == '='){
                if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type AssignmentNode");
                auto newnode = Node_Factory<ContAssignmentNode>();
                Append_Node_To_List(ops, std::move(newnode) );
                mostrecentnodewasanop = true;

            }else if(*it == '<'){
                //Check if the next character is '=' to handle '<=' operator. If so, consume it.
                auto itt = it;
                ++itt;
                if((itt != in->Raw_Expr.end()) && (*itt == '=')){
                    ++it;
                    if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type LessThanEqNode");
                    auto newnode = Node_Factory<FuncLessThanEqNode>();
                    Append_Node_To_List(ops, std::move(newnode) );

                }else{
                    if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type LessThanNode");
                    auto newnode = Node_Factory<FuncLessThanNode>();
                    Append_Node_To_List(ops, std::move(newnode) );
                }
                mostrecentnodewasanop = true;

            }else if(*it == '>'){
                //Check if the next character is '=' to handle '>=' operator. If so, consume it.
                auto itt = it;
                ++itt;
                if((itt != in->Raw_Expr.end()) && (*itt == '=')){
                    ++it;
                    if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type MoreThanEqNode");
                    auto newnode = Node_Factory<FuncMoreThanEqNode>();
                    Append_Node_To_List(ops, std::move(newnode) );

                }else{
                    if(PARSING_H_VERBOSE) FUNCINFO("Making an op node (4) of type MoreThanNode");
                    auto newnode = Node_Factory<FuncMoreThanNode>();
                    Append_Node_To_List(ops, std::move(newnode) );
                }
                mostrecentnodewasanop = true;

            }else{
                FUNCERR("Unrecognized operator! This is a programming issue - simply need to implement the routine");
            }
        }

        //Must be part of a variable name or a number. Dump debugging info to screen to double check.
    }

    //Check if there are ANY operations found. If not, then this is most likely a lowly ExprNode, which 
    // cannot be further parsed. Indicate this.
    if(ops.empty() && ( (*(tokens.begin()))->Type == NodeType::Shuttle) ){
        auto newnode = Node_Factory<ExprNode>( (*(tokens.begin()))->Raw_Expr );
        (*(tokens.begin())) = std::move(newnode);
    }

    //-------- Order based on operator precedence. This routine is written/described in large notebook.
    std::map<unsigned long int, long int> precedence; // See http://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#Operator_precedence .

    //------------------------------------------------------------------------------------------------------
    precedence[ NodeType::ContComma ]        = 100; //Note: This is decoupled from function-argument comma precedence, which is made explicit by grouping.
                                                    //      It could be used for, say, Lie brackets or some other thing.
                                                    //      In C/C++, this would have less precendence than '='.
  
    precedence[ NodeType::ContAssignment ]   = 0; //This should always be the last operation performed. It will make it the top-level node in the tree.
    // +=
    // -=
    // *=
    // /=
    // &=
    // |=
    // ^=

    // ||

    // &&

    // |

    // ^

    // &

    // !=
    // ==

    precedence[ NodeType::FuncLessThan ]     = 2; //These logical operations should wait until 'just about' last chance.
    precedence[ NodeType::FuncLessThanEq ]   = 2; // They should be performed after common operations like addition, subtraction, etc..
    precedence[ NodeType::FuncMoreThan ]     = 2; // because a+b+c<d typically means (a+b+c)<d
    precedence[ NodeType::FuncMoreThanEq ]   = 2;

    // <<
    // >>

    precedence[ NodeType::FuncAdd ]          = 3;
    precedence[ NodeType::FuncSubtract ]     = 4;

    precedence[ NodeType::FuncDivide ]       = 6;
    precedence[ NodeType::FuncMultiply ]     = 6;
    // %

    precedence[ NodeType::FuncNegate ]       = 8;
    precedence[ NodeType::FuncExponentiate ] = 8;  //Difficult to place. Should be same level as negation, but treated separately.
    // +
    // ~
    // !
    // ++  (prefix)
    // --  (prefix)

    // ++  (suffix)
    // --  (suffix)

    //----------------------------------------------------------------------------------------------------------

    if(PARSING_H_VERBOSE) FUNCINFO("Entering the tree-construction routine now. We have (" << ops.size() << ") ops, (with " << numofunaryops << " of them unitary) and (" << tokens.size() << ") tokens");

    while(!ops.empty()){
        if(PARSING_H_VERBOSE) FUNCINFO("    Continued into another loop. We have (" << ops.size() << ") ops remaining and (" << tokens.size() << ") tokens");
        if(PARSING_H_VERBOSE) for(auto it = tokens.begin(); it != tokens.end(); ++it) FUNCINFO("        Token: '" << (*it)->Raw_Expr << "' with type = " << (*it)->Type);

        auto ops_highest = ops.begin();
        long int precedence_highest = -1;

        //For most operators, we prefer equal-precedence operations on the LHS. Assume this is true. Correct
        // later if we need to.
        for(auto o_it = ops.begin(); o_it != ops.end(); ++o_it){
            const unsigned long int thetype = (*o_it)->Type;

            //if( precedence[thetype] >= precedence_highest ){   //Prefer equal-precedence operations on the RHS.
            if( precedence[thetype] >  precedence_highest ){   //Prefer equal-precedence operations on the LHS.
                 precedence_highest = precedence[thetype];
                 ops_highest = o_it;
            }
        }

        //Check if we have chosen exponentiation. If so, prefer equal-precedence operations on the **RHS** (because
        // exponentiation prefers the RHS so that 2**3**4 == 2**(3**4)  and != (2**3)**4. 
        //
        //All this code does is (effectively) work the opposite direction along the ops container.
        //
        //Note: This wreaks havoc when mixing negation and exponentiation. We can get around this by bumping up negation's precedence
        // to be the same as exponentiation. In practice, this appears to not present any extra issues (because the negative often 
        // 'bubbles up' to the highest expression anyways..)
        if(precedence[(*ops_highest)->Type] == precedence[NodeType::FuncExponentiate]){
            for(auto o_it = ops.begin(); o_it != ops.end(); ++o_it){
                const unsigned long int thetype = (*o_it)->Type;

                if( precedence[thetype] >= precedence_highest ){   //Prefer equal-precedence operations on the RHS.
                //if( precedence[thetype] >  precedence_highest ){   //Prefer equal-precedence operations on the LHS.
                     precedence_highest = precedence[thetype];
                     ops_highest = o_it;
                }
            }
        }


        //If the highest-precedence op is a binary operation...
        if((*ops_highest)->Type != NodeType::FuncNegate){
            if(PARSING_H_VERBOSE) FUNCINFO("The most precedent routine was a binary op.");

            //Check to make sure that we have at least two tokens remaining.
            if(tokens.size() < 2){
                FUNCERR("Unable to perform binary operation - we 'ran out' of tokens. Is the expression: '" << in->Raw_Expr << "' valid?");
            }

            auto tokens_A = tokens.begin();
            auto tokens_B = ++(tokens.begin());

            for(auto it = ops.begin(); it != ops_highest; ++it){
                //If this node is a binary operation, increment the counter by 1. If unary, do not.
                if((*it)->Type != NodeType::FuncNegate){
                    if(PARSING_H_VERBOSE) FUNCINFO("Prior to iteration, token A points to the " << std::distance(tokens.begin(), tokens_A) << " token");
                    if(PARSING_H_VERBOSE) FUNCINFO("Prior to iteration, token B points to the " << std::distance(tokens.begin(), tokens_B) << " token");
                    ++tokens_A;
                    ++tokens_B;
                }
            }
 
            //We now have an operation in ops_highest (at position i), the ith token (A), and the (i+1)th token (B).
            // Push A and B into the operation's children. Place the operation at the place of the ith token.
            auto actual_op = std::move(*ops_highest);
            auto actual_A  = std::move(*tokens_A);
            auto actual_B  = std::move(*tokens_B);
    
            Append_Node_To_List(actual_op->Children, std::move( actual_A ) );
            Append_Node_To_List(actual_op->Children, std::move( actual_B ) );
    
            //Move the loaded function into the place of the first token.  
            *tokens_A = std::move(actual_op);
    
            //Remove the other (now invalid) token and the (now invalid) op.
            tokens.erase(tokens_B);
            ops.erase(ops_highest);

        //Otherwise, if the highest-precedence op is a unary operation...
        }else{
            if(PARSING_H_VERBOSE) FUNCINFO("The most precedent routine was a unary op.");

            //Check to make sure that we have at least one token remaining.
            if(tokens.size() < 1){
                FUNCERR("Unable to perform unary operation - we 'ran out' of tokens. Is the expression: '" << in->Raw_Expr << "' valid?");
            }

            auto tokens_A = tokens.begin();
            for(auto it = ops.begin(); it != ops_highest; ++it){
                //If this node is a binary operation, increment the counter by 1. If unary, do not.
                if((*it)->Type != NodeType::FuncNegate){
                    ++tokens_A;
                }
            }

            //We now have an operation in ops_highest (at position i) and the ith token (A).
            // Push A into the operation's children. Place the operation at the place of the ith token.
            auto actual_op = std::move(*ops_highest);
            auto actual_A  = std::move(*tokens_A);
 
            Append_Node_To_List(actual_op->Children, std::move( actual_A ) );
 
            //Move the loaded function into the place of the token.  
            *tokens_A = std::move(actual_op);
 
            //Remove the (now invalid) op.
            ops.erase(ops_highest);
        }
    }


    //Check that there is a single token remaining.
    if( tokens.size() != 1 ){
        FUNCERR("Parsing algorithm has extra tokens remaining, when there should only be one.");
    }

    //The remaining token will now take the place of the shuttle node we have been passed.
    in = std::move( *(tokens.begin()) );

    //Recurse on the children and return.
    for(auto it = in->Children.begin(); it != in->Children.end(); ++it){
        *it = Internal_Use_Parse_String( std::move( *it ) );
    }

    return in;
}


std::unique_ptr<BaseNode> Internal_Use_Convert_Rational_Exprs(std::unique_ptr<BaseNode> in){
    //First, check if this is an ExprNode. If not, then recurse on the children and return.
    if(in->Type != NodeType::Expr){
        for(auto it = in->Children.begin(); it != in->Children.end(); ++it){
            *it = Internal_Use_Convert_Rational_Exprs( std::move( *it ) );
        }
        return in;
    }

    //Bail on special NPC types.
    if((in->Raw_Expr == "inf") || (in->Raw_Expr == "infinity") || (in->Raw_Expr == "missingno")){
        return in;
    }

    //Exact conversions to rational numbers. Does not handle floats which hold lossy numbers.
    mpq_class x(0);
    if(Convert_String_To_Type<mpq_class>(in->Raw_Expr,x)){
        //We create a new node of ExprRationalNode type casted to BaseNode.
        x.canonicalize();
        auto newnode = Numerical_Node_Factory<ExprRationalNode, mpq_class>(x);
        return newnode;
    }

    //Attempted conversions from floating point numbers. This conversion IS lossy (but, imo, the 
    // least lossy technique possible for floats.)
    double y(0);
    if(Convert_String_To_Type<double>(in->Raw_Expr,y)){
        //Now attempt conversion of the double to a rational. This is (another) lossy step.
        mpq_class res = Convert_Double_To_Rational(y);
        res.canonicalize();
        auto newnode = Numerical_Node_Factory<ExprRationalNode, mpq_class>(res);
        return newnode;
    }

    return in;
}

std::unique_ptr<BaseNode> Internal_Use_Convert_Double_Exprs(std::unique_ptr<BaseNode> in){ //This assumes machine-precision doubles.
    //First, check if this is an ExprNode. If not, then recurse on the children and return.
    if(in->Type != NodeType::Expr){
        for(auto it = in->Children.begin(); it != in->Children.end(); ++it){
            *it = Internal_Use_Convert_Double_Exprs( std::move( *it ) );
        }
        return in;
    }

    //Bail on special NPC types.
    if((in->Raw_Expr == "inf") || (in->Raw_Expr == "infinity") || (in->Raw_Expr == "missingno")){
        return in;
    }

    //Check if the quantity is a (machine precision) double floating point number. If so, convert 
    // the given node into a ExprDoubleNode.
    double x;
    if(Convert_String_To_Type<double>(in->Raw_Expr,x)){
        //We create a new node of ExprDoubleNode type casted to BaseNode.
        auto newnode = Numerical_Node_Factory<ExprDoubleNode, double>(x);
        return newnode;
    }
    return in;
}

std::unique_ptr<BaseNode> Parse_String(const std::string &in, bool Simplify = true, bool PreferLossy = false){
    //The top parent node is a dummy node. We use a simple "shuttle" node (which is essentially a 
    // parenthesis node) which is designed to temporarily hold data.
    std::unique_ptr<BaseNode> out { new ShuttleNode };
    out->Raw_Expr = in;
    Canonicalize_String(out->Raw_Expr, CANONICALIZE::TRIM_ALL); //Remove ALL whitespace.

    //------------------------------------ Basic Parsing -----------------------------------
    //Pass the node to the basic, recursive node parser.
    out = Internal_Use_Parse_String( std::move(out) );

    //-------------------------------- Numerical Conversions -------------------------------
    //Lossless conversions. Try to convert (everything) to a rational number. Lossy conversion 
    // to rational is OK, because it (should) reduce loss during simplification, etc..
    // Rationals can (at the moment) be costly. GMP rationals sit on the heap, and every 
    // rational in the expression tree will necessarily introduce many other (temp) rationals.
    if(!PreferLossy) out = Internal_Use_Convert_Rational_Exprs( std::move(out) );

    //Lossy conversions. Try to avoid these if at all possible. 
    out = Internal_Use_Convert_Double_Exprs( std::move(out) );

    //---------------------------------- Simplifications -----------------------------------
    //Perform basic (lossless) simplifications. These include numerical simplifications. 
    // These simplifications can be very costly. They are on by default, but sometimes we 
    // just can't afford them (eg. rapid numerical evaluation).
    if(Simplify){
        out = Simplify_Basic_Lossless( std::move(out) );
    }else{
        out = Simplify_Basic_Structural( std::move(out) ); //The bare minimal simplifications.
    }

    return out;
}




#endif
