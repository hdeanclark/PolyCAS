//PolyCAS_Fitting.cc - Written by hal clark in 2013.
//
// This is a library which is used to fit a scalar-valued function f(x,y,z,...;A,B,C,...)
// to the provided data. It crucially requires function parsing and evaluation, and is 
// thus a part of PolyCAS, although it is probably a better fit in Ygor. The interface
// provided here was written quickly. Feel free to update it when a class which can 
// handle scalar function data with multiple variables (i.e. an extension of samples_1D)
// is around.
//

#include <iostream>
#include <list>
#include <string>
#include <memory>
#include <tuple>
#include <functional>
#include <memory>
#include <sstream>
#include <fstream>

#include "PolyCAS_Structs.h"
#include "PolyCAS_Parsing.h"
#include "PolyCAS_Numerical.h"
#include "PolyCAS_Simplify.h"
#include "PolyCAS_Substitution.h"

#include "PolyCAS_Fitting.h"

#include "YgorMisc.h"
#include "YgorString.h"
#include "YgorMath.h"
#include "YgorStats.h"
//#include "YgorFilesDirs.h"
//#include "YgorPerformance.h"
#include "YgorAlgorithms.h"


//This function will either be successful, or it will fail and indicate so by setting wasOK to false.
// If successful, many statistical quantities will be returned. If an error occured, the output is 
// not defined and should not be used or examined.
//
//Input is not consumed. All data is copied, if necessary.
//
//NOTE: This function likely uses nelder mead simplexes. In the future, more sophisticated techniques
// may be implemented.
//NOTE: The columns of data are zero-index based.
//
// Input:
//     - (bool *) Error indicator.
//
//     - (string) Unnamed function. Should not contain any '=' and should be ready to evaluate.
//           Example: 'A*x+B*y+C', 'slope*avar+intercept', '($0)*A-($1)*B', ...
//
//     - (list of list of numbers) The raw data. Not all of which will necessarily be used. All data
//       should be of equal number of columns, even if this means you've padded it...
//
//     - (list of strings) Variables to fill with data from each row. These are subs which designate
//       which columns to pull data from.
//           Example: 'x=$0', 'y=$1/2.0', 'z=if($2>12,1/0,$2)', ...
//
//     - (string) The signal to use for the function's value. Has access to the entire row.
//           Example: '$2', 'min($3,1.0)', ...
//
//     - (string) The signal to use for the uncertainty of a given point. Has access to the entire row.
//           Example: '1.0', '$3', 'exp(-1.23*($3-0.15)^2)', ...
//           Note: A reasonable estimate here is crucial for a meaningful chi-square! See inline notes.
//           Note: A decent strategy is to perform a fast-and-dirty unweighted fit first to find suitable
//            initial guesses, and then perform a more careful fit (with less emphasis on outliers) using 
//            those parameters. An example of this is in Tests/Test_Fitting_1.cc at time of writing.
//           Note: Instead of an expression, names can be passed in. Currently recognized:
//            1) "cauchy" --> Assume a Cauchy distribution with weight = 1 + 4*(f-F)^2. 
//
//     - (list of strings) Parameters to tune along with (req'd) guesses of best fit (your estimates).
//           Example: 'A=1.23', 'B=0', 'C=sin(23.0)', 'slope=10.0/3.456', ...
//
// Optional input:
//     - (bool) Verbosity.
//
//     - (double) Characteristic Length. What is a rough scale of the data? Is a 'medium'-sized step
//       1E-16, 3.0, or 3E27 ?
//
//     - (int) Max number of iterations to try. Nelder-Mead simplexes sometimes walk around near the
//       minima before settling down (similar to a dog circling a bed before lying down). If your 
//       sum of squared residuals space is smooth (smooth function, smoothish data) it is ok to go 
//       fairly small here. 300-750 ought to suffice for general uses. Any more than 5000 is almost
//       guaranteed to be useless unless you have 500 parameters to fit.
//
//     - (double) Tolerance in change of resulting sum of squared residuals from one iteration to the
//       next. You can treat this (somewhat) as the precision you desire the minimized chi square to 
//       have. 1E-6 is probably the highest you'll really need in most cases. This would be a decent
//       level of precision in most line fitting routines.
//
// Output:
//     - (list of strings) Best estimate of parameters to tune. (These can be subbed into the function
//       via PolyCAS to evaluate the function at some <x,y,z,...>.)
//           Example: 'A=1.23', 'B=1E-6', 'C=-1.6', 'slope=3.21', ...
//
//     - ...some more numbers to indicate fit quality... See below.
//


std::tuple<std::list<std::string>,double,double,long int,double,double,double,std::list<double>> 
PolyCAS_Fit_Data(bool *wasOK, 
                 const std::string &f, 
                 const std::list<std::list<double>> &data, 
                 const std::list<std::string> &vars, 
                 const std::string &F_data,
                 const std::string &uncertainties,
                 const std::list<std::string> &params,
                 bool Verbose/*=false*/, 
                 double char_len/*=0.3*/, 
                 int max_iters/*=250*/, 
                 double ftol/*=1E-6*/ ){

    if(wasOK == nullptr) FUNCERR("Passed a nullptr bool. Unable to signal whether evaluation worked or not");
    *wasOK = false;
    std::tuple<std::list<std::string>,double,double,long int,double,double,double,std::list<double>> out;

    // ----------------- Sanity checking -------------------
    if(f.empty() || data.empty() || F_data.empty() || params.empty()){
        FUNCWARN("Given insufficient information to perform fit. Bailing");
        return out;
    }
    if(uncertainties.empty()){
        FUNCERR("An estimation of uncertainties is required for a sensible chi-square. If not interested in statistics, use '1.0' for all");
    }
    {
        //All rows have same number of cols.
        auto it = data.begin();
        size_t numcols = it->size();
        do{
            if(numcols != it->size()){
                FUNCWARN("Found two rows which differ in number of columns. Please pad to identical sizes. Bailing");
                return out;
            }
            numcols = it->size();
        }while((++it) != data.end());
    }


    // ---------------- Parsing and setup -----------------
    const auto DOF = static_cast<long int>(data.size()) - static_cast<long int>(params.size()); 

    auto parsed_f = Parse_String(f,false,true);      //The unnamed function. This is where we inject fit parameters.
    if(!parsed_f->Validate_Tree()){
        FUNCWARN("Validation of parsed f failed");
        return out;
    }

    auto parsed_F = Parse_String(F_data,false,true);
    if(!parsed_F->Validate_Tree()){      //The measurement of the actual function. This is likely just '$2'.
        FUNCWARN("Validation of parsed F_data failed");
        return out;
    }

    auto parsed_U = Parse_String(uncertainties,false,true);
    if(!parsed_U->Validate_Tree()){      //The uncertainties of each data point. This is likely just '1.0'.
        FUNCWARN("Validation of parsed uncertainties failed");
        return out;
    }

    //Cycle through the list of variable substitutions, parsing each. Substitute into the main expression.
    if(!vars.empty()){
        std::list<std::unique_ptr<BaseNode>> sub_exprs;
        for(auto s_it = vars.begin(); s_it != vars.end(); ++s_it) sub_exprs.push_back( Parse_String(*s_it,false,true) );

        parsed_f = Substitute_Basic(std::move(parsed_f), sub_exprs);
        if(!parsed_f->Validate_Tree()){
            FUNCWARN("Validation of parsed and subsituted expression failed");
            return out;
        }
    }
     
    std::list<std::string> Bare_Params; //A list of the parameters without any assignment operator.
    std::list<double> Def_Params;  //A list of user-provided initial guesses for the parameters.
    for(auto s_it = params.begin(); s_it != params.end(); ++s_it){
        const auto pos = s_it->find('=');
        if(pos == std::string::npos){
            FUNCWARN("Found a malformed parameter specification. Must also provide a starting estimate");
            return out;
        }
        const auto name    = s_it->substr(0,pos); // '(A)=1.23'
        const auto val_str = s_it->substr(pos+1); // 'A=(1.23)'
        Bare_Params.push_back(name);

        if(!Is_String_An_X<double>(val_str)){ //This should be properly parsed by PolyCAS. Do so if something complicated is needed....
             FUNCWARN("Could not interpret initial guess for parameter. Bailing");
             return out;
        }
        Def_Params.push_back(stringtoX<double>(val_str));
    }

    // ---------------- 'sticky' statistical quantities for later use ------------------- 
    //We capture the best when we see them in case the max number of iterations is exceeded
    // and the routine craps out prematurely. In theory, the best should be the last (when 
    // ftol is achieved).
    bool Performed_First_Pass = false; //On the first pass through (only), we will compute this for later use.
    double F_data_mean = 0.0;
    double best_SStot = std::numeric_limits<double>::max();  // = sum_i (yi - ymean)^2.
    double best_SSres = std::numeric_limits<double>::max();  // = sum_i (yi - y_meas_i)^2  aka ~what we are minimizing.
    double best_WSSR  = std::numeric_limits<double>::max();  // same as SSres but contains a weighting factor. This what we are minimizing.
    std::list<double> best_SRs;

    // ----------------- minimization lambda ------------------
    bool failure_inside_func_to_min = false;   //Escape signal. If this is set (within func_to_min), iteration halts asap.
    auto func_to_min = [&](double p[]) -> double {
        //We want to minimize WSSR = 'weighted sum of squares of residuals' = 'weighted residual sum of squares'
        // by adjusting A,B,C,...  The unweighted SSres is equivalent to WSSR when wi = 1.0;
        bool l_OK;
        size_t i;
        double SStot(0.0), SSres(0.0), WSSR(0.0);
        std::list<double> SRs;

        for(auto r_it = data.begin(); r_it != data.end(); ++r_it){
            std::list<std::unique_ptr<BaseNode>> subs, subs_copy, subs_copy_2;   ///...eek...
            
            //Construct the variable substitutions: '$0=1.23', '$1=2.34', ...
            i=0;
            for(auto c_it = r_it->begin(); c_it != r_it->end(); ++c_it, ++i){ //Column iteration.
                const auto avar = "$"_s + Xtostring<size_t>(i) + "="_s + Xtostring<double>(*c_it);
                subs.push_back( Parse_String(avar,false,true) );
                subs_copy.push_back( Parse_String(avar,false,true) );
                subs_copy_2.push_back( Parse_String(avar,false,true) );
            }
            
            //Construct the parameter substitutions: 'A=1.23', 'B=2.34', ...
            // Could probably speed up __func__ by moving this out of loop and avoiding many parses...
            i=0;
            for(auto p_it = Bare_Params.begin(); p_it != Bare_Params.end(); ++p_it, ++i){
                const auto apar = *p_it + "="_s + Xtostring<double>(p[i]);
                subs.push_back( Parse_String(apar,false,true) );
                subs_copy.push_back( Parse_String(apar,false,true) );
                subs_copy_2.push_back( Parse_String(apar,false,true) );
            }

            //Duplicate the two expressions so we can collapse them to numerical values.
            auto parsed_f_dup = parsed_f->Duplicate();  //The unnamed function. This is where we inject fit parameters.
            auto parsed_F_dup = parsed_F->Duplicate();  //The measurement of the actual function. This is likely just '$2'.
            auto parsed_U_dup = parsed_U->Duplicate();  //The uncertainties.

            //Perform the substitution.
            parsed_f_dup = Substitute_Basic(std::move(parsed_f_dup), subs);
            parsed_F_dup = Substitute_Basic(std::move(parsed_F_dup), subs_copy);
            if(!parsed_f_dup->Validate_Tree() || !parsed_F_dup->Validate_Tree()){
                FUNCWARN("Validation of parsed_f or parsed_F substituted expression failed");
                failure_inside_func_to_min = true;
                return -1.0;
            }

            //Attempt numerical evaluation.
            double func; 
            parsed_f_dup = Numerical_Basic(std::move(parsed_f_dup), &l_OK, &func);
            if(!l_OK){
                FUNCWARN("Unable to numerically evaluate unnamed function. Cannot proceed");
                failure_inside_func_to_min = true;
                return -1.0;
            }
            double func_data;
            parsed_F_dup = Numerical_Basic(std::move(parsed_F_dup), &l_OK, &func_data);
            if(!l_OK){
                FUNCWARN("Unable to numerically evaluate function measurement. Cannot proceed");
                failure_inside_func_to_min = true;
                return -1.0;
            }

            const auto SR = (func - func_data)*(func - func_data); //Squared residual.
            SRs.push_back(SR);

            //Weighting. This should be able to be passed in. Here we assume the uncertainties follow some
            // (chosen, known) distribution and thus we weight measurements with lower importance (higher W)
            // when they are unlikely to be drawn at random from such a distribution. In practice, this can
            // help reduce importance of outliers and can make a fit better because it has means to account
            // for variability in measurements. It can also fail badly and bloat significance. Caveat emptor!
            // NOTE: If initial guesses are not great, go with Wuniform. If they are close to optimal, try
            // Wcauchy or Wsq. Try chaining them together with Wuniform and then Wcauchy...
            //const auto Wuniform = 1.0; //Safest, but least-squares has well known poor outlier handling.
            //const auto Wgauss = ...; //Gaussian. Too steep. If first guesses aren't right, expect poor fit.
            //const auto Wsq = YGORABS(func - func_data); //Turns LSR to |yi-fi|. Outlier strength reduced.

            double W;
            if(uncertainties == "cauchy"){
                W = (1.0 + 4.0*SR); //Cauchy distribution. Should help reject outliers better.
                                    // From what I can tell, this is ~ what gnuplot uses.
            }else{
                parsed_U_dup = Substitute_Basic(std::move(parsed_U_dup), subs_copy_2);
                if(!parsed_U_dup->Validate_Tree()){
                    FUNCWARN("Validation of parsed_U substituted expression failed");
                    failure_inside_func_to_min = true;
                    return -1.0;
                }

                parsed_U_dup = Numerical_Basic(std::move(parsed_U_dup), &l_OK, &W);
                if(!l_OK){
                    FUNCWARN("Unable to numerically evaluate function measurement. Cannot proceed");
                    failure_inside_func_to_min = true;
                    return -1.0;
                }
            }
            if(W == 0.0){
                FUNCWARN("Somehow ended up with zero uncertainty. This routine cannot handle zero uncertainty");
                failure_inside_func_to_min = true;
                return -1.0;
            }

            WSSR += SR/(W*W);

            if(!Performed_First_Pass){ 
                F_data_mean += func_data;
            }else{
                SSres += SR; //+=(func - func_data)*(func - func_data)
                SStot += (func_data - F_data_mean)*(func_data - F_data_mean);
            }
        }
   
        //Stick the Sticky statistics, if appropriate.
        if(!Performed_First_Pass){
            Performed_First_Pass = true;
            F_data_mean /= static_cast<double>(data.size());
        }else if(WSSR < best_WSSR){
            best_WSSR  = WSSR;
            best_SSres = SSres;
            best_SStot = SStot;
            best_SRs   = SRs;
        }

        //Regular least-squares approach.
        return WSSR;

        //EXPERIMENTAL: least-median approach. NOTE: All statistics using this approach will be GARBAGE!
        //return Ygor_Median(SRs);
    };

    //------------------------------------------------------------------------------------------------------
    //--------------------------------------- Minimization Setup -------------------------------------------
    //------------------------------------------------------------------------------------------------------
    //These are the parameters which will be passed in to the function.
    const size_t numb_of_fit_parameters = Bare_Params.size();
    auto min_params = std::make_unique<double[]>(numb_of_fit_parameters);
    
    //Fill the parameters with some best-guesses.
    {
      auto d_it = Def_Params.begin();
      for(size_t i=0; i<numb_of_fit_parameters; ++i, ++d_it){
          (min_params.get())[i] = *d_it;
      }
    }

    //NMSimplex<double> minimizer(numb_of_fit_parameters, characteristic length, max number of iters, ftol);
    NMSimplex<double> minimizer(numb_of_fit_parameters, char_len, max_iters, ftol);
    minimizer.init(min_params.get(), func_to_min);

    //------------------------------------------------------------------------------------------------------
    //------------------------------------- Minimization computation ---------------------------------------
    //------------------------------------------------------------------------------------------------------
    while(minimizer.iter() == 0){
        if(failure_inside_func_to_min == true){
            FUNCWARN("Encountered failure whilst trying to evaluate function to be minimized. Cannot continue");
            return out;
        }
        if(Verbose){
            std::cout.precision(10);
            FUNCINFO("Minimization: At iteration " << minimizer.iteration << " the lowest value is " << minimizer.func_vals[minimizer.curr_min]);
        }
    }

    if(minimizer.iter() == -1) FUNCERR("Min scheme exited due to error. Maybe the initial params were not set?");
    if(minimizer.iter() ==  1) FUNCWARN("Min scheme finished due to max num of iters being exceeded.");
    if(minimizer.iter() ==  2) if(Verbose) FUNCINFO("Min scheme completed due to ftol < ftol_min.");

    minimizer.get_params(min_params.get());

    //Load the fit parameters into PolyCAS-parseable format.
    std::list<std::string> fit_pars;
    {
      auto s_it = Bare_Params.begin(); 
      for(size_t i=0; i<numb_of_fit_parameters; ++i, ++s_it){
          fit_pars.push_back( *s_it + "="_s + Xtostring<double>((min_params.get())[i]) );
      }
    }


    //------------ output synthesis ------------- 
    const auto chi_sq = best_WSSR;
    const auto red_chi_sq = chi_sq/static_cast<double>(DOF);
    const auto Qvalue = Stats::Q_From_ChiSq_Fit(chi_sq, static_cast<double>(DOF));
 
    //Not sure which is more useful. See http://en.wikipedia.org/wiki/Coefficient_of_determination. 
    const auto raw_coeff_deter = 1.0 - best_SSres/best_SStot; //Uses unweighted SSres, not weighted SSres. Use this if worried about weight scaling.
    const auto mod_coeff_deter = 1.0 - best_WSSR/best_SStot;  //Uses weighted SSres. This is the consistent choice. Use this if in doubt.
    //best_SRs contains an unordered listing of all squared-residuals and is suitable for inspection/verification of uncertainties.

    *wasOK = true;
    return std::make_tuple(fit_pars, chi_sq, Qvalue, DOF, red_chi_sq, raw_coeff_deter, mod_coeff_deter, best_SRs);
}




//Writes a gnuplot-able file to the desired location. 
bool PolyCAS_Write_Fit_Results_As_Plottable_File(
                 const std::string &f,
                 const std::list<std::list<double>> &data,
                 const std::list<std::string> &vars,
                 const std::string &F_data,
                 const std::string &uncertainties,
                 const std::list<std::string> &params,
                 std::tuple<std::list<std::string>,double,double,long int,double,double,double,std::list<double>> res,
                 const std::string &Filename,
                 const std::string &Title /*= ""*/ ){

    //This routine is a work-in-progress...

    const auto fit_params   = std::get<0>(res);
    const auto chi_sq       = std::get<1>(res);
    const auto Qvalue       = std::get<2>(res);
    const auto dof          = std::get<3>(res);
    const auto red_chi_sq   = std::get<4>(res);
    const auto raw_coef_det = std::get<5>(res);
    const auto mod_coef_det = std::get<6>(res);
    const auto SRs          = std::get<7>(res);  //Sum of residuals. Useful for inspection...

    //First, collect the data into a stringified format.
    std::stringstream data_ss, SR_ss;
    data_ss << R"***( "< echo -e ')***"; //"//Incorrect parsing of raw literals :/.
    for(auto r_it = data.begin(); r_it != data.end(); ++r_it){
        for(auto c_it = r_it->begin(); c_it != r_it->end(); ++c_it) data_ss << *c_it << " ";
        data_ss << R"***(\n)***";
    }
    data_ss << R"***('" )***";  //"//Incorrect parsing of raw literals :/.

    SR_ss << R"***( "< echo -e ')***"; //"//Incorrect parsing of raw literals :/.
    for(auto d_it = SRs.begin(); d_it != SRs.end(); ++d_it){
        SR_ss << *d_it << R"***(\n)***";
    }
    SR_ss << R"***('" )***";  //"//Incorrect parsing of raw literals :/.


    //Begin dumping to file. Start with some identification and stats.
    std::fstream FO(Filename,std::fstream::out);

    FO << "set title '" << Title << "'" << std::endl;
    FO << "#Title      = " << Title << std::endl;
    FO << "#chi_sq     = " << chi_sq << std::endl;
    FO << "#Qvalue     = " << Qvalue << std::endl;
    FO << "#DOF        = " << dof << std::endl;
    FO << "#red_chi_sq = " << red_chi_sq << std::endl;
    FO << "#raw_coeff_of_deter = " << raw_coef_det << std::endl;
    FO << "#mod_coeff_of_deter = " << mod_coef_det << std::endl;
    FO << "# Fitting variables:" << std::endl;
    for(auto it = vars.begin(); it != vars.end(); ++it) FO << "#    " << *it << std::endl;
    FO << "# F column = " << F_data << std::endl;
    FO << "# Uncertainties = " << uncertainties << std::endl;
    FO << std::endl;

    //Some stuff which might be useful for inspection or record.
    FO << "###### Square residuals: #####" << std::endl;
    FO << "#plot " << SR_ss.str() << " u ($0):($1) title 'Unordered Squared Residuals'" << std::endl;
    FO << "#exit" << std::endl;
    FO << "# Or, in a histogram format: " << std::endl;
    FO << "#binwidth=0.1 # <-- This needs to be tuned to suit distribution. Should be ~ (highest-lowest)/10." << std::endl;
    FO << "#set boxwidth binwidth" << std::endl;
    FO << "#bin(x,width)=width*floor(x/width) + binwidth/2.0" << std::endl;
    FO << "#plot " << SR_ss.str() << " u (bin($1,binwidth)):(1.0) smooth freq with boxes title 'Histogram of Squared Residuals'" << std::endl;
    FO << "#exit" << std::endl;

    FO << std::endl;

    //Lay down a swack of boilerplate.
    FO << R"***(set pointsize 0.6)***" << std::endl;
    FO << R"***(set style line 1 lc rgb '#000000' lw 1.1 pt 5)***" << std::endl;
    FO << R"***(set style line 2 lc rgb '#1D4599' lw 1.1 pt 5)***" << std::endl;
    FO << R"***(set style line 3 lc rgb '#11AD34' lw 1.1 pt 5)***" << std::endl;
    FO << R"***(set style line 4 lc rgb '#E62B17' lw 1.1 pt 5)***" << std::endl;
    FO << R"***(set style line 5 lc rgb '#E69F17' lw 1.1 pt 9)***" << std::endl;
    FO << R"***(set style line 6 lc rgb '#2F3F60' lw 1.1 pt 9)***" << std::endl;
    FO << R"***(set style line 7 lc rgb '#2F6C3D' lw 1.1 pt 9)***" << std::endl;
    FO << R"***(set style line 8 lc rgb '#8F463F' lw 1.1 pt 9)***" << std::endl;
    FO << R"***(set style line 9 lc rgb '#8F743F' lw 1.1 pt 11)***" << std::endl;
    FO << R"***(set style line 10 lc rgb '#031A49' lw 1.1 pt 11)***" << std::endl;
    FO << R"***(set style line 11 lc rgb '#025214' lw 1.1 pt 11)***" << std::endl;
    FO << R"***(set style line 12 lc rgb '#6D0D03' lw 1.1 pt 11)***" << std::endl;
    FO << R"***(set style line 13 lc rgb '#6D4903' lw 1.1 pt 13)***" << std::endl;
    FO << R"***(set style line 14 lc rgb '#224499' lw 1.1 pt 13)***" << std::endl;
    FO << R"***(set style line 15 lc rgb '#00FF00' lw 1.1 pt 13)***" << std::endl;
    FO << R"***(set style line 16 lc rgb '#00BBBB' lw 1.1 pt 7)***" << std::endl;
    FO << R"***(set style line 17 lc rgb '#000000' lw 1.1 pt 7)***" << std::endl;
    FO << R"***(set style line 18 lc rgb '#000000' lw 1.1 pt 7)***" << std::endl;
    FO << R"***(set style increment user)***" << std::endl;
    FO << R"***(set grid lc rgb '#000000')***" << std::endl;
    FO << R"***(set zero 1e-50)***" << std::endl;
    FO << std::endl;

    //Dump the best-fit parameters. gnuplot will pick these up in our function.
    for(auto it = fit_params.begin(); it != fit_params.end(); ++it)  FO << *it << std::endl;

    //Name the unnamed function so we can refer to it.
    FO << "f(";
    for(auto it = vars.begin(); it != vars.end(); ++it){
        FO << it->substr(0,it->find("="));
        if(std::next(it) != vars.end()) FO << ",";
    }
    FO << ")=" << f << std::endl;
    FO << std::endl;

    if(vars.size() == 1){
        FO << R"***(# Sample functions at somewhere nearer modern resolutions.)***" << std::endl;
        FO << R"***(set sample 5000)***" << std::endl;
        FO << R"***(# Correct for aspect ratio. Do not expand function at the expense of distortion.)***" << std::endl;
        FO << R"***(# This will probably waste space, and may be annoying in some situations.)***" << std::endl;
        FO << R"***(set view equal)***" << std::endl;
        FO << R"***(set size square)***" << std::endl;
        FO << std::endl;
        FO << R"***(# Note: Below, the 'vectors' are the fit residuals (impulses from fit function to data )***" << std::endl;
        FO << R"***(# points along y-axis).)***" << std::endl;

        //Dump the plot command, along with some easily-editable stuff for alternative plots.
        FO << R"***(plot f(x) title 'Fit' lt 1 \)***" << std::endl;
        FO << R"***(  , )***" << data_ss.str() << R"***( using ($1):($2) w p title 'Data' lt 2 \)***" << std::endl;
        FO << R"***(  , )***" << data_ss.str() << R"***( using ($1):($2):(0.0):(f($1)-$2) w vectors nohead lt 3 title '')***" << std::endl;

FUNCWARN("This function is not properly writen. It can only handle the case where x=$0, F=$1 and will otherwise silently fail! FIXME");
        FO << std::endl;
        FO << R"***(refresh)***" << std::endl;
        FO << std::endl;
        FO << R"***(exit)***" << std::endl;
        FO << std::endl;

        //Convenient plotting to file.
        FO << R"***(# Make a single-page pdf.)***" << std::endl;
        FO << R"***(#set term pdfcairo enhanced color solid font 'cmr10,12' size 8.5in,11.0in)***" << std::endl;
        FO << R"***(#set output ')***" << Filename << R"***(.pdf')***" << std::endl;
        FO << std::endl;
        FO << R"***(#replot)***" << std::endl;
        FO << R"***(#set output)***" << std::endl;
        FO << R"***(#set term pop)***" << std::endl;

    }else if(vars.size() == 2){
        FO << R"***(# Sample surfaces more than default (10?). This gets expensive and crowded quickly.)***" << std::endl;
        FO << R"***(set isosample 30)***" << std::endl;
        FO << R"***(# Sample functions at somewhere nearer modern resolutions.)***" << std::endl;
        FO << R"***(set sample 5000)***" << std::endl;
        FO << R"***(# Hide non-visible side of surface. Only draw diagonal lines for surface (seems clearer).)***" << std::endl;
        FO << R"***(set hidden3d offset 0 trianglepattern 4 )***" << std::endl;
        FO << R"***(# dgrid3d will synthesize a regular grid out of irregular data. No need for this here.)***" << std::endl;
        FO << R"***(unset dgrid3d)***" << std::endl;
        FO << R"***(# Enable some simple, default contour lines. 5 of them will be produced automatically.)***" << std::endl;
        FO << R"***(set cntrparam)***" << std::endl;
        FO << R"***(# Enable simple contour *lines*, ala cartographers. Display them on the surface and base.)***" << std::endl;
        FO << R"***(set contour both)***" << std::endl;
        FO << R"***(# Correct for aspect ratio. Do not expand function at the expense of distortion.)***" << std::endl;
        FO << R"***(# This will probably waste space, and may be annoying in some situations.)***" << std::endl;
        FO << R"***(set view equal)***" << std::endl;
        FO << std::endl;
        FO << R"***(# Note: Below, the 'vectors' are the fit residuals (impulses from fit function to data )***" << std::endl;
        FO << R"***(# points along z-axis).)***" << std::endl;
    
        //Dump the plot command, along with some easily-editable stuff for alternative plots.
        FO << R"***(###############################################################################)***" << std::endl;
        FO << R"***(# For a filled-in surface. Sometimes harder to see, but smoother looking.)***" << std::endl;
        FO << R"***(###############################################################################)***" << std::endl;
        FO << R"***(#set hidden3d front)***" << std::endl;
        FO << R"***(#set style fill solid 0.5)***" << std::endl;
        FO << R"***(#set palette rgbformulae 23,28,3)***" << std::endl;
        FO << R"***(#splot f(x,y) w pm3d title 'Fit' lt 1 \)***" << std::endl;
        FO << R"***(#  , f(x,y) with lines lt -2 notitle \)***" << std::endl;
        FO << R"***(#  , )***" << data_ss.str() << R"***( using 1:2:3 w p title 'Data' lt 2 \)***" << std::endl;
        FO << R"***(#  , )***" << data_ss.str() << R"***( using 1:2:($3):(0.0):(0.0):(f($1,$2)-$3) w vectors nohead notitle )***" << std::endl;
        FO << R"***(#)***" << std::endl;
        FO << R"***(###############################################################################)***" << std::endl;
        FO << R"***(# For a non-filled in surface, drawn with lines. It is easier to see, but is hard to)***" << std::endl;
        FO << R"***(###############################################################################)***" << std::endl;
        FO << R"***(# discern at a glance. Try this first.)***" << std::endl;
        FO << R"***(splot f(x,y) title 'Fit' lt 1 \)***" << std::endl;
        FO << R"***(  , )***" << data_ss.str() << R"***( using 1:2:3 w p lt 2 title 'Data' \)***" << std::endl;
        FO << R"***(  , )***" << data_ss.str() << R"***( using 1:2:($3):(0.0):(0.0):(f($1,$2)-$3) w vectors nohead lt 3 title '')***" << std::endl;
FUNCWARN("This function is not properly writen. It can only handle the case where x=$0, y=$1, F=$2 and will otherwise silently fail! FIXME");
        FO << std::endl;
        FO << R"***(###############################################################################)***" << std::endl;
        FO << R"***(# For more exotic stuff, see gnuplot documentation. It is quite handy.)***" << std::endl;
        FO << R"***(# http://gnuplot.sourceforge.net/)***" << std::endl;
        FO << R"***(###############################################################################)***" << std::endl;
        FO << R"***(refresh)***" << std::endl;
        FO << std::endl;
        FO << R"***(exit)***" << std::endl;
        FO << std::endl;
 
        //Convenience stuff for plotting to file.
        FO << R"***(# Make a simple animated gif. )***" << std::endl;
        FO << R"***(#set term gif animate delay 10 loop 0 size 1024,1024 optimize )***" << std::endl;
        FO << R"***(#set output ')***" << Filename << R"***(.gif')***" << std::endl;
        FO << R"***(# Make a multi-page pdf booklet.)***" << std::endl;
        FO << R"***(#set term pdfcairo enhanced color solid font 'cmr10,12' size 8.5in,11.0in)***" << std::endl;
        FO << R"***(#set output ')***" << Filename << R"***(.pdf')***" << std::endl;
        FO << std::endl;
        FO << R"***(# Animation. Can be used in any term (wxt, x11, png, gif))***" << std::endl;
        FO << R"***(do for [anim_t=1:360:1] {)***" << std::endl;
        FO << R"***(    ang_h=(anim_t))***" << std::endl;
        FO << R"***(    ang_v=(80.0 + 20.0*cos(4.0*anim_t*3.14159265/90.0)))***" << std::endl;
        FO << R"***(    set view ang_v,ang_h)***" << std::endl;
        FO << R"***(    refresh # Should be used for ephemeral plots. Works on gifs, too.)***" << std::endl;
        FO << R"***(    #replot # May be needed...)***" << std::endl;
        FO << R"***(})***" << std::endl;
        FO << R"***(#set output)***" << std::endl;
        FO << R"***(#set term pop)***" << std::endl;

    }else{
        FUNCERR("Cannot plot this using gnuplot...");
    }
 
    FO.close();
    return true;
}






