//PolyCAS_Generation.h.

#ifndef POLYCAS_GENERATION_H
#define POLYCAS_GENERATION_H

#include <list>
#include <string>
#include <memory>

//Given a list of variables (or (valid) expressions), output a series of expressions
// involving the basic arithmetic operations (+-*/)
//std::list<std::string> PolyCAS_Generate_Basic_Arithmetic(const std::list<std::string> &vars);

//Given a list of variables (or (valid) expressions), output a random expression
// involving the basic arithmetic operations (+-*/) and one or more of the given variables.
std::string PolyCAS_Randomly_Generate_Basic_Arithmetic_Involving(const std::list<std::string> &vars, long int max_num_vars, long int seed = -1);

//Given a list of variables (or (valid) expressions) and functions of one argument, output a random expression
// involving the basic arithmetic operations (+-*/) and one or more of the given variables.
std::string PolyCAS_Randomly_Gen_Basic_Arith_Involving_Vars_SV_Funcs(const std::list<std::string> &vars, 
                       const std::list<std::string> &funcs, long int max_num_vars, long int seed = -1);



#endif
