//Simplify.h - Routines to simplify trees made up of BaseNode-derived classes.

#ifndef PROJECT_POLYCAS_SIMPLIFY_H_HDR_GRD_
#define PROJECT_POLYCAS_SIMPLIFY_H_HDR_GRD_

#include <iostream>
#include <string>
#include <memory>
#include <list>

#include "YgorMisc.h"

#include "PolyCAS_Structs.h"

#include "PolyCAS_Autogen_Simplify.h"


#define SIMPLIFY_H_VERBOSE false



//This routine takes a node type and eliminates nested nodes. 
//
//For examples: AddNodes can be linked like:  
//
//      [AddNode] -- [AddNode] -- [AddNode] -- ...
//         |            |            |
//      [ExprNode1]   [ExprNode2]   [ExprNode3]
//          
// and this routine will 'collapse' these nodes into a single node, like: 
//      
//              [AddNode]   --  ...
//             /    |   |___  
//            /     |       |
//           /      |       |
// [ExprNode1] [ExprNode2] [ExprNode3]
//
//Note: This routine keeps order intact, so it should be suitable for: addition, subtraction, and multiplication.
//
/*
std::unique_ptr<BaseNode> Local_Use_Simplify_Basic_Ordered_Collapse(std::unique_ptr<BaseNode> in, unsigned long int Node_Type, bool *Changed = nullptr){
    //Redo the computation (and entire tree recursion) until nothing changes.
    if(Changed == nullptr){
        bool l_Changed;
        do{
            l_Changed = false;
            in = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(in), Node_Type, &l_Changed);
        }while(l_Changed == true);
        Changed = nullptr;
        return in;
    }
    //------------- Recursion fence --------------
    
    //If this is not an (AddNode?) [Node_Type], then recurse on the children nodes and return.
    if(in->Type != Node_Type){
        for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it){
            (*c_it) = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(*c_it), Node_Type, Changed);
        }
        return in;
    }

    //Check if there are any children (AddNodes?) [Node_Types]. If there are not, recurse on them anyways.
    for(auto c_it = in->Children.begin(); c_it != in->Children.end();  ){
        if( (*c_it)->Type != Node_Type ){
            (*c_it) = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(*c_it), Node_Type, Changed);
            ++c_it;
        }else{
            *Changed = true;

            //Collapse the child's children into this nodes's children, immediately prior to the child's location.
            in->Children.splice( c_it, std::move((*c_it)->Children) );
            
            //Remove the child, iterating the iterator to the next available child.
            c_it = in->Children.erase( c_it );
        }
    }

    return in;
}
*/

std::unique_ptr<BaseNode> Local_Use_Simplify_Basic_Ordered_Collapse(std::unique_ptr<BaseNode> in, unsigned long int Node_Type_A, unsigned long int Node_Type_B, bool *Changed = nullptr){
    //Redo the computation (and entire tree recursion) until nothing changes.
    if(Changed == nullptr){
        bool l_Changed;
        do{
            l_Changed = false;
            in = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(in), Node_Type_A, Node_Type_B, &l_Changed);
        }while(l_Changed == true);
        Changed = nullptr;
        return in;
    }
    //------------- Recursion fence --------------

    //If this is not a Node_Type_A, then recurse on the children nodes and return.
    if(in->Type != Node_Type_A){
        for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it){
            (*c_it) = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(*c_it), Node_Type_A, Node_Type_B, Changed);
        }
        return in;
    }

    //Check if there are any children Node_Type_B. If there are not, recurse on them. If there are, collapse them.
    for(auto c_it = in->Children.begin(); c_it != in->Children.end();  ){
        if( (*c_it)->Type != Node_Type_B ){
            (*c_it) = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(*c_it), Node_Type_A, Node_Type_B, Changed);
            ++c_it;
        }else{
            *Changed = true;

            //Collapse the child's children into this nodes's children, immediately prior to the child's location.
            in->Children.splice( c_it, std::move((*c_it)->Children) );

            //Remove the child, iterating the iterator to the next available child.
            c_it = in->Children.erase( c_it );
        }
    }

    return in;
}

//This routine takes two node types and reorders nested nodes. 
//
//For examples: A tree like: 
//
//       [AddNode] -- [SubtractNode] -- [AddNode] -- ...
//         |            |                  |
//      [ExprNode1]   [ExprNode2]       [ExprNode3]
//
// might be reordered to (if Node_Type_A is AddNode and Node_Type_B is SubtractNode) 
//
//       [AddNode] -- [AddNode] -- [SubtractNode -- ...
//         |            |                |       
//      [ExprNode1]   [ExprNode3]    [ExprNode2]
//
std::unique_ptr<BaseNode> Local_Use_Simplify_Basic_Reordering(std::unique_ptr<BaseNode> in, unsigned long int Node_Type_A, unsigned long int Node_Type_B, bool *Changed = nullptr){
    //Redo the computation (and entire tree recursion) until nothing changes.
    if(Changed == nullptr){
        bool l_Changed;
        do{
            l_Changed = false;
            in = Local_Use_Simplify_Basic_Reordering(std::move(in), Node_Type_A, Node_Type_B, &l_Changed);
        }while(l_Changed == true);
        Changed = nullptr;
        return in;
    }
    //------------- Recursion fence --------------

    //If this is not a [Node_Type_B], then recurse on the children nodes and return.
    if(in->Type != Node_Type_B){
        for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it){
            (*c_it) = Local_Use_Simplify_Basic_Reordering(std::move(*c_it), Node_Type_A, Node_Type_B, Changed);
        }
        return in;
    }

    //Check if there are any children [Node_Type_A]. If there are not, recurse on them.
    for(auto c_it = in->Children.begin(); c_it != in->Children.end();  ){
        if( (*c_it)->Type != Node_Type_A ){
            (*c_it) = Local_Use_Simplify_Basic_Reordering(std::move(*c_it), Node_Type_A, Node_Type_B, Changed);
            ++c_it;
        }else{
            *Changed = true;
/*
            //Collapse the child's children into this nodes's children, immediately prior to the child's location.
            in->Children.splice( c_it, std::move((*c_it)->Children) );

            //Remove the child, iterating the iterator to the next available child.
            c_it = in->Children.erase( c_it );
*/

        }
    }

    return in;
}

template <class T>
std::unique_ptr<BaseNode> Local_Use_Simplify_Basic_Exact_Numerical(std::unique_ptr<BaseNode> in, unsigned long int Node_Type, bool *Changed = nullptr){
    //Redo the computation (and entire tree recursion) until nothing changes.
    if(Changed == nullptr){
        bool l_Changed;
        do{
            l_Changed = false;
            in = Local_Use_Simplify_Basic_Exact_Numerical<T>(std::move(in), Node_Type, &l_Changed);
        }while(l_Changed == true);
        Changed = nullptr;
        return in;
    }
    //------------- Recursion fence --------------

    //If this is not an operation node, then recurse on the children nodes and return.
    //
    //This routine requires at least two children nodes.
    if((   (in->Type != NodeType::FuncAdd) 
        && (in->Type != NodeType::FuncSubtract)
        && (in->Type != NodeType::FuncMultiply)
        && (in->Type != NodeType::FuncDivide)
        && (in->Type != NodeType::FuncExponentiate)
       ) 
       ||
       (in->Children.size() < 2)
    ){
        for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it){
            (*c_it) = Local_Use_Simplify_Basic_Exact_Numerical<T>(std::move(*c_it), Node_Type, Changed);
        }
        return in;
    }

    //Check if there are any adjacent children [Node_Type]'s. If there are not, recurse on them.
    for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it ){
        auto c2_it = c_it;
        ++c2_it;

        if( ((*c_it)->Type != Node_Type) || !(c2_it != in->Children.end()) || ((*c2_it)->Type != Node_Type) ){
            (*c_it) = Local_Use_Simplify_Basic_Exact_Numerical<T>(std::move(*c_it), Node_Type, Changed);
        }else{
            auto A = Cast_From_To_Node<BaseNode,T>(std::move(*c_it));
            auto B = Cast_From_To_Node<BaseNode,T>(std::move(*c2_it));
            bool KeepC2 = false;

            if(in->Type == NodeType::FuncAdd){   // A + B + C + D + ...
                A->x += B->x;
                *Changed = true;
            }else if(in->Type == NodeType::FuncSubtract){  // A - B - C - D - ...
                if(std::distance(in->Children.begin(), c_it) == 0){
                    A->x -= B->x;
                }else{
                    A->x += B->x;
                }
                *Changed = true;
            }else if(in->Type == NodeType::FuncMultiply){
                A->x *= B->x;
                *Changed = true;
            }else if(in->Type == NodeType::FuncDivide){
                if(B->x != 0){  //This will be handled by converting to either 'inf' (1/0) or 'missingno' (0/0)
                    A->x /= B->x;
                    *Changed = true;
                }else{
                    KeepC2 = true;  //Psych! Keep the second child intact.
                }
            }else if(in->Type == NodeType::FuncExponentiate){
                if(auto AA = dynamic_cast<ExprRationalNode *>(A.get())){
                    if(auto BB = dynamic_cast<ExprRationalNode *>(B.get())){
                        mpz_t bnum, bden;
                        mpz_init(bnum);
                        mpz_init(bden);
                        mpz_set(bnum, ((BB->x).get_num()).get_mpz_t());
                        mpz_set(bden, ((BB->x).get_den()).get_mpz_t());
    
                        if((BB->x >= 0) && (mpz_cmp_ui(bden,1) == 0) && mpz_fits_ulong_p(bnum)){ //We safely handle ONLY the positive integer case, in general.
                            unsigned long int bnum_uli = mpz_get_ui(bnum); //This is a funny function. We have to ensure the sign/size are appropriate!
                            mpz_pow_ui(((AA->x).get_num()).get_mpz_t(), ((AA->x).get_num()).get_mpz_t(), bnum_uli);
                            mpz_pow_ui(((AA->x).get_den()).get_mpz_t(), ((AA->x).get_den()).get_mpz_t(), bnum_uli);
                            (AA->x).canonicalize();
                            *Changed = true;
                        }else{
                            KeepC2 = true;  //Psych! Keep the second child intact.
                        }
                        mpz_clear(bnum);
                        mpz_clear(bden);
                    }
                }else{
                    FUNCERR("Exact exponentiation simplification is currently only implemented for GNU GMP rationals.");
                }
            }

            //Re-cast the nodes back to BaseNode-compatible pointers to place them back in the tree.
            *c_it  = Cast_From_To_Node<T,BaseNode>(std::move(A));
            *c2_it = Cast_From_To_Node<T,BaseNode>(std::move(B));

            if(!KeepC2){  //Remove the second node if it has been consumed.
                in->Children.erase(c2_it);
            }
        }
    }

    //Check if there is a single child. If so, the operation has become redundant and can be removed.
    //
    //NOTE: This routine CAN NOT propagate these changes up the tree! To propagate upward, we MUST rely
    // on recursion to comb the tree again after a change!
    if(in->Children.size() == 1){
        std::unique_ptr<BaseNode> thechild = nullptr;
        thechild = std::move(*(in->Children.begin()));
        in = std::move(thechild);
    }

    return in;
}

template <class T>
std::unique_ptr<BaseNode> Local_Use_Simplify_Basic_InExact_Numerical(std::unique_ptr<BaseNode> in, unsigned long int Node_Type, bool *Changed = nullptr){
    //Redo the computation (and entire tree recursion) until nothing changes.
    if(Changed == nullptr){
        bool l_Changed;
        do{
            l_Changed = false;
            in = Local_Use_Simplify_Basic_InExact_Numerical<T>(std::move(in), Node_Type, &l_Changed);
        }while(l_Changed == true);
        Changed = nullptr;
        return in;
    }
    //------------- Recursion fence --------------

    //If this is not an operation node, then recurse on the children nodes and return.
    //
    //This routine requires at least two children nodes.
    if((   (in->Type != NodeType::FuncAdd) 
        && (in->Type != NodeType::FuncSubtract)
        && (in->Type != NodeType::FuncMultiply)
        && (in->Type != NodeType::FuncDivide)
        && (in->Type != NodeType::FuncExponentiate)
       ) 
       ||
       (in->Children.size() < 2)
    ){
        for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it){
            (*c_it) = Local_Use_Simplify_Basic_InExact_Numerical<T>(std::move(*c_it), Node_Type, Changed);
        }
        return in;
    }

    //Check if there are any adjacent children [Node_Type]'s. If there are not, recurse on them.
    for(auto c_it = in->Children.begin(); c_it != in->Children.end(); ++c_it ){
        auto c2_it = c_it;
        ++c2_it;

        if( ((*c_it)->Type != Node_Type) || !(c2_it != in->Children.end()) || ((*c2_it)->Type != Node_Type) ){
            (*c_it) = Local_Use_Simplify_Basic_InExact_Numerical<T>(std::move(*c_it), Node_Type, Changed);
        }else{
            auto A = Cast_From_To_Node<BaseNode,T>(std::move(*c_it));
            auto B = Cast_From_To_Node<BaseNode,T>(std::move(*c2_it));
            bool KeepC2 = false;

            if(in->Type == NodeType::FuncAdd){   // A + B + C + D + ...
                A->x += B->x;
                *Changed = true;
            }else if(in->Type == NodeType::FuncSubtract){  // A - B - C - D - ...
                if(std::distance(in->Children.begin(), c_it) == 0){
                    A->x -= B->x;
                }else{
                    A->x += B->x;
                }
                *Changed = true;
            }else if(in->Type == NodeType::FuncMultiply){
                A->x *= B->x;
                *Changed = true;
            }else if(in->Type == NodeType::FuncDivide){
                if(B->x != 0){  //This will be handled by converting to either 'inf' (1/0) or 'missingno' (0/0)
                    A->x /= B->x;
                    *Changed = true;
                }else{
                    KeepC2 = true;  //Psych! Keep the second child intact.
                }
            }else if(in->Type == NodeType::FuncExponentiate){
                A->x = pow(A->x, B->x);
                *Changed = true;
            }

            //Re-cast the nodes back to BaseNode-compatible pointers to place them back in the tree.
            *c_it  = Cast_From_To_Node<T,BaseNode>(std::move(A));
            *c2_it = Cast_From_To_Node<T,BaseNode>(std::move(B));

            if(!KeepC2){  //Remove the second node if it has been consumed.
                in->Children.erase(c2_it);
            }
        }
    }

    //Check if there is a single child. If so, the operation has become redundant and can be removed.
    //
    //NOTE: This routine CAN NOT propagate these changes up the tree! To propagate upward, we MUST rely
    // on recursion to comb the tree again after a change!
    if(in->Children.size() == 1){
        std::unique_ptr<BaseNode> thechild = nullptr;
        thechild = std::move(*(in->Children.begin()));
        in = std::move(thechild);
    }

    return in;
}


std::unique_ptr<BaseNode> Simplify_Basic_Structural(std::unique_ptr<BaseNode> in){
    //These simplifications should provide only *required structural simplifications* without which
    // trees would be considered invalid or completely useless. Do not put generic numerical 
    // routines here - this is for dealing with things like CommaNodes, ShuttleNodes (if they get
    // here, etc..)
    
    //----------------------------------------------------------------------------------------------
    //Collapse function nodes with CommaNode arguments.
//    in = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(in), NodeType::Func, NodeType::ContComma);
    in = Simplify_Collapse_Commas_In_Functions(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 02- tree is: " << in->Output_Serial());

    // ... more here ...


    return in;
}




std::unique_ptr<BaseNode> Simplify_Basic_Lossless(std::unique_ptr<BaseNode> in){
    //This routine performs the most basic simplification routines. It should not do anything 
    // drastic. It should also not assume any particular algebraic group for ExprNodes.
    //

for(int i = 0; i < 5; ++i){ //This is a stop-gap for handling sequential simplification.    FIXME TODO

    //----------------------------------------------------------------------------------------------
    //Minimal, structural simplifications. These should be considered 'mandatory' simplifications!
    in = Simplify_Basic_Structural(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 00- tree is: " << in->Output_Serial());


    //----------------------------------------------------------------------------------------------
    //Obliterate consecutive  FuncNegationNodes.
    in = Simplify_Collapse_Double_Negations(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 01- tree is: " << in->Output_Serial());

    //----------------------------------------------------------------------------------------------

//MOVED TO STRUCTURAL SIMPLIFICATIONS...
//    //Collapse function nodes with CommaNode arguments.
//    in = Simplify_Collapse_Commas_In_Functions(std::move(in));
//    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 02- tree is: " << in->Output_Serial());

    //Collapse nested addition nodes.
    in = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(in), NodeType::FuncAdd, NodeType::FuncAdd);
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 03- tree is: " << in->Output_Serial());
   
    //Collapse nested subtraction nodes.
//    in = Local_Use_Simplify_Basic_Ordered_Collapse(std::move(in), NodeType::FuncSubtract, NodeType::FuncSubtract); //Doesn't properly handle nests!
    in = Simplify_Nested_Subtraction(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 04- tree is: " << in->Output_Serial());

    //Reordering [subtraction] -- [addition] nodes to be [addition] -- [subtraction].
//    in = Local_Use_Simplify_Basic_Reordering(std::move(in), NodeType::FuncAdd, NodeType::FuncSubtract);   //NOTE: I dont think this works properly! Verify it first!

    //----------------------------------------------------------------------------------------------
    //Perform (exact) numerical simplifications.
    in = Local_Use_Simplify_Basic_Exact_Numerical<ExprRationalNode>(std::move(in), NodeType::ExprRational);
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 05- tree is: " << in->Output_Serial());


    //----------------------------------------------------------------------------------------------
    //Ordering (arrangement) conventions.
    in = Simplify_Bubble_Up_Negations_Multiply(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 06- tree is: " << in->Output_Serial());
    in = Simplify_Bubble_Up_Negations_Divide(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 07- tree is: " << in->Output_Serial());

    in = Simplify_Reorder_Numeric_First_Addition(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 08- tree is: " << in->Output_Serial());
    in = Simplify_Reorder_Numeric_First_Subtract(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 09- tree is: " << in->Output_Serial());
    in = Simplify_Reorder_Numeric_First_Multiply(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 10- tree is: " << in->Output_Serial());

    in = Simplify_Reorder_Negation_Last_Addition(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 11- tree is: " << in->Output_Serial());

    in = Simplify_Collect_Numeric_Across_Division(std::move(in)); //Requires numeric nodes on the left.
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 12- tree is: " << in->Output_Serial());

    in = Simplify_Convert_Addition_With_Negation_To_Subtraction(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 13- tree is: " << in->Output_Serial());

    in = Simplify_Convert_Addition_With_Subtraction_To_Addition(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 14- tree is: " << in->Output_Serial());

    in = Expand_Distribute_Multiplication_Over_Addition_And_Subtraction(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 15- tree is: " << in->Output_Serial());

    in = Simplify_Pair_Identical_Expressions_Within_Addition(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 16- tree is: " << in->Output_Serial());

    in = Simplify_Accumulate_PseudoSimilar_Expressions_Within_Addition(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 17- tree is: " << in->Output_Serial());

    in = Simplify_Cancel_PseudoSimilar_Expressions_Within_Addition(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 18- tree is: " << in->Output_Serial());

    in = Simplify_Cancel_Identical_Expressions_Within_Addition(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 19- tree is: " << in->Output_Serial());

    in = Simplify_Remove_Unneeded_Zeros(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 20- tree is: " << in->Output_Serial());


    in = Simplify_Pair_Identical_Expressions_Within_Subtraction(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 21- tree is: " << in->Output_Serial());

    in = Simplify_Cancel_Identical_Expressions_Within_Subtraction(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 22- tree is: " << in->Output_Serial());

    in = Simplify_Remove_Unneeded_Unities(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 23- tree is: " << in->Output_Serial());

    in = Simplify_Pull_Out_Negatives_Numerical(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 24- tree is: " << in->Output_Serial());

    in = Simplify_Numerical_LessThans(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 25- tree is: " << in->Output_Serial());

    in = Simplify_Function_If(std::move(in));
    if(SIMPLIFY_H_VERBOSE) FUNCINFO("DEBUG 26- tree is: " << in->Output_Serial());

}

    return in;
}


std::unique_ptr<BaseNode> Simplify_Basic_Lossy(std::unique_ptr<BaseNode> in){
    //Perform all lossless simplifications.
    in = Simplify_Basic_Lossless(std::move(in));    

    //----------------------------------------------------------------------------------------------
    //Perform (lossy) numerical simplifications.
    in = Local_Use_Simplify_Basic_InExact_Numerical<ExprDoubleNode>(std::move(in), NodeType::ExprDouble);

    return in;
}


#endif
