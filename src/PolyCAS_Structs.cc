//Structs.cc.

#include <iostream>
#include <string>
#include <memory>
#include <list>

#include <gmpxx.h>     //Needed for ExprRational and ExprFloat (numerical) types.

#include "YgorMisc.h"
#include "YgorString.h"

#include "PolyCAS_Structs.h"
#include "PolyCAS_Common_Routines.h"

#define STRUCTS_CC_VERBOSE false

#ifndef STRUCTS_CC_INCLUDE_ALL_SPECIALIZATIONS
    #define STRUCTS_CC_INCLUDE_ALL_SPECIALIZATIONS
#endif

/*
template <class T>  std::string Xtostring(T numb){
    std::stringstream ss;
    ss << numb;
    return ss.str();
}
*/


#ifndef RET_FLS_IF_FLS 
  #define RET_FLS_IF_FLS( S, MSG ) \
      if(!(S)){ FUNCWARN(MSG); return false; };
#endif



//---------------------------------------------------------------------------------------------------------------------
//------------------------------------------------- class BaseNode ----------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
BaseNode::BaseNode() : Type(0) { }
BaseNode::BaseNode(unsigned long int inType) : Type(inType) { }
BaseNode::BaseNode(unsigned long int inType, const std::string &inRaw_Expr) : Type(inType), Raw_Expr(inRaw_Expr) { }
/* //Cannot do this - should either use the "Curiously-repeating template pattern" or simply provide it for all derivations.
BaseNode::BaseNode(const BaseNode &dup) : Type(dup.Type), Raw_Expr(dup.Raw_Expr) {
    for(auto it = dup.Children.begin(); it != dup.Children.end(); ++it){
        std::unique_ptr<BaseNode> childcopy( new BaseNode( **it ) );
        Append_Node_To_List(this.Children, std::move(childcopy));
    }
};
*/
BaseNode::~BaseNode() = default; //We have to define this (even if default) because it is in class spec (as virtual).

std::string BaseNode::Output_Parse_Tree(long int depth) const {  //Note: depth = 0 by default.
    //This function will output a GraphViz-readable 'dot' syntax file, suitable for converting to a graph image.
    // For example, perform:
    //   1) write the output of this function to file test2.gv.
    //   2) cat test2.gv  | dot -Tpng -o /tmp/test.png && chromium --incognito /tmp/test.png
 
    //Prep.
    std::string out;
    if(depth == 0) out += "graph parsetree { \n bgcolor = black \n";

    //Output the attributes of this given node.
    const std::string myLabel(this->Output_Graph_Label()); //depth));
    const std::string myID(this->Output_Graph_ID()); //depth));
    out += myID + " [label=\"" + myLabel + "\", color=green, fontcolor=green];\n";

    //Looping through nodes, spitting out the linkages between this node and each child node.
    for(auto it=Children.begin(); it!=Children.end(); ++it){
        if( *it != nullptr ){
            const std::string childID( (*it)->Output_Graph_ID() ); //depth+1) );
            out += "    " + myID + " -- " + childID + " [type=s, color=green];\n"; 
        }else{
            FUNCERR("Found a non-valid child node!");
        }
    }

    //Now we call the same on the children nodes.
    for(auto it=Children.begin(); it!=Children.end(); ++it){
        if( *it != nullptr ){
            out += (*it)->Output_Parse_Tree(depth+1);
        }else{
            FUNCERR("Found a non-valid child node!");
        }
    }

    //Cleanup.
    if(depth == 0) out += " } ";
    return out;
}

bool BaseNode::Validate_Nodes_Memory(void) const {
    //Cycle through the children, ensuring none are nullptr. Do not recurse.
    for(auto it = Children.begin(); it != Children.end(); ++it){
        if((*it) == nullptr){
            FUNCWARN("Found a node which is invalid");
            return false;
        }
    }
    return true;
}

bool BaseNode::Validate_Tree(void) const {
    //Validate this node and its memory.
    if( ! this->Validate_Node() ) return false;
    if( ! this->Validate_Nodes_Memory() ) return false;  
 
    //Perform the same on all children.
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){
        if( *it == nullptr){
            FUNCWARN("Found a null reference in the children of a node!");
            return false;
        }else{
            if( ! (*it)->Validate_Tree() ) return false;
        }
    }
    return true;
}

std::string BaseNode::Output_Graph_ID(void) const { 
    return Xtostring<long int>( (long int)(this) );
}

std::list<std::list<std::unique_ptr<BaseNode>>::const_iterator> BaseNode::Get_Lower_Nodes(void) const { 
    std::list<std::list<std::unique_ptr<BaseNode>>::const_iterator> torecurse, childnodes;
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){
        torecurse.push_back(it);
    }

    while(!torecurse.empty()){
        const auto thisnode = (*(torecurse.begin()));
        //Push this node into childnodes.
        childnodes.push_back(thisnode);
        for(auto it = (*thisnode)->Children.begin(); it != (*thisnode)->Children.end(); ++it){
            //Push back all the children nodes we have yet to recurse.
            torecurse.push_back(it);
        }
        //Remove this node from the recursal routine.
        torecurse.erase( torecurse.begin() );
    }
    return childnodes;
}

bool BaseNode::operator == (const BaseNode &rhs) const { //Recursive equality.
    //Basic equivalence tests. These things are all inherited from the BaseNode.
    if( this->Type != rhs.Type ) return false;
    if( this->Raw_Expr != rhs.Raw_Expr ) return false;
    if( this->Children.size() != rhs.Children.size()) return false;

    //Specialized comparisons (Note: The type is the same, so they *should* both cast the same!) for numerical types.
    if(auto *thisc = dynamic_cast<const ExprRationalNode *>(this)){
        if(auto *rhsc  = dynamic_cast<const ExprRationalNode *>(&rhs)){
            if(thisc->x != rhsc->x) return false;
        }
    }else if(auto *thisc = dynamic_cast<const ExprDoubleNode *>(this)){
        if(auto *rhsc  = dynamic_cast<const ExprDoubleNode *>(&rhs)){
            //We should be satisfied with 1ppm equivalence, otherwise should use arb. prec. floats.
            if(RELATIVE_DIFF(thisc->x, rhsc->x) > 1E-6){
                return false;
            }
        }
    }  

    //Recursive equality.
    if( ! this->Children.empty() ){
        for(auto t_it = this->Children.begin(), r_it = rhs.Children.begin(); t_it != this->Children.end(); ++t_it, ++r_it){
            if( (*t_it)->Are_Not_Equiv((**r_it)) ) return false;
        }
    }
    return true;
}
bool BaseNode::operator != (const BaseNode &rhs) const { //Recursive inequality.
    return !( (*this) == rhs );
}
bool BaseNode::Are_Equiv(const BaseNode &rhs) const { //Recursive equality.
    //This function is meant to better handle the issue of accidentally comparing pointers to nodes instead of nodes themselves.
    return ( (*this) == rhs );
}
bool BaseNode::Are_Not_Equiv(const BaseNode &rhs) const { //Recursive equality.
    return !( (*this) == rhs );
}


//---------------------------------------------------------------------------------------------------------------------
//--------------------------- Simple BaseNode-derived classes - Common Templated Functions ----------------------------
//---------------------------------------------------------------------------------------------------------------------
//Convenient template helper-code for simplistic derivations from BaseNode.
// XXXX is something like "ShuttleNode" and YYYY is something like "Shuttle" (A node type!)
//
#ifndef PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE_DEFINED
  #define PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE_DEFINED
  #define PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE( XXXX, YYYY )             \
    XXXX::XXXX() : BaseNode(NodeType::YYYY) { }                                              \
    XXXX::XXXX(const std::string &inRaw_Expr) : BaseNode(NodeType::YYYY, inRaw_Expr) { }     \
    std::unique_ptr<BaseNode> XXXX::Duplicate(void) const {                                  \
        std::unique_ptr<BaseNode> out = std::make_unique<XXXX>(this->Raw_Expr);              \
        for(auto it = this->Children.begin(); it != this->Children.end(); ++it){             \
            out->Children.push_back(nullptr);                                                \
            auto childnode = (*it)->Duplicate();                                             \
            (*(out->Children.rbegin())) = std::move(childnode);                              \
        }                                                                                    \
        return out;                                                               \
    }    
#endif
//    XXXX::~XXXX() = default;                                                                 


PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(ShuttleNode,Shuttle)

PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(ExprNode,Expr)

PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncNode,Func)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncAddNode,FuncAdd)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncSubtractNode,FuncSubtract)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncNegateNode,FuncNegate)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncMultiplyNode,FuncMultiply)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncDivideNode,FuncDivide)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(ContParenthesisNode,ContParenthesis)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncExponentiateNode,FuncExponentiate)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(ContCommaNode,ContComma)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(ContAssignmentNode,ContAssignment)

PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncLessThanNode,FuncLessThan)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncMoreThanNode,FuncMoreThan)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncLessThanEqNode,FuncLessThanEq)
PROJECT_POLYCAS_SIMPLE_TEMPLATED_FUNCTIONS_FROM_BASENODE(FuncMoreThanEqNode,FuncMoreThanEq)

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------- Simple BaseNode-derived classes --------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

//----- ShuttleNode.
std::string ShuttleNode::Output_Graph_Label(void) const { 
    return std::string("Shuttle - You should NOT see me if parsing was OK" ); 
}
std::string ShuttleNode::Output_Serial(void) const {
    std::string out;
    for(auto it=this->Children.begin(); it != this->Children.end(); ++it){
        out += (*it)->Output_Serial();
    }
    return out;
}
bool ShuttleNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.empty(), "Found a ShuttleNode with children");
    return true;
}
size_t ShuttleNode::Min_Num_Children(void) const { return 0; }


//----- ExprNode.
std::string ExprNode::Output_Graph_Label(void) const { 
    return std::string("ExprNode \\n" + this->Raw_Expr); 
}
std::string ExprNode::Output_Serial(void) const {
    return this->Raw_Expr;
} 
bool ExprNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(!this->Raw_Expr.empty(), "Found an empty ExprNode");
    RET_FLS_IF_FLS(this->Children.empty(), "Found an ExprNode with children");
    return true;
}
size_t ExprNode::Min_Num_Children(void) const { return 1; }

//----- FuncNode.
std::string FuncNode::Output_Graph_Label(void) const { 
    return std::string("FuncNode \\n" + this->Raw_Expr); 
}
std::string FuncNode::Output_Serial(void) const {
    std::string out(this->Raw_Expr); //Function's name.
    out += "(";
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){
        out += (*it)->Output_Serial();
        if( it != --(this->Children.end()) ) out += ",";
    }
    return out + ")";
}
bool FuncNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(!this->Children.empty(), "Found a FuncNode without any children");
    return true;
}
size_t FuncNode::Min_Num_Children(void) const { return 0; } //Do I allow F() as valid? Not sure... Probably should not...

//----- FuncAddNode.
std::string FuncAddNode::Output_Graph_Label(void) const {
    return std::string("AddNode \\n +"); 
}
std::string FuncAddNode::Output_Serial(void) const {
    std::string out("(");
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){
        out += (*it)->Output_Serial();
        if( it != --(this->Children.end()) ) out += "+"; 
    }
    return out + ")";
}
bool FuncAddNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() >= 2, "Found a FuncAddNode with <2 children");
    return true;
}
size_t FuncAddNode::Min_Num_Children(void) const { return 2; }

//----- FuncSubtractNode.
std::string FuncSubtractNode::Output_Graph_Label(void) const { 
    return std::string("SubtractNode \\n -"); 
}
std::string FuncSubtractNode::Output_Serial(void) const {
    if(this->Validate_Node()){
        std::string out("(");
        for(auto it = this->Children.begin(); it != this->Children.end(); ++it){
            out += (*it)->Output_Serial();
            if( it != --(this->Children.end()) ) out += "-";
        }
        return out + ")";
    }
    FUNCERR("Found a subtraction node with " << this->Children.size() << " children (should be >= 2)");
    return "";
}
bool FuncSubtractNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() >= 2, "Found a FuncSubtractNode with <2 children");
    return true;
}
size_t FuncSubtractNode::Min_Num_Children(void) const { return 2; }

//----- FuncNegateNode.
std::string FuncNegateNode::Output_Graph_Label(void) const { 
    return std::string("NegateNode \\n -"); 
}
std::string FuncNegateNode::Output_Serial(void) const {
    if(this->Validate_Node()){
        return "(-"_s + this->Children.front()->Output_Serial() + ")";
    }
    FUNCERR("Found a negation node with " << this->Children.size() << " children (should be 1.)");
    return "";
}
bool FuncNegateNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 1, "Found a FuncNegateNode with !exactly one child");
    return true;      
}
size_t FuncNegateNode::Min_Num_Children(void) const { return 1; }

//----- FuncMultiplyNode.
std::string FuncMultiplyNode::Output_Graph_Label(void) const { 
    return std::string("MultiplyNode \\n *"); 
}
std::string FuncMultiplyNode::Output_Serial(void) const {
    std::string out("(");
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){
        out += (*it)->Output_Serial();
        if( it != --(this->Children.end()) ) out += "*";
    }
    return out + ")";
}
bool FuncMultiplyNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() >= 2, "Found a FuncMultiplyNode with <2 children");
    return true;  
}
size_t FuncMultiplyNode::Min_Num_Children(void) const { return 2; }

//----- FuncDivideNode.
std::string FuncDivideNode::Output_Graph_Label(void) const { 
    return std::string("DivideNode \\n /"); 
}
std::string FuncDivideNode::Output_Serial(void) const {
    if(this->Validate_Node()){
        return "("_s + this->Children.front()->Output_Serial() + "/"_s + this->Children.back()->Output_Serial() + ")";
    }
    FUNCERR("Found a division node with " << this->Children.size() << " children (should be exactly 2.)");
    return "";
}
bool FuncDivideNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 2, "Found a FuncDivideNode with !exactly two child");
    return true;       
}
size_t FuncDivideNode::Min_Num_Children(void) const { return 2; }

//----- ContParenthesisNode.
std::string ContParenthesisNode::Output_Graph_Label(void) const { 
    return std::string("ParenthesisNode \\n ()"); 
}
std::string ContParenthesisNode::Output_Serial(void) const {
    if(this->Validate_Node()){
        return "("_s + this->Children.front()->Output_Serial() + ")";
    }
    FUNCERR("Found a parenthesis node with " << this->Children.size() << " children (should be exactly 1.)");
    return "";
}
bool ContParenthesisNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 1, "Found a FuncParenthesisNode with !exactly one child");
    return true;   
}
size_t ContParenthesisNode::Min_Num_Children(void) const { return 1; }


//----- FuncExponentiateNode.
std::string FuncExponentiateNode::Output_Graph_Label(void) const {
    return std::string("ExponentiateNode \\n ^");
}
std::string FuncExponentiateNode::Output_Serial(void) const {
    if(this->Validate_Node()){
        return "("_s + this->Children.front()->Output_Serial() + "**"_s + this->Children.back()->Output_Serial() + ")";
    }
    FUNCERR("Found an exponentiation node with " << this->Children.size() << " children (should be exactly 2.)");
    return "";
}
bool FuncExponentiateNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 2, "Found a FuncExponentiateNode with !exactly two children");
    return true;    
}
size_t FuncExponentiateNode::Min_Num_Children(void) const { return 2; }

//----- ContCommaNode.
std::string ContCommaNode::Output_Graph_Label(void) const {
    return std::string("CommaNode \\n ,");
}
std::string ContCommaNode::Output_Serial(void) const {
    std::string out("(");
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){
        out += (*it)->Output_Serial();
        if( it != --(this->Children.end()) ) out += ",";
    }
    return out + ")";
}
bool ContCommaNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() >= 2, "Found a FuncCommaNode with <2 children");
    return true;
}
size_t ContCommaNode::Min_Num_Children(void) const { return 2; }

//----- ContAssignmentNode.
std::string ContAssignmentNode::Output_Graph_Label(void) const {
    return std::string("AssignmentNode \\n =");
}
std::string ContAssignmentNode::Output_Serial(void) const {
    return this->Children.front()->Output_Serial() + "="_s + this->Children.back()->Output_Serial();
}
bool ContAssignmentNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 2, "Found a ContAssignmentNode with !exactly two children");
/* 
TODO: This would be nice to implement.
HOW THE HELL CAN I RECURSE ON ALL CHILDREN USING THIS? CAN I BUFFER ALL CHILDREN??

    //Recursively check for other assignment nodes.
    const auto childnodes = this->Get_Lower_Nodes();
    for(auto it = childnodes.begin(); it != childnodes.end(); ++it){
        if((*(*it))->Type == NodeType::ContAssignment) return false;
    }
*/
    return true;
}
size_t ContAssignmentNode::Min_Num_Children(void) const { return 2; }

 
//----- FuncLessThanNode.
std::string FuncLessThanNode::Output_Graph_Label(void) const {
    return std::string("LessThanNode \\n <");
}
std::string FuncLessThanNode::Output_Serial(void) const {
    return this->Children.front()->Output_Serial() + "<"_s + this->Children.back()->Output_Serial();
}
bool FuncLessThanNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 2, "Found a FuncLessThanNode with !exactly two children");
    RET_FLS_IF_FLS(this->Raw_Expr.empty(), "Found a FuncLessThanNode with a raw expression");
    return true;
}
size_t FuncLessThanNode::Min_Num_Children(void) const { return 2; }

//----- FuncMoreThanNode.
std::string FuncMoreThanNode::Output_Graph_Label(void) const {
    return std::string("MoreThanNode \\n >");
}
std::string FuncMoreThanNode::Output_Serial(void) const {
    return this->Children.front()->Output_Serial() + ">"_s + this->Children.back()->Output_Serial(); 
}
bool FuncMoreThanNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 2, "Found a FuncMoreThanNode with !exactly two children");
    RET_FLS_IF_FLS(this->Raw_Expr.empty(), "Found a FuncMoreThanNode with a raw expression");
    return true;
}
size_t FuncMoreThanNode::Min_Num_Children(void) const { return 2; }

//----- FuncLessThanEqNode.
std::string FuncLessThanEqNode::Output_Graph_Label(void) const {
    return std::string("LessThanEqNode \\n <=");
}
std::string FuncLessThanEqNode::Output_Serial(void) const {
    return this->Children.front()->Output_Serial() + "<="_s + this->Children.back()->Output_Serial();
}
bool FuncLessThanEqNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 2, "Found a FuncLessThanEqNode with !exactly two children");
    RET_FLS_IF_FLS(this->Raw_Expr.empty(), "Found a FuncLessThanEqNode with a raw expression");
    return true;
}
size_t FuncLessThanEqNode::Min_Num_Children(void) const { return 2; }

//----- FuncMoreThanEqNode.
std::string FuncMoreThanEqNode::Output_Graph_Label(void) const {
    return std::string("MoreThanEqNode \\n >=");
}
std::string FuncMoreThanEqNode::Output_Serial(void) const {
    return this->Children.front()->Output_Serial() + ">="_s + this->Children.back()->Output_Serial(); 
}
bool FuncMoreThanEqNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.size() == 2, "Found a FuncMoreThanEqNode with !exactly two children");
    RET_FLS_IF_FLS(this->Raw_Expr.empty(), "Found a FuncMoreThanEqNode with a raw expression");
    return true; 
}
size_t FuncMoreThanEqNode::Min_Num_Children(void) const { return 2; }


//---------------------------------------------------------------------------------------------------------------------
//--------------------------------------- Numerical BaseNode-derived classes ------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
//----- ExprRationalNode.
ExprRationalNode::ExprRationalNode() : BaseNode(NodeType::ExprRational), x(0) { }                                              

//ExprRationalNode::ExprRationalNode(const std::string &inRaw_Expr) : BaseNode(NodeType::YYYY, inRaw_Expr), x(inRaw_Expr) { }     

template <class T> ExprRationalNode::ExprRationalNode(T in) : BaseNode(NodeType::ExprRational), x(in) { this->x.canonicalize(); }

//ExprRationalNode::~ExprRationalNode(){// = default;
//    FUNCINFO("Called ~ExprRationalNode()");
//}

#ifdef STRUCTS_CC_INCLUDE_ALL_SPECIALIZATIONS
    template ExprRationalNode::ExprRationalNode(int in);
    template ExprRationalNode::ExprRationalNode(long int in);
    template ExprRationalNode::ExprRationalNode(const std::string &in);
    template ExprRationalNode::ExprRationalNode(mpq_class in);
#endif

template <class T> bool ExprRationalNode::SetX(T in){
    this->x = mpq_class(in);
    this->x.canonicalize();
    return true;
}
#ifdef STRUCTS_CC_INCLUDE_ALL_SPECIALIZATIONS
    template bool ExprRationalNode::SetX(int in);
    template bool ExprRationalNode::SetX(long int in);
    template bool ExprRationalNode::SetX(const std::string &in);
    template bool ExprRationalNode::SetX(mpq_class in);
#endif
       
std::unique_ptr<BaseNode> ExprRationalNode::Duplicate(void) const {
    auto out = Numerical_Node_Factory<ExprRationalNode,mpq_class>(this->x);
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){                 
        out->Children.push_back(nullptr);                                                    
        auto childnode = (*it)->Duplicate();                                                 
        (*(out->Children.rbegin())) = std::move(childnode);                                   
    }                                                                                        
    return out;                                                                   
}    

std::string ExprRationalNode::Output_Graph_Label(void) const {
    return std::string("ExprRationalNode \\n " + this->x.get_str());
}
std::string ExprRationalNode::Output_Serial(void) const {
    return "("_s + this->x.get_str() + ")";
}
bool ExprRationalNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.empty(), "Found an ExprRationalNode with children");
    return true;
}
size_t ExprRationalNode::Min_Num_Children(void) const { return 1; }

//----- ExprDoubleNode.
ExprDoubleNode::ExprDoubleNode() : BaseNode(NodeType::ExprDouble), x(0) { }                                              

//ExprDoubleNode::ExprDoubleNode(const std::string &inRaw_Expr) : BaseNode(NodeType::YYYY, inRaw_Expr), x(inRaw_Expr) { }     

//ExprDoubleNode::~ExprDoubleNode() = default;

template <class T> ExprDoubleNode::ExprDoubleNode(T in) : BaseNode(NodeType::ExprDouble), x(static_cast<double>(in)) { }
#ifdef STRUCTS_CC_INCLUDE_ALL_SPECIALIZATIONS
    template ExprDoubleNode::ExprDoubleNode(float in);
    template ExprDoubleNode::ExprDoubleNode(double in);
    template ExprDoubleNode::ExprDoubleNode(long int in);
#endif

template <class T> bool ExprDoubleNode::SetX(T in){
    this->x = static_cast<double>(in);
    return true;
}
#ifdef STRUCTS_CC_INCLUDE_ALL_SPECIALIZATIONS
    template bool ExprDoubleNode::SetX(float in);
    template bool ExprDoubleNode::SetX(double in);
    template bool ExprDoubleNode::SetX(long int in);
#endif
       
std::unique_ptr<BaseNode> ExprDoubleNode::Duplicate(void) const {
    auto out = Numerical_Node_Factory<ExprDoubleNode,double>(this->x);
    for(auto it = this->Children.begin(); it != this->Children.end(); ++it){                 
        out->Children.push_back(nullptr);                                                    
        auto childnode = (*it)->Duplicate();                                                 
        (*(out->Children.rbegin())) = std::move(childnode);                                   
    }                                                                                        
    return out;                                                                   
}    
std::string ExprDoubleNode::Output_Graph_Label(void) const {
    return std::string("ExprDoubleNode \\n " + XtoPreciseString<double>(this->x));
}
std::string ExprDoubleNode::Output_Serial(void) const {
    return "("_s + XtoPreciseString<double>(this->x) + ")";
}
bool ExprDoubleNode::Validate_Node(void) const {
    RET_FLS_IF_FLS(this->Children.empty(), "Found an ExprDoubleNode with children");
    return true;
}
size_t ExprDoubleNode::Min_Num_Children(void) const { return 1; }





 
