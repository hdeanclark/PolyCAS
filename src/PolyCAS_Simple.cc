//PolyCAS_Simple.cc - Written by hal clark in 2013.
//
// This is a library which is used to simplify using PolyCAS within other projects.
//
// Not all functionality is or should be exposed. This would be like trying to squeeze
// a toolbox through a needle. Instead, only simplified or customized techniques are 
// provided.
//

#include <iostream>
#include <list>
#include <string>
#include <memory>

#include "PolyCAS_Structs.h"
#include "PolyCAS_Parsing.h"
#include "PolyCAS_Numerical.h"
#include "PolyCAS_Simplify.h"
#include "PolyCAS_Substitution.h"

#include "YgorMisc.h"
#include "YgorFilesDirs.h"
#include "YgorPerformance.h"

//This function takes an expression and a list of substitutions (of the form "a=b") and returns either
// no result or a numerical value.
//
//NOTES: 
//    1) The return value is undefined if wasOK is set to false.
//    2) The list of substitutions can reference one another.
//    3) The list of substitutions are performed ONCE each, from front to back.
//    4) This function cannot return a non-numerical result.
//
double PolyCAS_Eval(bool *wasOK, const std::string &expr, const std::list<std::string> &subs){
    if(wasOK == nullptr) FUNCERR("Passed a nullptr bool. Unable to signal whether evaluation worked or not");
    double result = -1.0;
    *wasOK = false;

    //Parse the expression into a tree. Validate the parsed result.
    auto parsed = Parse_String(expr);
    if(!parsed->Validate_Tree()){
        FUNCWARN("Validation of parsed expression failed");
        return result; //FUNCERR("Validation of parsed expression failed");
    }

    //Cycle through the list of substitutions, parsing each. Substitute into the main expression.
    if(!subs.empty()){
        std::list<std::unique_ptr<BaseNode>> sub_exprs;
        for(auto s_it = subs.begin(); s_it != subs.end(); ++s_it){
            sub_exprs.push_back( Parse_String(*s_it) );
        }

        parsed = Substitute_Basic(std::move(parsed), sub_exprs);
        if(!parsed->Validate_Tree()){
            FUNCWARN("Validation of parsed and subsituted expression failed");
            return result; //FUNCERR("Validation of parsed and subsituted expression failed");
        }
    }

    //Attempt a numerical evaluation of the tree. Perform it many times for better timing results.
    parsed = Numerical_Basic<double>(std::move(parsed), wasOK, &result);
    return result; //User can check wasOK for status of numeric conversion.
}



