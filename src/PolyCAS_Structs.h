//Structs.h - Definitions of the most basic core containers.

#ifndef PROJECT_POLYCAS_STRUCTS_H_HDR_GRD_
#define PROJECT_POLYCAS_STRUCTS_H_HDR_GRD_

#include <string>
#include <memory>
#include <list>

#include <gmpxx.h>     //Needed for ExprRational and ExprFloat (numerical) types.

namespace NodeType {
    //NOTE: These values are NOT arbitrary. They can be adjusted, but specific values are relied upon in:
    //    - Numerical.h
    //    
    // Do not *adjust* existing values in any way until the above files have been cleared/adjusted.

    //Parsing-specific nodes.
    const unsigned long int Shuttle           = 0x00;

    //Control and flow nodes.
    const unsigned long int ContParenthesis   = 0x01;
    const unsigned long int ContComma         = 0x02;
    const unsigned long int ContAssignment    = 0x03;

    //Un-parseable statements like variables or numbers.
    const unsigned long int Expr              = 0x04; //Holds all non-specialized expressions.
    const unsigned long int ExprRational      = 0x05; //This is an arbitrary-precision rational number.
    const unsigned long int ExprDouble        = 0x06; //This is a machine-precision double.

    //Functions and basic operations.
    const unsigned long int Func              = 0x07; //Holds all functions except operations.
    const unsigned long int FuncAdd           = 0x08;
    const unsigned long int FuncSubtract      = 0x09;
    const unsigned long int FuncNegate        = 0x0a;
    const unsigned long int FuncMultiply      = 0x0b;
    const unsigned long int FuncDivide        = 0x0c;
    const unsigned long int FuncExponentiate  = 0x0d;

    const unsigned long int FuncLessThan      = 0x0e;
    const unsigned long int FuncMoreThan      = 0x0f;
    const unsigned long int FuncLessThanEq    = 0x10;
    const unsigned long int FuncMoreThanEq    = 0x11;

}


//---------------------------------------------------------------------------------------------------------------------
//------------------------------------------------- class BaseNode ----------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
//This class defines the nodes of a tree data structure. Each node can hold children and is of some derived type.
// Usually, the derived type enforces a specific operation on the children (such as an FuncAddNode) or simply holds
// a raw string containing a variable or an atom of either yet-to-be-parsed or unable-to-be-parsed text.
//
class BaseNode {
    public:

        //Implementation-specific members.
//        BaseNode *Parent; //Needed??
        std::list<std::unique_ptr<BaseNode>> Children;

        //Constructors and Destructors.
        BaseNode();
        BaseNode(unsigned long int inType);
        BaseNode(unsigned long int inType, const std::string &inRaw_Expr);
        virtual ~BaseNode(); //Virtual because we will often cast to BaseNode and thus the wrong destructor might be called.

        //Members.
        unsigned long int Type;         //Identifier, used to differentiate derived classes. 
        std::string Raw_Expr;           //Holds any string data required (such as raw variable names, function names, etc.)

        //Pure-virtual members.
        virtual std::string Output_Serial(void) const =0;  //Output the data structure as-is into a (serial, parseable, human-readable) format.
        virtual bool Validate_Node(void) const =0;       //Attempt to ensure each individual node seems 'reasonable.' 
        virtual std::string Output_Graph_Label(void) const =0; //Outputs the type of the node and the contents/operation.
        virtual std::unique_ptr<BaseNode> Duplicate(void) const =0; //Creates a duplicate (copy of all data) of itself and all children nodes.
        virtual size_t Min_Num_Children(void) const =0; //Minimal number of children a valid expression should have.

        //Const member functions.
        std::string Output_Graph_ID(void) const; //long int depth) const;    //Output a unique ID describing the (local) node.
        std::string Output_Parse_Tree(long int depth = 0) const;
        bool Validate_Nodes_Memory(void) const; //Verifies all pointers are non-nullptr.
        bool Validate_Tree(void) const;
//        std::list<std::unique_ptr<BaseNode> *> Get_Lower_Nodes(void) const;
//        auto  Get_Lower_Nodes(void) const -> decltype( std::list<this->Children.begin()> );
        std::list<std::list<std::unique_ptr<BaseNode>>::const_iterator> Get_Lower_Nodes(void) const;
        bool operator == (const BaseNode &rhs) const;
        bool operator != (const BaseNode &rhs) const;
        bool Are_Equiv(const BaseNode &rhs) const;
        bool Are_Not_Equiv(const BaseNode &rhs) const;
};


//Convenient template helper-code for simplistic derivations from BaseNode.
#ifndef PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE_DEFINED
  #define PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE_DEFINED
  #define PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE( XXXX )    \
    class XXXX : public BaseNode {                               \
    public:                                                      \
       XXXX();                                                   \
       XXXX(const std::string &inRaw_Expr);                      \
       std::unique_ptr<BaseNode> Duplicate(void) const;          \
       std::string Output_Graph_Label(void) const;               \
       std::string Output_Serial(void) const;                    \
       bool Validate_Node(void) const;                           \
       size_t Min_Num_Children(void) const;                      \
    };
#endif
//       ~XXXX();                                                  

//---------------------------------------------------------------------------------------------------------------------
//----------------------------------------- Simple BaseNode-derived classes -------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

//ShuttleNodes are used to bootstrap the parsing process by holding onto unparsed data. They do nothing else. 
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(ShuttleNode)

//ExprNodes are terminating nodes. They (probably) have no children and hold onto variables in string-form.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(ExprNode) 

//FuncNodes are nodes which hold onto generic functions. They hold the raw function name and have children (func args.)
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncNode)

//FuncAddNodes are a FuncNode specialization. They only denote addition and do not hold onto a func name.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncAddNode)

//FuncSubtractNodes are a FuncNode specialization. They only denote subtraction and do not hold onto a func name.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncSubtractNode)

//FuncNegateNodes are a FuncNode specialization. They only denote negation and do not hold onto a func name.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncNegateNode)

//FuncMultiplyNodes are a FuncNode specialization. They only denote multiplication and do not hold onto a func name.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncMultiplyNode)

//FuncDivideNodes are a FuncNode specialization. They only denote division and do not hold onto a func name.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncDivideNode)

//ContParenthesisNodes are a control node. They denote scope (and are mostly redundant within parse trees for normal types.)
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(ContParenthesisNode)

//FuncExponentiateNodes are a FuncNode specialization. They only denote exponentiation and do not hold onto a func name.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncExponentiateNode)

//ContCommaNodes are a control node. They denote various things, based on context. Commonly, they delineate function arguments.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(ContCommaNode)

//ContAssignmentNodes are a control node. They denote strict assignment (=) and behave mostly like funcnodes. They should only be top nodes!
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(ContAssignmentNode)

//FuncLessThans can denote several things. Generally, they are used as booleans.
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncLessThanNode)
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncMoreThanNode)
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncLessThanEqNode)
PROJECT_POLYCAS_SIMPLE_DERIVE_FROM_BASENODE(FuncMoreThanEqNode)


//---------------------------------------------------------------------------------------------------------------------
//--------------------------------------- Numerical BaseNode-derived classes ------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
class ExprRationalNode : public BaseNode {                               
    public:                                                      
        mpq_class x;      //The actual rational number.

        ExprRationalNode();                                                   
        template <class T> ExprRationalNode(T in);  //This templated function can handle T = const std::string &, but it should be used with care!
//        ~ExprRationalNode();
        template <class T> bool SetX(T in);

        std::unique_ptr<BaseNode> Duplicate(void) const;          
        std::string Output_Graph_Label(void) const;     
        std::string Output_Serial(void) const;                    
        bool Validate_Node(void) const;                           
        size_t Min_Num_Children(void) const;
};

class ExprDoubleNode : public BaseNode {
    public:
        double x;      

        ExprDoubleNode();
        template <class T> ExprDoubleNode(T in);
//        ~ExprDoubleNode();
        template <class T> bool SetX(T in);

        std::unique_ptr<BaseNode> Duplicate(void) const;
        std::string Output_Graph_Label(void) const;
        std::string Output_Serial(void) const;
        bool Validate_Node(void) const;
        size_t Min_Num_Children(void) const;
};


#endif

