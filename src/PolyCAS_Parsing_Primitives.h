//Parsing_Primitives.h - A set of basic parsing routines.
//
//NOTE: As of writing, these routines are not being used. They are kept around because some decent
// testing went into the regex, and I'm confident that they *could* be useful for something, sometime....
//

#ifndef PROJECT_POLYCAS_PARSING_PRIMITIVES_H_HDR_GRD_
#define PROJECT_POLYCAS_PARSING_PRIMITIVES_H_HDR_GRD_

#include <iostream>

#include <string>
#include <memory>
#include <vector>
#include <tuple>

#include "YgorMisc.h"
#include "YgorString.h"

#define PARSING_PRIMITIVES_H_VERBOSE false


std::tuple<std::vector<std::string>,std::vector<std::string>,std::vector<std::string> > 
Tokenize_All_Names_And_Numbers(const std::string &in){
    //This routine will return all (1) variable names, (2) numbers, and (3) function names within the given string.
    //
    //Note: Duplicates are treated as separate entities.
    //
    //Note: These regex are very easy to defeat. In particular, something like: 1.23^(4.56*7.89) will NOT be properly handled,
    // and WILL throw off the remaining routines, generally leading to complete havoc!
    std::vector<std::string> names, numbers, funcnames;
    std::string dup(in);

    //First, we scan for function names.
    funcnames = GetAllRegex2(dup, R"***(([a-zA-Z0-9_{}\]\[\\]+)[(])***");

    //Remove these function names from the input.
    for(auto it = funcnames.begin(); it != funcnames.end(); ++it){
        const auto pos = dup.find(*it);
        if(pos == std::string::npos) FUNCERR("Unable to find function name '" << *it << "' in the given string '" << dup << "'");
        dup.erase( pos, it->size() );
    }

    //Attempt to determine if something went awry.
    if((GetAllRegex2(dup, R"***(([a-zA-Z0-9_{}\]\[\\]+)[(])***")).empty()){
        FUNCERR("Missed a function name in the string '" << dup << "'");
    }


    //Second, we scan for variable names.
//    names = GetAllRegex2(dup, R"***(([a-zA-Z{]+[a-zA-Z0-9_{}\]\[\\]*[a-zA-Z}]+))***");  //Won't match single-character variable names!
    names = GetAllRegex2(dup, R"***(([a-zA-Z{]+(?:[a-zA-Z0-9_{}\]\[\\]*[a-zA-Z}]+)|(?:(?<![0-9.])[a-zA-Z}]+)))***");

    //Remove these names from the input.
    for(auto it = names.begin(); it != names.end(); ++it){
        const auto pos = dup.find(*it);
        if(pos == std::string::npos) FUNCERR("Unable to find variable name '" << *it << "' in the given string '" << dup << "'");
        dup.erase( pos, it->size() );
    }

    //Attempt to determine if something went awry.
    if((GetAllRegex2(dup, R"***(([a-zA-Z{]+(?:[a-zA-Z0-9_{}\]\[\\]*[a-zA-Z}]+)|(?:(?<![0-9.])[a-zA-Z}]+)))***")).empty()){
        FUNCERR("Missed a function name in the string '" << dup << "'");
    }

    //Third, we look for floating-point numbers.
//    numbers = GetAllRegex2(dup, R"***(([-0-9*]+[.]{0,1}[-0-9*]+[E^]*[+-0-9*]+))***");
//    numbers = GetAllRegex2(dup, R"***(([-]?(?:0|[1-9][0-9]*)(?:\.[0-9]+)?(?:[eE^][+-]?[0-9.]+)?))***");
    numbers = GetAllRegex2(dup, R"***(([-]?(?:0|[1-9][0-9]*)(?:\.[0-9]+)?(?:(?:[eE^]|[*]{2})[+-]?[0-9.]+)?))***");

//    numbers = GetAllRegex2(dup, R"***(([-]?(?:0|[1-9][0-9]*)(?:\.[0-9]+)?(?:(?:[eE^])|(?:[*]{2})[+-]?[0-9.]+)?))***"); //Seems to work fine.
//      numbers = GetAllRegex2(dup, R"***(([-]?(?:0|[1-9][0-9]*)(?:\.[0-9]+)?(?:(?:[eE^])|(?:[*]{2})[\+\-]?[0-9.]+)*))***"); //Allows 1.0**1.0**1.0 to be matched as one #.

    //Remove these numbers from the input, but DO NOT remove preceeding '-'s.
    for(auto it = numbers.begin(); it != numbers.end(); ++it){
        const auto pos = dup.find(*it);
        if(pos == std::string::npos) FUNCERR("Unable to re-find number '" << *it << "' in the given string '" << dup << "'");
        if( it->at(0) == '-' ){
            it->erase( 0, 1); //Remove any preceeding '-'s we caught so that they can be properly parsed (else 2-1 would become (2)(-1) => AB insead of (2)-(1) => A-B)
        }
        dup.erase( pos, it->size() );
    }

    //Attempt to determine if something went awry.
    if((GetAllRegex2(dup, R"***(([-]?(?:0|[1-9][0-9]*)(?:\.[0-9]+)?(?:(?:[eE^]|[*]{2})[+-]?[0-9.]+)?))***")).empty()){
        FUNCERR("Missed a function name in the string '" << dup << "'");
    }


    return std::make_tuple(names, numbers, funcnames);
}










#endif
