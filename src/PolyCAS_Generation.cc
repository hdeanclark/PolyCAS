//PolyCAS_Generate.cc - Written by hal clark in 2013.
//
// This code was written to generate (basic, valid) arithmetical expressions involving
// a collection of given expressions (variables, sub-expressions, or anything itself
// a valid arithmetical expression).
//
// It is designed to be used for testing parsing, numerical evaluation, simplification, 
// and can be used for various scientific exploration (e.g. generating and testing
// simple models).
//

#include <iostream>
#include <list>
#include <string>
#include <memory>
#include <random>
#include <iterator>

#include "PolyCAS_Structs.h"
#include "PolyCAS_Parsing.h"
#include "PolyCAS_Numerical.h"
#include "PolyCAS_Simplify.h"
#include "PolyCAS_Substitution.h"

#include "PolyCAS_Generation.h"

#include "YgorMisc.h"
#include "YgorFilesDirs.h"
#include "YgorPerformance.h"


//This is the recursive function which randomly generates basic arithmetical expressions.
// At each recurse, the only signaller we give to any children is (roughly) how many more
// children they should create.
static std::unique_ptr<BaseNode> Local_Use_Randomly_Generate_Basic_Arithmetic_Involving(const std::list<std::string> &vars, 
           const std::list<std::string> &funcs, long int *max_num_vars, long int seed, long int depth = 0){

    //This routine should not be called unless we require a valid node as output. Therefore, if
    // max_num_vars is 0, we choose the shortest node we can (i.e. a single variable from the
    // provided list) and return.
    if(max_num_vars == nullptr) FUNCERR("Passed an invalid pointer (max_num_vars). Unable to signal children and therefore cannot continue");

    const long int num_of_vars_avail = static_cast<long int>(vars.size());
    if(num_of_vars_avail <= 1) FUNCERR("Encountered programming error - not provided any variable to choose from. Cannot continue");

    //Set up the random number generator.
//SLOW!
//std::random_device rdev;
//seed = static_cast<long int>(rdev()); //Is type unsigned int.
    std::default_random_engine re(seed);

    std::unique_ptr<BaseNode> out = std::make_unique<ShuttleNode>();

    //Check if we are constrained to output a child-less node or not.
    if((*max_num_vars) <= 0){
        std::uniform_int_distribution<long int> rd(0,num_of_vars_avail-1); //Endpoint-inclusive.
        long int which = rd(re);
        const std::string the_var = *(std::next(vars.begin(), which));
        out = Node_Factory<ExprNode>(the_var);  // need to Cast_From_To_Node<BaseNode,T>(std::move(*c_it));  ??

    //Otherwise we can generate any node type (including child-less nodes).
    }else{
        bool Generate_Variable_Node = false;
        std::uniform_real_distribution<double>  rd_r(0.0, 1.0); //To choose whether to emit variable node or not.
        std::uniform_int_distribution<long int> rd_i(0,2147483600); //This generates a new seed. Max long int ~2147483647.

        //[avg_expr_len] controls how long the expressions get, on average, before terminating with a variable node.
        // -If short expressions (below the max_num_vars) are desired, make it closer to 1.0.
        // -If long expressions (up to, and around, max_num_vars) are desired, make it closer to 0.0. 
        //
        // There is some bias that occurs if too high or low. If the expressions are dominated by variables on the
        // left (i.e. they look like '(((((((-((A+B)+C)-D...') then [avg_expr_len] is set too high. If output
        // is often much shorter than [*max_num_vars] then [avg_expr_len] is too high. If the expressions
        // are dominated by variables on the right (i.e. '...A+B)-C)+D))))))))))') then [avg_expr_len] is
        // too low.
        //
        //NOTE: most times we can afford to drop a single variable because we have two children to fill.
        // But the root node is a single. If we fill it with a variable, the expression cannot continue. 
        // Thus, we overload this case with 1% probability of generating a terminating node. This is not
        // ideal. Instead, we should examine a running probability (or sit down and work out all theoretical
        // possibilities). Be sure to do so before seriously using this routine because the "randomness" in
        // generation is hampered! -------- This routine is not currently ergodic! --------
        double avg_expr_len = 0.4;
        if((depth == 0)) avg_expr_len = 0.01; //The first && (*max_num_vars != 1
        if(rd_r(re) < avg_expr_len) Generate_Variable_Node = true; 

        //We use recursion to DRY to make a child-less variable node.
        if(Generate_Variable_Node){
            --(*max_num_vars);
            long int trick(0);
            const long int newseed = rd_i(re);
            out = Local_Use_Randomly_Generate_Basic_Arithmetic_Involving(vars,funcs,&trick,newseed,depth+1);

        //Otherwise, we choose an arithmetical operation and then populate the children nodes.
        }else{
/*
            std::uniform_int_distribution<long int> rd(0,5-1); //Endpoint-inclusive.
            const auto choice = rd(re);

            if(choice == 0){
                out = Node_Factory<FuncAddNode>();
            }else if(choice == 1){
                out = Node_Factory<FuncSubtractNode>();
            }else if(choice == 2){
                out = Node_Factory<FuncMultiplyNode>();
            }else if(choice == 3){
                out = Node_Factory<FuncDivideNode>();
            }else if(choice == 4){
                out = Node_Factory<FuncNegateNode>();
            }else{
                FUNCERR("Programming mistake - no node with that number to choose!");
            }
*/

            std::uniform_int_distribution<long int> rd(0,funcs.size() + 5 - 1); //Endpoint-inclusive.
            const auto choice = rd(re);

            if(choice == 0){
                out = Node_Factory<FuncAddNode>();
            }else if(choice == 1){
                out = Node_Factory<FuncSubtractNode>();
            }else if(choice == 2){
                out = Node_Factory<FuncMultiplyNode>();
            }else if(choice == 3){
                out = Node_Factory<FuncDivideNode>();
            }else if(choice == 4){
                out = Node_Factory<FuncNegateNode>();
            }else{
                out = Node_Factory<FuncNode>();
                out->Raw_Expr = *std::next(funcs.begin(), choice - 5); 
            }

            //Expected number of nodes.
            long int min_num_children = -1;
            if(choice < 5){ //Arithmetical types.
                min_num_children = static_cast<long int>(out->Min_Num_Children());
            }else{ //User passed-in functions. We assume they are single argument.
                min_num_children = 1;
            }

            //Now create the minimum number of children possible. We could create more if desired,
            // (Just add a new BaseNode function "Max_Num_Children" and go from there).
            //
            //Arithmetical types.
            for(long int i = 0; i < min_num_children; ++i){
                std::unique_ptr<BaseNode> childnode;
                const long int newseed = rd_i(re);
                childnode = Local_Use_Randomly_Generate_Basic_Arithmetic_Involving(vars,funcs,max_num_vars,newseed,depth+1);
                Append_Node_To_List(out->Children, std::move(childnode));
            }
        }
    }
    return out;
}



//Given a list of variables (or (valid) expressions), output a random expression
// involving the basic arithmetic operations (+-*/) and one or more of the given variables.
//
// The first parameter could be anything which is itself a valid expression: this 
// routine is indifferent. An example might be: {"A", "B", "exp(-C)", "(1+2+3+D)" };
// Each variable will have the same occurrence rate (none are biased to show up more
// frequently then the others). To easily overcome this, provide a variable multiple
// times to increase the overall likelihood of it showing up.
//
// The variables may show up more than once. They are not 'consumed' when they are
// first used. At least one variable should be provided.
//
// The second parameter controls (roughly!) how many variables should show up in the 
// generated expression. There may be more, but not less. 
//
// This function works directly with the expression tree and only emits a string at
// the end. This means that it is probably impossible to produce output which cannot
// be stored in a valid expression tree (and thus limits the utility for self-testing).
// Expressions are NOT simplified.
std::string PolyCAS_Randomly_Generate_Basic_Arithmetic_Involving(const std::list<std::string> &vars, long int max_num_vars, long int seed /*= -1*/){
    //First, ensure the input is sane.
    if(vars.empty()) FUNCERR("Not given any variables/expressions to use in expressions. Unable to continue");
    if(vars.size() == 1){
        //We mostly have binary operations, so we need to ensure we have at least 2 variables to choose from.
        // We just trick the scheme by harmlessly duplicating the provided expression.
        const std::list<std::string> duplicated({vars.front(), vars.front()});
        return PolyCAS_Randomly_Generate_Basic_Arithmetic_Involving(duplicated, max_num_vars);
    }

    if(max_num_vars <= 0) FUNCERR("Not able to produce expressions with max_num_vars = '" << max_num_vars << "'. Bailing");

    //Seed if nothing provided to avoid depleting entropy.
    if(seed == -1){
        std::random_device rdev;
        seed = static_cast<long int>(rdev()); //Is type unsigned int.
    }

    const std::list<std::string> empty_list_of_funcs;
    std::unique_ptr<BaseNode> expression_node = Local_Use_Randomly_Generate_Basic_Arithmetic_Involving(vars, empty_list_of_funcs, &max_num_vars, seed);

    //Emit the serialized expression. Proper parenthesis will (should!) be ensured.
    return expression_node->Output_Serial();
}




//Given a list of variables (or (valid) expressions) and functions of one argument, output a random expression
// involving the basic arithmetic operations (+-*/) and one or more of the given variables.
//
//This routine is very similar to 'PolyCAS_Randomly_Generate_Basic_Arithmetic_Involving' except that single-argument
// functions are also allowed.
std::string PolyCAS_Randomly_Gen_Basic_Arith_Involving_Vars_SV_Funcs(const std::list<std::string> &vars, 
                       const std::list<std::string> &funcs, long int max_num_vars, long int seed /*= -1*/){
    //First, ensure the input is sane.
    if(vars.empty()) FUNCERR("Not given any variables/expressions to use in expressions. Unable to continue");
    if(vars.size() == 1){
        //We mostly have binary operations, so we need to ensure we have at least 2 variables to choose from.
        // We just trick the scheme by harmlessly duplicating the provided expression.
        const std::list<std::string> duplicated({vars.front(), vars.front()});
        return PolyCAS_Randomly_Gen_Basic_Arith_Involving_Vars_SV_Funcs(duplicated, funcs, max_num_vars);
    }

    if(max_num_vars <= 0) FUNCERR("Not able to produce expressions with max_num_vars = '" << max_num_vars << "'. Bailing");

    //Seed if nothing provided to avoid depleting entropy.
    if(seed == -1){
        std::random_device rdev;
        seed = static_cast<long int>(rdev()); //Is type unsigned int.
    }

    std::unique_ptr<BaseNode> expression_node = Local_Use_Randomly_Generate_Basic_Arithmetic_Involving(vars, funcs, &max_num_vars, seed);

    //Emit the serialized expression. Proper parenthesis will (should!) be ensured.
    return expression_node->Output_Serial();
}


