#include <iostream>
#include <string>
#include <list>
#include <tuple>
#include <functional>

#include "YgorMisc.h"

#include "PolyCAS_Fitting.h"

// Compare this with http://www.sigmaplot.com/products/sigmaplot/productuses/prod-uses45.php
// Appears to reproduce the results shown.

int main(int, char**){

    bool wasOK;
    const std::string f = "a*exp(-0.5*((x-x0)/b)^2)";
    const std::list<std::list<double>> data({ 
        {-6.0000, 1.6000},
        {-5.0000, 1.8000},
        {-3.0000, 2.0000},
        {0.0000, 4.0000},
        {2.0000, 2.7500},
        {3.0000, 1.6800},
        {4.0000, 0.6400},
        {6.0000, 0.2000} });
        
    const std::list<std::string> vars({"x=$0"});
    const std::string F_data("$1");
    const std::string Uncertainties("0.3");
    const std::list<std::string> params({"a=4.123","x0=-1.23","b=40.321"});

    //------------
    //Try to do a rough fit to find some sensible initial guesses. This will give us lots of info, including
    // a list of all square-residuals which can be used for better uncertainty estimation.
    const auto rough_res = PolyCAS_Fit_Data(&wasOK, f, data, vars, F_data, "1.0", params, false);
    if(!wasOK){
        FUNCWARN("Rough fit appears to have failed. Proceeding anyways..");
    }
 
    //Using the rough fit params, try a more careful fit with less emphasis on outliers. Also enable verbosity
    const auto rough_params = std::get<0>(rough_res);
    const auto res = PolyCAS_Fit_Data(&wasOK, f, data, vars, F_data, Uncertainties, rough_params, true);
    if(!wasOK){
        FUNCERR("Careful fit appears to have failed");
    }


    //------------
    const auto fit_params   = std::get<0>(res);
    const auto chi_sq       = std::get<1>(res);
    const auto Qvalue       = std::get<2>(res);
    const auto dof          = std::get<3>(res);
    const auto red_chi_sq   = std::get<4>(res);
    const auto raw_coef_det = std::get<5>(res);
    const auto mod_coef_det = std::get<6>(res);

    //Dump the data used (for outside validation, perhaps)
    for(auto r_it = data.begin(); r_it != data.end(); ++r_it){
        for(auto c_it = r_it->begin(); c_it != r_it->end(); ++c_it){
             std::cout << *c_it << " ";
        }
        std::cout << std::endl;
    }

    std::cout << "chi_sq     = " << chi_sq << std::endl;
    std::cout << "Qvalue     = " << Qvalue << std::endl;
    std::cout << "DOF        = " << dof << std::endl;
    std::cout << "red_chi_sq = " << red_chi_sq << std::endl;
    std::cout << "raw_coeff_of_deter = " << raw_coef_det << std::endl;
    std::cout << "mod_coeff_of_deter = " << mod_coef_det << std::endl;


    for(auto it = fit_params.begin(); it != fit_params.end(); ++it)   std::cout << *it << std::endl;
    std::cout << f << std::endl;


    return 0;
}
