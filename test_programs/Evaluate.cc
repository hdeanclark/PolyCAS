//Evaluate.cc.

#include <iostream>

#include <list>
#include <string>
#include <memory>

#include "../src/PolyCAS_Structs.h"
#include "../src/PolyCAS_Parsing.h"

#include "../src/PolyCAS_Numerical.h"
#include "../src/PolyCAS_Simplify.h"

#include "YgorMisc.h"
#include "YgorFilesDirs.h"
#include "YgorPerformance.h"

int main(int argc, char **argv){
    if(argc < 2) FUNCERR("This program evaluates a numerical expression. It requires the expression passed as a string");
    const std::string originalexpression(argv[1]);

    //Parse the expression into a tree. Validate the parsed expression.
    auto parsed = Parse_String(originalexpression);
    if(!parsed->Validate_Tree()) FUNCERR("Validation of parsed expression failed");

/*
This is (maybe?) unneeded. Parsing includes a lossless simplificatin step.

    //Simplify the expression. Validate the result. 
    //
    //This may or may not be desired by the user. In order to resolve some functions (eg. if)
    // this must be performed. It will likely increase runtime. It is possible to not simplify.
    parsed = Simplify_Basic_Lossy(std::move(parsed));
    if(!parsed->Validate_Tree()) FUNCERR("Validation of simplified expression failed");
*/

    //Attempt a numerical evaluation of the tree. Perform it many times for better timing results.
    bool wasOK = false;
    double result = 0.0;
    parsed = Numerical_Basic<double>(std::move(parsed), &wasOK, &result);

    if(wasOK == true){
        std::cout << result << std::endl;
    }else{
        FUNCERR("Numerical evaluation failed");
    }

    return 0;
}


