//Test_Substitution.cc.

#include <iostream>

#include <list>
#include <string>
#include <memory>

#include "PolyCAS_Structs.h"
#include "PolyCAS_Parsing.h"

#include "PolyCAS_Substitution.h"

#include "YgorMisc.h"
#include "YgorFilesDirs.h"

int main(){
    //Push back some expressions for substitution.
    auto base = Parse_String("a=b+c");
    std::list<std::unique_ptr<BaseNode>> originals;
    Append_Node_To_List(originals, Parse_String("b=b+c"));
    Append_Node_To_List(originals, Parse_String("c=2*b"));
    Append_Node_To_List(originals, Parse_String("b=if(0,b,c)"));

    //Validate the parsed expressions.
    if(! base->Validate_Tree()){
        FUNCERR("Validation of parsed expression failed");
    }else{
        FUNCINFO("Validation was OK");
    }
    for(auto it = originals.begin(); it != originals.end(); ++it){
        if(! (*it)->Validate_Tree()){
            FUNCERR("Validation of parsed expression failed");
        }else{
            FUNCINFO("Validation was OK");
        }
    }

    //Output the expression.
//    const std::string parsedoriginal = parsed->Output_Serial();
//    FUNCINFO("The output is: ------------> '" << parsedoriginal << "'");


    //Output the parse tree in a graphical form (for human debugging, validation.)
    WriteStringToFile(base->Output_Parse_Tree(), "/tmp/polycas_parsed.gv");
    Execute_Command_In_Pipe(" cat /tmp/polycas_parsed.gv | dot -Tpng -o /tmp/polycas_parsed.png &>/dev/null && chromium --incognito /tmp/polycas_parsed.png &>/dev/null" );
    for(auto it = originals.begin(); it != originals.end(); ++it){
        WriteStringToFile((*it)->Output_Parse_Tree(), "/tmp/polycas_parsed.gv");
        Execute_Command_In_Pipe(" cat /tmp/polycas_parsed.gv | dot -Tpng -o /tmp/polycas_parsed.png &>/dev/null && chromium --incognito /tmp/polycas_parsed.png &>/dev/null" );
    }

    //Perform the substitution.
    base = Substitute_Basic(std::move(base), originals);

    //Do it several more times, possibly performing nested substitution...
    // Depending on the original expressions, this could produce HUGE expressions!
    for(int i=0; i<10; ++i) base = Substitute_Basic(std::move(base), originals);

    //Output the parse tree again.
    WriteStringToFile(base->Output_Parse_Tree(), "/tmp/polycas_parsed.gv");
    Execute_Command_In_Pipe(" cat /tmp/polycas_parsed.gv | dot -Tpng -o /tmp/polycas_parsed.png &>/dev/null && chromium --incognito /tmp/polycas_parsed.png &>/dev/null" );
    for(auto it = originals.begin(); it != originals.end(); ++it){
        WriteStringToFile((*it)->Output_Parse_Tree(), "/tmp/polycas_parsed.gv");
        Execute_Command_In_Pipe(" cat /tmp/polycas_parsed.gv | dot -Tpng -o /tmp/polycas_parsed.png &>/dev/null && chromium --incognito /tmp/polycas_parsed.png &>/dev/null" );
    }



/*
    //Duplicate the nodes.
    std::list<std::unique_ptr<BaseNode>> dupes;
    for(auto it = originals.begin(); it != originals.end(); ++it){
        Append_Node_To_List(dupes, (*it)->Duplicate());
    }

    //Output the parse tree (again) to ensure duplication was successful.
    for(auto it = dupes.begin(); it != dupes.end(); ++it){
        WriteStringToFile((*it)->Output_Parse_Tree(), "/tmp/polycas_duped.gv");
        Execute_Command_In_Pipe(" cat /tmp/polycas_duped.gv | dot -Tpng -o /tmp/polycas_duped.png &>/dev/null && chromium --incognito /tmp/polycas_duped.png &>/dev/null " );
    }
*/   
 


/*
    //Re-validate the parsed expression.
    if(!simplified->Validate_Tree()){
        FUNCERR("Validation of reparsed, simplified expression failed");
    }else{
        FUNCINFO("Validation was OK");
    }

    //Output the simplified expression. It should be mathematically-equivalent to the pre-simplified form, but may differ in 
    // ordering, parenthesis placement, etc..
    const std::string simplifiedreparsed = simplified->Output_Serial();
    FUNCINFO("The simplified out is:  ---> '" << simplifiedreparsed << "'");

    //Output the simplified parse tree in a graphical form (for human debugging, validation.)
    WriteStringToFile(simplified->Output_Parse_Tree(), "/tmp/polycas_simplified.gv");
    Execute_Command_In_Pipe(" cat /tmp/polycas_simplified.gv | dot -Tpng -o /tmp/polycas_simplified.png &>/dev/null && chromium --incognito /tmp/polycas_simplified.png &>/dev/null &" );

*/

    FUNCINFO("Test was successful");
    return 0;
}


