#include <iostream>
#include <string>
#include <list>
#include <tuple>
#include <functional>

#include "YgorMisc.h"

#include "PolyCAS_Fitting.h"


int main(int, char**){

    bool wasOK;
    const std::string f = "A*x+B*y+C";
    const std::list<std::list<double>> data({ 
         {1.0, 1.0, 3.0}, 
         {2.0, 1.0, 3.1}, 
         {3.0, 1.0, 3.2},
         {4.0, 1.0, 3.4}, 
         {1.0, 2.0, 4.0}, 
         {2.0, 2.0, 4.2},
         {3.0, 2.0, 4.3}, 
         {4.0, 2.0, 4.5}, 
         {1.0, 4.0, 5.4},
         {2.0, 4.0, 5.6}, 
         {3.0, 4.0, 6.0}, 
         {3.5, 4.1, 3.0} });
        
    const std::list<std::string> vars({"x=$0","y=$1"});
    const std::string F_data("$2");
    const std::string Uncertainties("0.25*$2");
    const std::list<std::string> params({"A=0.2","B=0.2","C=2.0"});

    //------------
    const auto res = PolyCAS_Fit_Data(&wasOK, f, data, vars, F_data, Uncertainties, params);
    if(!wasOK){
        FUNCERR("Fit appears to have failed");
    }

    //------------
    const auto fit_params = std::get<0>(res);
    const auto chi_sq     = std::get<1>(res);
    const auto Qvalue     = std::get<2>(res);
    const auto dof        = std::get<3>(res);
    const auto red_chi_sq = std::get<4>(res);

    //Dump the data used (for outside validation, perhaps)
    for(auto r_it = data.begin(); r_it != data.end(); ++r_it){
        for(auto c_it = r_it->begin(); c_it != r_it->end(); ++c_it){
             std::cout << *c_it << " ";
        }
        std::cout << std::endl;
    }

    std::cout << "chi_sq     = " << chi_sq << std::endl;
    std::cout << "Qvalue     = " << Qvalue << std::endl;
    std::cout << "DOF        = " << dof << std::endl;
    std::cout << "red_chi_sq = " << red_chi_sq << std::endl;


    for(auto it = fit_params.begin(); it != fit_params.end(); ++it)   std::cout << *it << std::endl;
    std::cout << f << std::endl;


    return 0;
}
