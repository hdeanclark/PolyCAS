
#include <string>
#include <vector>
#include <tuple>

#include "YgorMisc.h"
#include "YgorString.h"
#include "PolyCAS_Parsing_Primitives.h"


//Output a breakdown of what names were found in the input.
void Test_Tokenization(const std::string &in){
    std::cout << "Testing tokenization of string: " << in << std::endl;
    std::cout << "NOTE: The regex *will* catch preceeding '-'s, but they are trimmed so the expressions can be more easily parsed!" << std::endl;
    const auto out = Tokenize_All_Names_And_Numbers(in);
    
    std::cout << "Found variable names: " << std::endl;
    for(auto it = (std::get<0>(out)).begin(); it != (std::get<0>(out)).end(); ++it){
        std::cout << "    " << *it << std::endl;
    }

    std::cout << "Found numbers: " << std::endl;
    for(auto it = (std::get<1>(out)).begin(); it != (std::get<1>(out)).end(); ++it){
        std::cout << "    " << *it << std::endl;
    }

    std::cout << "Found function names: " << std::endl;
    for(auto it = (std::get<2>(out)).begin(); it != (std::get<2>(out)).end(); ++it){
        std::cout << "    " << *it << std::endl;
    }

    return;
}

int main(int argc, char **argv){

    Test_Tokenization("1.23+exp(sin(trig_func(x*y*z+(a*(b*(c))))))/-5.55E-10.3+6.678987^+9.876+a_{3}");
    Test_Tokenization("1.0000**(1.0000**(1.0000E(-49.3E(23.123**-6.8))))");


    return 0;
}

