#!/usr/bin/env bash

COMMON_FLAG=" -std=c++11 -g -Ofast -pedantic -Wall -funsafe-math-optimizations "
COMMON_SRCS=" ../Structs.cc "
COMMON_LIBS=" -lgmpxx -lgmp -lygor "

g++ $COMMON_FLAG Test_Parsing.cc      -o test_parsing      $COMMON_SRCS $COMMON_LIBS
g++ $COMMON_FLAG Test_Numerical.cc    -o test_numerical    $COMMON_SRCS $COMMON_LIBS 
g++ $COMMON_FLAG Evaluate.cc          -o evaluate          $COMMON_SRCS $COMMON_LIBS 
g++ $COMMON_FLAG Test_Substitution.cc -o test_substitution $COMMON_SRCS $COMMON_LIBS 
g++ $COMMON_FLAG Test_Parsing_2.cc    -o test_parsing_2    $COMMON_SRCS $COMMON_LIBS 


exit

### OLD - Needed??

g++ -std=c++11 -g Test_Parsing_Primitives.cc  -o test_tokenization -lygor 
