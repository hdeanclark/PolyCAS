//Test_Numerical.cc.

#include <iostream>

#include <list>
#include <string>
#include <memory>

#include "PolyCAS_Structs.h"
#include "PolyCAS_Parsing.h"

#include "PolyCAS_Numerical.h"
#include "PolyCAS_Simplify.h"

#include "YgorMisc.h"
#include "YgorFilesDirs.h"
#include "YgorPerformance.h"

int main(){
    const std::string originalexpression("1.000+3E-1-0.5*sin(1.2345678)*cos(1.45678)/0.5");
    FUNCINFO("The input is: -------------> '" << originalexpression << "'");

    //Parse the expression into a tree. Validate it.
    YGORPERF_TIMESTAMP_DT;
    auto parsed = Parse_String(originalexpression);
    YGORPERF_TIMESTAMP_DT;
    if(!parsed->Validate_Tree()){
        FUNCERR("Validation of parsed expression failed");
    }else{
        FUNCINFO("Validation was OK");
    }

    //Simplify the expression. Validate the result. (This may or may not be desired by the 
    // user - it is OK to leave it out. Execution speeds may increase or decrease!)
    parsed = Simplify_Basic_Lossy(std::move(parsed));
    if(!parsed->Validate_Tree()){
        FUNCERR("Validation of simplified expression failed");
    }else{
        FUNCINFO("Simplification was OK");
    }


    //Attempt a numerical evaluation of the tree. Perform it many times for better timing results.
    bool wasOK = false;
    double result = 0.0;

    YGORPERF_TIMESTAMP_DT;
    for(int i = 0; i < 1E6; ++i){
        parsed = Numerical_Basic<double>(std::move(parsed), &wasOK, &result);
    }
    YGORPERF_TIMESTAMP_DT;

    if(wasOK == true){
        FUNCINFO("The numerical evaluation was OK. The result was: " << result );
    }else{
        FUNCERR("The numerical evaluation was NOT OK!");
    }

    FUNCINFO("Test was successful");
    return 0;
}


