#include <iostream>

#include <list>
#include <string>
#include <memory>

#include "PolyCAS_Structs.h"
#include "PolyCAS_Parsing.h"

#include "PolyCAS_Simplify.h"

#include "YgorMisc.h"
#include "YgorFilesDirs.h"

int main(){
//    const std::string originalexpression("((a+b)*(c+d)) + (((a+b+c))) + exp(g(x,y,cos(sin(x)))) ^ F(x)");
//    const std::string originalexpression("(a-b-c-d-e-f-g-h-i-j)/(a+b-c+d-e+f-g+h-i+j)");
//    const std::string originalexpression("a - f(x,y,sqrt(x**2 + y**2))*log(atan2(x,sqrt(x**2 + y**2)))");
//    const std::string originalexpression("J(x,y,a) = a - f(x,y,sqrt(x**2 + y**2))*log(atan2(x,sqrt(x**2 + y**2)))");
//    const std::string originalexpression("((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))/((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))*((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))+((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))-((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))**((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))/((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))*((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))*((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))+((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))+((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))-((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))+((a-f(x,y,sqrt(((x**2)+(y**2)))))*log(atan2(x,sqrt(((x**2)+(y**2))))))");
//    const std::string originalexpression("a-b**c");
//    const std::string originalexpression("1.000**2.000**3.000");  //Should be 1.000**(2.000**3.000).
//    const std::string originalexpression("10+3-2"); //Should give 11.
//    const std::string originalexpression("10-3+2"); //Should give 9.
//    const std::string originalexpression("-a**b");  //Should be -(a**b).
//    const std::string originalexpression("-(-a**b)*(-(-a)^(-b+c*d/e))");
//    const std::string originalexpression("-(-a**b)*(-(-a)^(-b+c*200/100))+1.02E6+1.2345678");
//    const std::string originalexpression("(1.2345678 + 3.141592653)*(2.3456789 - 0.5*3.141592653)/(3.456789)");
//    const std::string originalexpression("1.000 + 3E-1 - 0.5*sin(1.2345678) * cos(1.45678) / 0.5");
//    const std::string originalexpression("(a+b)+2*(a+b) +c-c");
//    const std::string originalexpression("a+a+b+c+b+c");
//    const std::string originalexpression("if(1+1-2,1111,9999)");
//    const std::string originalexpression("if(1,1,0)"); // 1
//    const std::string originalexpression("if(0,1,0)"); // 0
//    const std::string originalexpression("if(0+2,1,0)"); // 1
//    const std::string originalexpression("if(0+2-2,1,0)"); // 0
//    const std::string originalexpression("if(-0+2-2,1,0)"); // 0
//    const std::string originalexpression("if(-0+2-2,1*1,0)"); // 0
//    const std::string originalexpression("if(1+1-2+1,1*1,0)"); // 1
//    const std::string originalexpression("1*if(1+1-2+1,1,0)"); // 1
//    const std::string originalexpression("1*if(1+1-2+1-1,1,0)"); // 0
//    const std::string originalexpression("1*if(1+1-2+1,if(0,1,0),0)"); // 0
//    const std::string originalexpression("1*if(1+1-2+1,if(1,1,0),0)"); // 1
//    const std::string originalexpression("1<2"); // 1
    const std::string originalexpression("-1<2"); // 1
//    const std::string originalexpression("(a-a+0)+(b-b-0)-(0+0+a*0+b*0+0/c)+(0/0)+(12345678901234/0)");  //Should be missingno + inf
//    const std::string originalexpression("(a-a)-(a+b+c)");
//    const std::string originalexpression("123^8");
//    const std::string originalexpression("12345^12345");  //Very large! Seems to work OK (I think?)

//These do not work (due to flaws in this project.)
//    const std::string originalexpression("1.000 + 3.12345678E200");  //The exponent is too large for the double-to-rational routine to handle.

//Are bad and should not work.
//    const std::string originalexpression("J(x,y,a) = a - f(x=10,y,sqrt(x**2 + y**2))*log(atan2(x,sqrt(x**2 + y**2)))"); //Failure: two '='s

    FUNCINFO("The input is: -------------> '" << originalexpression << "'");

    //--------------------- Initial parsing -----------------------
    //Parse the expression into a tree.
    auto parsed = Parse_String(originalexpression);
    const auto parseddup = parsed->Duplicate();

    //Validate the parsed expression.
    if(!parsed->Validate_Tree()){
        FUNCERR("Validation of parsed expression failed");
    }else{
        FUNCINFO("Validation was OK");
    }

    //Dump the tree into a string.
    const std::string parsedoriginal = parsed->Output_Serial();
    FUNCINFO("The output is: ------------> '" << parsedoriginal << "'");
 
    //Output the parse tree in a graphical form (for human debugging, validation.)
    WriteStringToFile(parsed->Output_Parse_Tree(), "/tmp/polycas_parsed1.gv");
    Execute_Command_In_Pipe(" cat /tmp/polycas_parsed1.gv | dot -Tpng -o /tmp/polycas_parsed1.png &>/dev/null && chromium --incognito /tmp/polycas_parsed1.png &>/dev/null &" );

    WriteStringToFile(parseddup->Output_Parse_Tree(), "/tmp/polycas_parsed1dup.gv");
    Execute_Command_In_Pipe(" cat /tmp/polycas_parsed1dup.gv | dot -Tpng -o /tmp/polycas_parsed1dup.png &>/dev/null && chromium --incognito /tmp/polycas_parsed1dup.png &>/dev/null &" );

    //------------------------ Re-parsing --------------------------
    //Now, read in the parsed output as if it were an expression.
    auto reparsed = Parse_String(parsedoriginal);
    const auto reparseddup = reparsed->Duplicate();

    //Validate the parsed expression.
    if(!reparsed->Validate_Tree()){
        FUNCERR("Validation of reparsed expression failed");
    }else{
        FUNCINFO("Validation was OK");
    }

    //Dump the tree into a string.
    const std::string reparsedoriginal = reparsed->Output_Serial();
    FUNCINFO("The reparsed output is: ---> '" << reparsedoriginal << "'");

    //Output the parse tree in a graphical form (for human debugging, validation.)
    WriteStringToFile(reparsed->Output_Parse_Tree(), "/tmp/polycas_parsed2.gv");
    Execute_Command_In_Pipe(" cat /tmp/polycas_parsed2.gv | dot -Tpng -o /tmp/polycas_parsed2.png &>/dev/null && chromium --incognito /tmp/polycas_parsed2.png &>/dev/null &" );

    //------------------ Comparison of Parsings ------------------
    //Verify that the equivalence procedure works.
    if(parseddup->Are_Not_Equiv(*parseddup)){
        FUNCERR("Equivalence function improperly comparing nodes (1)");
    }else if(reparseddup->Are_Not_Equiv(*reparseddup)){
        FUNCERR("Equivalence function improperly comparing nodes (2)");
    }else{
        FUNCINFO("Properly evaluated equivalence function on duplicated nodes");
    }


    //Verify that the same thing was produced.
    //if(parsedoriginal != reparsedoriginal){ //Compare the stringified output (bad idea.)
    if(parseddup->Are_Not_Equiv(*reparseddup)){ //Compare the trees directly (better idea, but very difficult to do!)
        FUNCWARN("Reparsing the parsed expression does NOT yield an identical expression!");
        FUNCWARN("   |--> This means there is something wrong with either the parser or the output routines.");
        FUNCWARN("   |--> OR, it might mean that the simplification rules were able to eliminate an impossible branch");
        FUNCERR("Parse error (see previous.)");
    }else{
        FUNCINFO("Successfully reparsed the parsed expression, getting the same thing out afterward.");
    }

    //---------------------- Simplification -------------------------
    //Perform the most basic simplifications on the parse tree.
    auto simplified =  Simplify_Basic_Lossy(std::move(reparsed));

    //Re-validate the parsed expression.
    if(!simplified->Validate_Tree()){
        FUNCERR("Validation of reparsed, simplified expression failed");
    }else{
        FUNCINFO("Validation was OK");
    }

    //Output the simplified expression. It should be mathematically-equivalent to the pre-simplified form, but may differ in 
    // ordering, parenthesis placement, etc..
    const std::string simplifiedreparsed = simplified->Output_Serial();
    FUNCINFO("The simplified out is:  ---> '" << simplifiedreparsed << "'");

    //Output the simplified parse tree in a graphical form (for human debugging, validation.)
    WriteStringToFile(simplified->Output_Parse_Tree(), "/tmp/polycas_simplified.gv");
    Execute_Command_In_Pipe(" cat /tmp/polycas_simplified.gv | dot -Tpng -o /tmp/polycas_simplified.png &>/dev/null && chromium --incognito /tmp/polycas_simplified.png &>/dev/null &" );

    //-------------------------------------------------------------

    FUNCINFO("Test was successful");
    return 0;
}


