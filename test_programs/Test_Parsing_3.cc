//Test_Parsing_3.cc - This program takes a list of expressions and tests parsing/simplification against Maxima.
//                    It is meant to test specific nuances of simplification.

#include <iostream>

#include <list>
#include <string>
#include <memory>

#include "PolyCAS_Structs.h"
#include "PolyCAS_Parsing.h"

#include "PolyCAS_Simplify.h"

#include "YgorMisc.h"
#include "YgorFilesDirs.h"

int main(){
    std::list<std::string> test_expressions;
    {
        auto more = LoadFileToList("../Data/Raw_Axiom_CATS");
        test_expressions.splice( test_expressions.begin(), more);
    }
    {
        auto more = LoadFileToList("../Data/Simplification_Tests");
        //Remove comments.
        for(auto it = more.begin(); it != more.end(); ){
            if((it->size() < 3) || (((*it)[0] == '/') && ((*it)[1] == '/')) ){
                it = more.erase(it);
            }else{
                ++it;
            }
        }
        test_expressions.splice( test_expressions.begin(), more);
    }


    for(auto s_it = test_expressions.begin(); s_it != test_expressions.end(); ++s_it){

        FUNCINFO("---- Performing validation of expression: " << *s_it );

        auto parsed = Parse_String(*s_it);
        if(!parsed->Validate_Tree()){
            FUNCERR("Validation of parsed expression failed");
        }
        const std::string parsedoriginal = parsed->Output_Serial();

        auto reparsed = Parse_String(parsedoriginal);
        if(!reparsed->Validate_Tree()){
            FUNCERR("Validation of reparsed expression failed");
        }
        const std::string reparsedoriginal = reparsed->Output_Serial();

        if(parsedoriginal != reparsedoriginal){
            FUNCWARN("Reparsing the parsed expression does NOT yield an identical expression!");
            FUNCWARN("   |--> This means there is something wrong with either the parser or the output routines.");
            FUNCERR("Parse error (see previous.)");
        }
    
//        Execute_Command_In_Pipe(" cat /tmp/polycas_parsed.gv | dot -Tpng -o /tmp/polycas_parsed.png &>/dev/null && chromium --incognito /tmp/polycas_parsed.png &>/dev/null &" );
    
        //Perform the most basic simplifications on the parse tree.
        auto simplified =  Simplify_Basic_Lossy(std::move(reparsed));
        if(!simplified->Validate_Tree()){
            FUNCERR("Validation of reparsed, simplified expression failed");
        }
        const std::string simplifiedreparsed = simplified->Output_Serial();
    
        //Compare the difference of the original and simplified using Maxima.
        std::string cmd;
        cmd += " maxima --very-quiet --run-string=\"display2d: false; ";
        cmd += "ratsimp( ("_s + *s_it + ") - ("_s + simplifiedreparsed + ") ); \" 2>&1 | tail -n 1 | tr --delete '\\n' "_s;

        const auto res = Execute_Command_In_Pipe(cmd);
 
        if(res != "0"){
            FUNCWARN("Discrepancy between original: '" << *s_it << "' ");
            FUNCWARN("    and simplified: '" << simplifiedreparsed << "' ");
            FUNCERR("Maxima says the difference between the original and the simplified is '" << res << "'");
        }
    }   
 
    
    FUNCINFO("Test was successful");
    return 0;
}


