//Test_Generation.cc.

#include <iostream>

#include <list>
#include <string>
#include <memory>

#include "YgorMisc.h"
//#include "YgorFilesDirs.h"

//#include "PolyCAS_Structs.h"
//#include "PolyCAS_Parsing.h"
//#include "PolyCAS_Substitution.h"

#include "PolyCAS_Generation.h"

int main(){
    long int num_of_exprs(1000); //Generation 1000 expressions.
    long int max_expr_len(15);   //Not a hard limit. Might get 16 variable expressions because of binary operations.

    std::list<std::string> somevars({"Sb", "S3m", "S1y"});
    for(auto i=0; i<num_of_exprs; ++i){
        std::cout << PolyCAS_Randomly_Generate_Basic_Arithmetic_Involving(somevars,max_expr_len) << std::endl;
    }

    return 0;
}



